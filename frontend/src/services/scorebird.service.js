import axios from 'axios';

var ScoreBirdServiceInstance = null;

class ScoreBirdService {
  hostname = window && window.location && window.location.hostname;

  apiUrl = 'http://' + this.hostname + ':4000';

  hologram = {
    apiUrl: 'https://dashboard.hologram.io/api/1/devices',
    orgId: '13417',
    apiKey: '3he2rRDrPUfKHdieRQoOv4CmOLIF4u'
  };

  groupsPromise = null;
  devicesPromise = null;
  teamsPromise = null;
  gamesPromise = {};

  devicesData;
  groupsData;
  teamsData;
  gamesData = {};

  devicesById = {};
  groupsById = {};
  teamsById = {};
  gamesById = {};

  gamesActiveDate = '';

  constructor() {
    if (!ScoreBirdServiceInstance) {
      ScoreBirdServiceInstance = this;
    }
    return ScoreBirdServiceInstance;
  }

  getDevicesData() {
    if (!this.devicesPromise) {
      this.devicesPromise = axios.get(`/devices`).then((res) => {
        let devices = res.data.devices;
        let devicesData = [];
        let devicesById = {};
        devices.map((device) => {
          let device_row = {
            device_name: device.device_name ? device.device_name : '--',
            device_id: device.serial ? device.serial : '--',
            group: device.groups ? device.groups.join(', ') : '--',
            group_names: '--',
            sport: device.sport ? device.sport : '--',
            score_board: device.score_board || '--',
            last_online_ts: device.last_online_ts
              ? device.last_online_ts
              : '--',
            notes: device.notes ? device.notes : '',
            online: device.online ? device.online : ''
          };
          devicesData.push(device_row);
          devicesById[device.serial] = device_row;
        });
        this.devicesData = devicesData;
        this.devicesById = devicesById;
        return true;
      });
    }
    return this.devicesPromise;
  }

  getDeviceDetails(deviceId) {
    return axios.get(`/devices/detail/${deviceId}`).then((res) => {
      let response = res.data;
      return response;
    });
  }

  saveDeviceDetails(params) {
    return axios.post(`/devices/updateInfo/`, params).then((res) => {
      let response = res.data;
      this.devicesPromise = null;
      return response;
    });
  }
  saveFacilityDetails(params) {
    var _this = this;
    return axios
      .post(`/facilities/updateFacilityInfoFromFacilitypage/`, params)
      .then((res) => {
        let response = res.data;
        // this.devicesPromise = null;
        return response;
      });
  }
  getDeviceLocation(sim_number) {
    return new Promise(function (resolve, reject) {
      if (localStorage.getItem('sim_locations')) {
        let sim_locations = JSON.parse(localStorage.getItem('sim_locations'));
        if (sim_locations[sim_number]) {
          resolve(sim_locations[sim_number]);
        }
      }
      axios.get(`/devices/getDeviceLocation/${sim_number}`).then((res) => {
        let response = res.data;
        let sim_locations = {};
        if (localStorage.getItem('sim_locations')) {
          sim_locations = JSON.parse(localStorage.getItem('sim_locations'));
        } else {
          sim_locations = {};
        }
        sim_locations[sim_number] = response.maps.address;
        localStorage.setItem('sim_locations', JSON.stringify(sim_locations));
        resolve(sim_locations[sim_number]);
      });
    });
  }

  getAllSchools() {
    return axios.get('/schools/getAll');
  }
  
  getGroupsData() {
    if (!this.groupsPromise) {
      this.groupsPromise = axios.get(`/devices/getAllGroups`).then((res) => {
        let groups = res.data.data.Items;
        let groupsData = [];
        let groupsById = {};
        groups.map((g) => {
          let grp_row = {
            id: g.group_id,
            text: g.group_name
          };
          groupsData.push(grp_row);
          groupsById[g.group_id] = grp_row;
        });
        this.groupsData = groupsData;
        this.groupsById = groupsById;
        return true;
      });
    }
    return this.groupsPromise;
  }

  assignGroupsToDevices(params) {
    // this.devicesPromise = null;
    // delete window.ScoreBirdService.devicesPromise;
    // this.groupsPromise = null;
    // delete window.ScoreBirdService.groupsPromise;
    return axios.post(`/devices/assignGroups`, params).then(function (response) {
      return response.data;
    });
  }

  getAllTeams() {
    if (!this.teamsPromise) {
      this.teamsPromise = axios.get(`/devices/getAllTeams`).then((res) => {
        let teams = res.data.data.Items;
        return teams;
      });
    }
    return this.teamsPromise;
  }

  assignTeams(params) {
    return axios.post(`/devices/assignTeams`, params).then((response) => {
      return response;
    });
  }

  // todo
  getDeviceStreamData(device_id) {
    return axios.get(`/devices/streamData/${device_id}`).then((response) => {
      return response;
    });
  }

  // todo
  getGamesData(selected_date) {
    if (!this.gamesPromise[selected_date]) {
      this.gamesPromise[selected_date] = axios
        .post(`/games`, {
          selected_date: selected_date
        })
        .then((res) => {
          let games = res.data.games;
          let gamesData = [];
          games.map((game) => {
            gamesData.push({
              game_id: game.game_id || '--',
              schedule_id: game.schedule_id || '--',
              game_name: game.name || '--',
              start_time: game.game_start_ts || '--',
              group_name: game.group_name || '--',
              home_team: game.team_home_school || '--',
              home_team_id: game.team_home_id || '--',
              away_team: game.team_away_school || '--',
              away_team_id: game.team_away_id || '--',
              group_name: game.group_name || '--',
              sport: game.sport || '--',
              device_name:
                game.game_data && game.game_data.device_serial
                  ? game.game_data.device_serial
                  : '--',
              game_snapshot: game.game_snapshot || '--',
              game_icon: game.game_icon || 'no-icon',
              finalized: game.finalized && game.finalized === true,
              game_data: game.game_data || '',
              partner_id: game.partner_id
            });
          });
          this.gamesData[selected_date] = gamesData;
          return true;
        });
    }
    return this.gamesPromise[selected_date];
  }

  /*
  getGameDetails(gameId) {
    return axios.get(`/games/detail/${gameId}`).then((res) => {
      let response = res.data;
      return response;
    });
  }
  */

  // 3 api calls instead of one getGameDetails
  getOneGame(id) {
    axios.get(`/api/game/${id}`).then((res) => {
      return res.data;
    });
  }
  getOneSchedule(id) {
    axios.get(`/api/schedule/${id}`).then((res) => {
      return res.data;
    });
  }
  getAllDevicesByKey(key,value) {
    axios.get(`/api/device/${key}/${value}`).then((res) => {
      return res.data;
    });
  }

  /*
  finalizeGame(gameId) {
    axios.get(`/games/finalize/${gameId}`).then((res) => {
      return res.data;
    });
  }
  
  unfinalizeGame(gameId) {
    axios.get(`/games/unfinalize/${gameId}`).then((res) => {      
      return res.data;
    });
  }
  */

  //done
  getAllDevices() {
    axios.get('api/device').then((res) => {
      return res.data;
    });
  }

  //done
  getAllFacilities() {
    axios.get('api/facility').then((res) => {
      return res.data;
    });
  }

  //done
  getAllUsers() {
    axios.get('api/user').then((res) => {
      return res.data;
    });
  }

  //done
  getSchoolfacilityData(schoolId) {
    axios.get(`api/school/facilityData/${schoolId}`).then((res) => {
      return res.data;
    });
  }

  //done
  updateUser(id,payload) {    
    axios.put(`/api/user`, payload).then((res) => {
      return res.data;
    });
  }

  //done
  createUser(payload) {
    axios.post(`/api/user`, payload).then((res) => {
      return res.data;
    });
  }

  //done
  getOneUser(id) {
    axios.get(`/api/user/${id}`).then((res) => {      
      return res.data;
    });
  }

  // done
  getAllSports() {
    axios.get('api/sport').then((res) => {   
      return res.data;
    });
  }

  //done
  updateSport(id, payload) {
    axios.put(`api/sport/${id}`, payload).then((res) => {
      return res.data;
    });
  }

  //done
  createSport(payload) {
    axios.post('/api/sport', payload).then((res) => {
      return res.data;
    });
  }
}

export default ScoreBirdService;
