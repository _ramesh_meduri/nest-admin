import React, { Component } from 'react';
import './App.scss';
import AdminPanel from './components/admin';
import Login from './components/login';
import Spinner from '@atlaskit/spinner';

class App extends Component {
  state = {
    loggedIn: false,
    authChecking: true
  };

  checkAuth() {
    setTimeout(() => {
      let admin_details = localStorage.getItem('adminAuthInfo')
        ? JSON.parse(localStorage.getItem('adminAuthInfo'))
        : {};

      if (admin_details.loggedIn) {
        this.setState({
          authChecking: false,
          loggedIn: true
        });
      } else {
        this.setState({
          authChecking: false,
          loggedIn: false
        });
      }
    }, 500);
  }

  afterLogin = () => {
    let admin_details = {
      loggedIn: true
    };
    localStorage.setItem('adminAuthInfo', JSON.stringify(admin_details));
    this.setState({
      loggedIn: true
    });
  };

  afterLogout = () => {
    localStorage.removeItem('adminAuthInfo');
    this.setState({ loggedIn: false });
  };

  renderAuthDom() {
    // if (this.state.loggedIn) {
    //   return <AdminPanel afterLogout={this.afterLogout} />;
    // } else {
    //   return <Login afterLogin={this.afterLogin} />;
    // }

    return <AdminPanel afterLogout={this.afterLogout} />;
  }

  componentDidMount() {
    this.checkAuth();
  }

  renderAuthDom2() {
    if (!this.state.authChecking) {
      return this.renderAuthDom();
    } else {
      return (
        <div className="page-loader">
          <Spinner size="large" />
        </div>
      );
    }
  }

  render() {
    return <div className="App">{this.renderAuthDom2()}</div>;
  }
}

export default App;
