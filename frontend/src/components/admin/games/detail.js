import React, { PureComponent } from 'react';
import Button from '@atlaskit/button';
import Spinner from '@atlaskit/spinner';
import HipchatMediaAttachmentCountIcon from '@atlaskit/icon/glyph/hipchat/media-attachment-count';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import moment from 'moment';

import ScoreBirdService from '../../../services/scorebird.service';
import ArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left';

class FinalizeButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFinalizeModalOpened: false
    };

    if (this.props.gameInfo.finalized) {
      this.modalHeader = 'UNFINALIZE GAME';
      this.modalInnerText = 'Do you really want to unfinalize this game?';
    } else {
      this.modalHeader = 'FINALIZE GAME';
      this.modalInnerText = 'Do you really want to finalize this game?';
    }
  }
  confirmFinalizeGame = () => {
    this.setState({
      isFinalizeModalOpened: true
    });
  };
  finalizeGame = () => {
    this.setState({
      isFinalizeModalOpened: false
    });
  };

  renderFinalizeDOM = () => {
    const closeModal = () => {
      this.setState({
        isFinalizeModalOpened: false
      });
      console.log('lamda function to be invoked yet.');
    };

    const actions = [
      { text: 'YES', onClick: this.finalizeGame, isDisabled: false },
      { text: 'NO', onClick: closeModal }
    ];

    return (
      <ModalTransition>
        {this.state.isFinalizeModalOpened && (
          <Modal
            actions={actions}
            onClose={true}
            heading={this.modalHeader}
            width={'medium'}
          >
            {this.modalInnerText}
          </Modal>
        )}
      </ModalTransition>
    );
  };

  render() {
    return (
      <div>
        {this.props.gameInfo.finalized ? (
          <Button className="finalize-btn" onClick={this.confirmFinalizeGame}>
            UNFINALIZE
          </Button>
        ) : (
          <Button className="finalize-btn" onClick={this.confirmFinalizeGame}>
            FINALIZE
          </Button>
        )}
        {this.renderFinalizeDOM()}
      </div>
    );
  }
}
const Header = (props) => (
  <div className="content-header">
    <div className="content-header-inner">
      <div className="left-panel">
        <div className="content-label" onClick={props.onClickBack}>
          <span className="back-icon" to="/devices">
            <ArrowLeftIcon />
          </span>
          Games [{props.gameInfo.team_data.name}]{' '}
          <span
            className={
              'game-status-indicator ' + props.gameInfo.team_data.game_icon
            }
          >
            <HipchatMediaAttachmentCountIcon />
          </span>
        </div>
      </div>
      <div className="right-panel">
        <div className="content-filters">
          <div className="finalize-btn">
            <FinalizeButton gameInfo={props.gameInfo} />
          </div>
        </div>
      </div>
    </div>
  </div>
);

class GameDetail extends PureComponent {
  constructor(props) {
    super(props);
    this.sbs = new ScoreBirdService();
    this.gameId = '';
    this.groupsById = {};
    this.allGroups = [];
    this.groupInfo = null;
    this.formRef = null;

    this.state = {
      validateOnChange: true,
      deviceName: 'abc',
      loading: true,
      isEdit: false,
      gameSaving: false,
      selectedItems: [],
      filterValue: '',
      gameId: '',
      allGroups: this.props.groups,
      submitting: false,
      showErrors: false,
      errorMsgs: Array,
      isFinalizeModalOpened: false,
      cancelBtnLabel: 'Cancel',
      submitBtnLabel: 'Update Group',
      deviceInfoSaving: false
    };
  }

  // If you provide a submit handler you can do any custom data handling & validation
  onSubmitHandler = () => {
    const validateResult = this.formRef.validate();
  };

  onValidateHandler = () => {
    console.log('onValidateHandler');
  };

  onResetHandler = () => {
    console.log('onResetHandler');
  };

  onChangeHandler = () => {
    const ref = this.formRef;
  };

  onBlurHandler = () => {
    console.log('onBlurHandler');
  };

  onFocusHandler = () => {
    console.log('onFocusHandler');
  };

  toggleEdit = () => {
    this.setState({ isEdit: !this.state.isEdit });
  };

  handleGroupDelete = (i) => {
    const { selectedGroups } = this.state;
    this.setState({
      selectedGroups: selectedGroups.filter((group, index) => index !== i)
    });
  };

  handleGroupAddition = (group) => {
    this.setState((state) => ({
      selectedGroups: [...state.selectedGroups, group]
    }));
  };

  handleFilterSuggestions = (textInputValue, allSuggestions) => {
    var lowerCaseQuery = textInputValue.toLowerCase();
    var selected = this.state.selectedGroups;

    return allSuggestions.filter(function(suggestion) {
      return (
        suggestion.text.toLowerCase().includes(lowerCaseQuery) &&
        selected.findIndex((s) => {
          return suggestion.text === s.text;
        }) === -1
      );
    });
  };

  selectItem = (item) => {
    const selectedItems = [...this.state.selectedItems, item];
    this.setState({ selectedItems });
  };

  removeItem = (item) => {
    const selectedItems = this.state.selectedItems.filter(
      (i) => i.value !== item.value
    );
    this.setState({ selectedItems });
  };

  selectedChange = (item) => {
    if (this.state.selectedItems.some((i) => i.value === item.value)) {
      this.removeItem(item);
    } else {
      this.selectItem(item);
    }
  };

  handleFilterChange = (value) => {
    this.setState({ filterValue: value });
  };

  handleOpenChange = (attrs) => {
    this.setState({ isOpen: attrs.isOpen });
  };

  getGameDetails(gameId) {
    this.sbs.getGroupsData().then(() => {
      let groups = this.sbs.groupsData;

      groups.forEach((g) => {
        this.groupsById[g.id] = g.text;
      });

      this.setState({ groups: groups });

      this.sbs.getGameDetails(gameId).then((response) => {
        let gameInfo = response.gameInfo;
        if (gameInfo.last_online_ts) {
          let item = gameInfo;
          let selectedDate = moment(gameInfo.last_online_ts).format(
            'MM/DD/YYYY, h:mm:ss a'
          );
          if (selectedDate === 'Invalid date') {
            item.last_online_ts = '--';
          } else {
            item.last_online_ts = selectedDate;
          }
        }
        if (gameInfo.last_update_ts) {
          let item = gameInfo;
          let selectedDate = moment(gameInfo.last_update_ts).format(
            'MM/DD/YYYY, h:mm:ss a'
          );
          if (selectedDate === 'Invalid date') {
            item.last_update_ts = '--';
          } else {
            item.last_update_ts = selectedDate;
          }
        }
        if (gameInfo.scheduled_date) {
          let item = gameInfo;
          let selectedDate = moment(gameInfo.scheduled_date).format(
            'MM/DD/YYYY'
          );
          if (selectedDate === 'Invalid date') {
            item.scheduled_date = '--';
          } else {
            item.scheduled_date = selectedDate;
          }
        }
        if (gameInfo.created_ts) {
          let item = gameInfo;
          let selectedDate = moment(gameInfo.created_ts).format(
            'MM/DD/YYYY, h:mm:ss a'
          );
          if (selectedDate === 'Invalid date') {
            item.created_ts = '--';
          } else {
            item.created_ts = selectedDate;
          }
        }
        if (gameInfo.device_data.last_online_ts) {
          let item = gameInfo;
          let selectedDate = moment(gameInfo.device_data.last_online_ts).format(
            'MM/DD/YYYY, h:mm:ss a'
          );
          if (selectedDate === 'Invalid date') {
            item.device_data.last_online_ts = '--';
          } else {
            item.device_data.last_online_ts = selectedDate;
          }
        }

        if (gameInfo.team_data.game_start_ts) {
          let item = gameInfo;
          let game_start_ts = gameInfo.team_data.game_start_ts;
          let selectedDate = moment(game_start_ts)
            .clone()
            .format('hh:mm a');
          if (selectedDate === 'Invalid date') {
            item.team_data.game_start_ts = '--';
          } else {
            item.team_data.game_start_ts = selectedDate;
          }
          let raw_date = game_start_ts.split('T')[0];
          let selectedDate2 = moment(raw_date).format('MM/DD/YYYY');
          if (selectedDate2 === 'Invalid date') {
            item.scheduled_date = '--';
          } else {
            item.scheduled_date = selectedDate2;
          }
        }

        gameInfo.group_names = '--';
        if (gameInfo.groups) {
          let groups2 = gameInfo.groups.map((g) => {
            return this.groupsById[g];
          });
          gameInfo.group_names = groups2.length ? groups2.join(',') : '--';
          gameInfo.group_names =
            gameInfo.group_names === ',' ? '--' : gameInfo.group_names;
        }

        this.gameInfo = gameInfo;
        this.setState({
          loading: false,
          gameInfo: gameInfo
        });
      });
    });
  }

  init() {
    const {
      match: { params }
    } = this.props;
    this.gameId = params.gameId;
    this.setState({
      gameId: params.gameId,
      loading: true
    });
  }

  componentDidMount() {
    this.init();
    this.getGameDetails(this.gameId);
  }

  renderForm() {
    if (this.state.loading) {
      return (
        <div className="loader">
          <Spinner size="large" />
        </div>
      );
    } else {
      var editView = {
        display: this.state.isEdit ? 'flex' : 'none'
      };

      var normalView = {
        display: this.state.isEdit ? 'none' : 'flex',
        marginTop: '0px'
      };

      return (
        <div className="device-info-container">
          <div className="notEdit-field-sec" style={{ marginBottom: '0px' }}>
            <h2>Game Information</h2>
          </div>
          <div className="view-info" style={normalView}>
            <div className="notEdit-field-sec">
              <label>Game Name</label>
              <span>
                {this.gameInfo.team_data ? this.gameInfo.team_data.name : '--'}
              </span>
            </div>
            <div className="notEdit-field-sec">
              <label>Schedule Date</label>
              <span>
                {this.gameInfo.scheduled_date
                  ? this.gameInfo.scheduled_date
                  : '--'}
              </span>
            </div>
            <div className="notEdit-field-sec">
              <label>Start Time</label>
              <span>
                {this.gameInfo.team_data
                  ? this.gameInfo.team_data.game_start_ts
                  : '--'}
              </span>
            </div>
            <div className="notEdit-field-sec">
              <label>Sport</label>
              <span>
                {this.gameInfo.team_data ? this.gameInfo.team_data.sport : '--'}
              </span>
            </div>
            <div className="notEdit-field-sec">
              <label>Home Team</label>
              <span>
                {this.gameInfo.team_data
                  ? this.gameInfo.team_data.team_home_school
                  : '--'}
              </span>
            </div>
            <div className="notEdit-field-sec">
              <label>Away Team</label>
              <span>
                {this.gameInfo.team_data
                  ? this.gameInfo.team_data.team_away_school
                  : '--'}
              </span>
            </div>
            <div className="notEdit-field-sec">
              <label>Live Indicator</label>
              <span>{this.gameInfo.status ? 'Online' : 'Offline'}</span>
            </div>
            <div className="notEdit-field-sec">
              <label>Snapshot</label>
              <span>Lorem epsum content</span>
            </div>
            <div className="notEdit-field-sec">
              <label>Integration Partner</label>
              <span>
                {this.gameInfo.state && this.gameInfo.state.partner_id
                  ? this.gameInfo.state.partner_id
                  : '--'}
              </span>
            </div>
            <div className="notEdit-field-sec">
              <label>Created Date</label>
              <span>
                {this.gameInfo.created_ts ? this.gameInfo.created_ts : '--'}
              </span>
            </div>
            <div className="notEdit-field-sec">
              <label>Last Updated on</label>
              <span>
                {this.gameInfo.last_update_ts
                  ? this.gameInfo.last_update_ts
                  : '--'}
              </span>
            </div>
            <div className="notEdit-field-sec">
              <label>Last Online time</label>
              <span>
                {this.gameInfo.last_update_ts
                  ? this.gameInfo.last_update_ts
                  : '--'}
              </span>
            </div>
          </div>
          <div className="notEdit-field-sec" style={{ marginBottom: '0px' }}>
            <h2>Device Information</h2>
          </div>
          <div className="view-info" style={normalView}>
            <div className="notEdit-field-sec">
              <label>Device Name</label>
              <span>
                {this.gameInfo.device_data.name
                  ? this.gameInfo.device_data.name
                  : '--'}
              </span>
            </div>
            <div className="notEdit-field-sec">
              <label>Device Number</label>
              <span>
                {this.gameInfo.device_serial
                  ? this.gameInfo.device_serial
                  : '--'}
              </span>
            </div>
            <div className="notEdit-field-sec">
              <label>Device SIM</label>
              <span>
                {this.gameInfo.device_data.sim_id
                  ? this.gameInfo.device_data.sim_id
                  : '--'}
              </span>
            </div>
            <div className="notEdit-field-sec">
              <label>Group</label>
              <span>
                {this.gameInfo.group_names ? this.gameInfo.group_names : '--'}
              </span>
            </div>
            <div className="notEdit-field-sec">
              <label>Firmware Version</label>
              <span>
                {this.gameInfo.device_data.firmware_version
                  ? this.gameInfo.device_data.firmware_version
                  : '--'}
              </span>
            </div>
            <div className="notEdit-field-sec">
              <label>Last Online</label>
              <span>
                {this.gameInfo.device_data.last_online_ts
                  ? this.gameInfo.device_data.last_online_ts
                  : '--'}
              </span>
            </div>
          </div>

          {this.state.showErrors ? (
            <div className="errorMsgs">{this.state.errorMsg}</div>
          ) : (
            ''
          )}
        </div>
      );
    }
  }

  onClickBack = () => {
    this.props.history.push('/games');
  };

  renderHeader() {
    if (!this.state.loading) {
      return (
        <Header onClickBack={this.onClickBack} gameInfo={this.state.gameInfo} />
      );
    } else {
      return '';
    }
  }

  render() {
    return (
      <div className="content-container">
        <div className="content-header-outer">{this.renderHeader()}</div>
        {this.renderForm()}
      </div>
    );
  }
}

export default GameDetail;
