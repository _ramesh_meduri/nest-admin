import React from 'react';
import { DatePicker } from '@atlaskit/datetime-picker';
import '../../../styles/devices.scss';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import DropdownMenu, {
  DropdownItemGroup,
  DropdownItem
} from '@atlaskit/dropdown-menu';
import HipchatMediaAttachmentCountIcon from '@atlaskit/icon/glyph/hipchat/media-attachment-count';

const Header = (props) => (
  <div className="content-header">
    <div className="content-header-inner">
      <div className="left-panel">
        <div className="content-label">Games</div>
        <div className="datepicker-sec">
          <span>On Date:</span>
          <div
            className={
              'datepicker-element ' + (props.loading ? 'disable-date' : '')
            }
          >
            <DatePicker
              id="datepicker-2"
              value={props.datePicker.defaultDate}
              onChange={props.datePicker.onValueChange}
              onBlur={props.datePicker.onBlur}
            />
          </div>
        </div>
      </div>
      <div className="right-panel">
        <div className="content-filters">
          {!props.loading && (
            <div className="DropdownMenu-games">
              <DropdownMenu
                trigger="Filter"
                triggerType="button"
                shouldFlip={false}
                positions="bottom"
                isMenuFixed
                onOpenChange={(e) => e}
              >
                <DropdownItemGroup>
                  {
                    <DropdownItem onClick={props.filterList.all}>
                      <div
                        className="status-icon-wrap"
                        style={{ paddingLeft: '7px' }}
                      >
                        All Games
                      </div>
                    </DropdownItem>
                  }
                  {
                    <DropdownItem onClick={props.filterList.games1}>
                      <div className="status-icon-wrap green-icon">
                        <HipchatMediaAttachmentCountIcon />
                        Live
                      </div>
                    </DropdownItem>
                  }
                  {
                    <DropdownItem onClick={props.filterList.games2}>
                      <div className="status-icon-wrap red-icon">
                        <HipchatMediaAttachmentCountIcon />
                        Inactive
                      </div>
                    </DropdownItem>
                  }
                  {
                    <DropdownItem onClick={props.filterList.games3}>
                      <div className="status-icon-wrap blue-icon">
                        <HipchatMediaAttachmentCountIcon />
                        Finalized
                      </div>
                    </DropdownItem>
                  }
                  {
                    <DropdownItem onClick={props.filterList.games4}>
                      <div className="status-icon-wrap gray-icon">
                        <HipchatMediaAttachmentCountIcon />
                        Scheduled
                      </div>
                    </DropdownItem>
                  }
                </DropdownItemGroup>
              </DropdownMenu>
            </div>
          )}
        </div>
      </div>
    </div>
  </div>
);

export default Header;
