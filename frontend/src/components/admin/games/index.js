import React, { Component } from 'react';
import { Redirect, NavLink } from 'react-router-dom';
import { AgGridReact } from 'ag-grid-react';
import Button from '@atlaskit/button';
import Spinner from '@atlaskit/spinner';
import '../../../styles/devices.scss';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import Header from './header';
import ScoreBirdService from '../../../services/scorebird.service';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import moment from 'moment';

const gameNameColrenderer = (props) => (
  <NavLink to={'/gameDetail/' + props.data.schedule_id}>{props.value}</NavLink>
);

const gameStartTSRenderer = (params) => {
  var val = params.value.replace('Z', '');
  return params.value === '--'
    ? '--'
    : moment(val).format('MM/DD/YYYY hh:mm a');
};

class Games extends Component {
  constructor(props) {
    super(props);
    this.sbs = new ScoreBirdService();
    let ts = new Date();
    this.current_date =
      ts.getFullYear() + '/' + (ts.getMonth() + 1) + '/' + ts.getDate();
    this.teamsById = {};
    this.scheduleById = {};
    this.active_date = '';
    this.selectedGame = null;

    this.state = {
      gamesData: [],
      gamesByStatus: {},
      modalHeader: '',
      modalInnerText: '',
      selectedDeviceId: -1,
      redirect: false,
      loading: true,
      isFinalizeModalOpened: false,
      columnDefs: [
        {
          headerName: 'Game',
          field: 'game_name',
          cellClass: function(params) {
            return 'game_name_col ' + params.data.game_icon;
          },
          tooltip: (params) => params.value,
          cellRendererFramework: gameNameColrenderer
        },
        {
          headerName: 'Sport',
          field: 'sport',
          tooltip: (params) => params.value
        },
        {
          headerName: 'Start Time',
          field: 'start_time',
          tooltip: gameStartTSRenderer,
          cellRenderer: gameStartTSRenderer
        },
        {
          headerName: 'Home Team',
          field: 'home_team',
          tooltip: (params) => params.value
        },
        {
          headerName: 'Away Team',
          field: 'away_team',
          tooltip: (params) => params.value
        },
        {
          headerName: 'Device Serial',
          field: 'device_name',
          tooltip: (params) => params.value
        },
        { headerName: 'Game Snapshot', field: 'game_shopshot' },
        {
          headerName: 'Actions',
          field: '',
          cellRendererFramework: this.gameActionColrenderer,
          suppressFilter: true
        }
      ],

      defaultColDef: {
        width: 100,
        editable: false,
        lockPosition: true,
        filter: 'agTextColumnFilter'
      },
      rowSelection: 'multiple',
      rowData: [],
      onGridReady: function(params) {
        params.api.sizeColumnsToFit();
        window.addEventListener('resize', function() {
          setTimeout(function() {
            params.api.sizeColumnsToFit();
          });
        });
      }
    };

    this.filterList = {
      all: () => {
        this.setState({
          rowData: this.state.gamesData
        });
      },
      games1: () => {
        this.setState({
          rowData: this.state.gamesByStatus['green-icon']
            ? this.state.gamesByStatus['green-icon']
            : []
        });
      },
      games2: () => {
        this.setState({
          rowData: this.state.gamesByStatus['red-icon']
            ? this.state.gamesByStatus['red-icon']
            : []
        });
      },
      games3: () => {
        this.setState({
          rowData: this.state.gamesByStatus['blue-icon']
            ? this.state.gamesByStatus['blue-icon']
            : []
        });
      },
      games4: () => {
        this.setState({
          rowData: this.state.gamesByStatus['gray-icon']
            ? this.state.gamesByStatus['gray-icon']
            : []
        });
      }
    };

    this.active_date = this.sbs.gamesActiveDate || this.current_date;
    // var active_date= moment(active_date).format('MM-DD-YYYY');

    this.datePicker = {
      defaultDate: this.active_date,
      // defaultDate  = moment(defaultDate).format('MM-DD-YYYY'),
      onValueChange: (e) => {
        let selected_date = e;
        this.setState({
          loading: true,
          rowData: []
        });
        this.sbs.gamesActiveDate = selected_date;
        this.datePicker.defaultDate = selected_date;
        this.getAllGamesBydate(selected_date);
      },

      onBlur: (e) => {
        console.log(e);
      }
    };
  }

  renderFinalizeDOM = () => {
    const closeModal = () => {
      this.setState({
        isFinalizeModalOpened: false
      });
    };

    const actions = [
      { text: 'YES', onClick: this.finalizeGame, isDisabled: false },
      { text: 'NO', onClick: closeModal }
    ];

    return (
      <ModalTransition>
        {this.state.isFinalizeModalOpened && (
          <Modal
            actions={actions}
            onClose={true}
            heading={this.state.modalHeader}
            width={'medium'}
          >
            Do you really want to {this.state.modalInnerText} this game?
          </Modal>
        )}
      </ModalTransition>
    );
  };

  finalizeGame = () => {
    this.setState({
      isFinalizeModalOpened: false
    });
  };

  confirmFinalizeGame = (selectedGame) => {
    this.selectedGame = selectedGame;
    this.setState({
      modalHeader: this.selectedGame.data.finalized
        ? 'UNFINALIZE GAME'
        : 'FINALIZE GAME',
      modalInnerText: this.selectedGame.data.finalized
        ? 'unfinalize'
        : 'finalize',
      isFinalizeModalOpened: true
    });
  };

  gameActionColrenderer = (params) => {
    if (params.data.game_data) {
      if (params.data.finalized) {
        return (
          <Button
            className="finalize-btn"
            onClick={this.confirmFinalizeGame.bind(null, params)}
          >
            UNFINALIZE
          </Button>
        );
      } else {
        return (
          <Button
            className="finalize-btn"
            onClick={this.confirmFinalizeGame.bind(null, params)}
          >
            FINALIZE
          </Button>
        );
      }
    } else {
      return '';
    }
  };

  setRowRedirect = (deviceId) => {
    this.setState({ selectedDeviceId: deviceId, redirect: true });
  };

  renderRowRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to={'/deviceDetail/' + this.state.selectedDeviceId} />;
    }
  };

  onRowClicked = (event) => {
    this.setRowRedirect(event.data.device_id);
  };

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
    window.addEventListener('resize', function() {
      setTimeout(function() {
        params.api.sizeColumnsToFit();
      });
    });
    params.api.sizeColumnsToFit();
  }

  getGridData() {
    if (this.state.loading) {
      return (
        <div className="loader">
          <Spinner size="large" />
        </div>
      );
    } else {
      var noRowsTemplate = '<span>No games to show.</span>';
      return (
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <div style={{ overflow: 'hidden', flexGrow: '1' }}>
            <div
              className="ag-theme-material ag-custom-styles device-grid"
              style={{
                height: 'calc(100vh - 100px)',
                width: '100%'
              }}
            >
              <AgGridReact
                columnDefs={this.state.columnDefs}
                defaultColDef={this.state.defaultColDef}
                rowSelection={this.state.rowSelection}
                enableColResize={true}
                enableSorting={true}
                floatingFilter={true}
                rowHeight="48"
                suppressDragLeaveHidesColumns={true}
                suppressRowClickSelection={true}
                rowData={this.state.rowData}
                onGridReady={this.state.onGridReady}
                onGridReady={this.onGridReady.bind(this)}
                pagination={true}
                paginationPageSize="20"
                overlayNoRowsTemplate={noRowsTemplate}
              />
            </div>
          </div>
          {this.renderFinalizeDOM()}
        </div>
      );
    }
  }

  getAllGamesBydate(date) {
    this.sbs.getGamesData(date).then((response) => {
      let gamesData = this.sbs.gamesData[date];
      let gamesByStatus = {};
      gamesData.map((g) => {
        let status = g.game_icon ? g.game_icon : 'no-icon';
        if (!gamesByStatus[status]) {
          gamesByStatus[status] = [];
        }
        gamesByStatus[status].push(g);
      });
      this.setState({
        loading: false,
        rowData: gamesData,
        gamesData: gamesData,
        gamesByStatus: gamesByStatus
      });
      this.datePicker.isDisabled = false;
    });
  }

  init() {
    this.setState({
      loading: true,
      rowData: []
    });
    this.getAllGamesBydate(this.active_date);
  }

  componentDidMount() {
    this.init();
  }

  render() {
    return (
      <div className="content-container">
        <div className="content-header-outer">
          <Header
            datePicker={this.datePicker}
            filterList={this.filterList}
            loading={this.state.loading}
          />
        </div>
        <div style={{ width: '100%', height: '100%' }}>
          {this.renderRowRedirect()}
          {this.getGridData()}
        </div>
      </div>
    );
  }
}

export default Games;
