import React, { Component } from 'react';
import TextField from '@atlaskit/textfield';
import Form, { Field, ErrorMessage } from '@atlaskit/form';
import Button from '@atlaskit/button';
import ArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left';
import ScorebirdService from '../../../services/scorebird.service';

class AddSport extends Component {
  constructor(props) {
    super(props);
    this.sbs = new ScorebirdService();
    this.state = { sportName: '' };
  }

  handleChange = (target, type) => {
    if (type === 'sport') {
      console.log(target.value);
      this.setState({
        sportName: target.value
      });
    }
  };

  back = () => {
    this.props.history.push('/sports');
  };

  render() {
    return (
      <div>
        <div className="content-header">
          <div className="content-header-inner header-row">
            <div className="left-panel back-with-heading">
              <div className="back-to-text" onClick={this.back}>
                <span className="back-icon">
                  <ArrowLeftIcon />
                </span>
                <b>Back To Sports </b>
              </div>
            </div>
            <div className="right-panel" />
          </div>
        </div>
        <div className="add-form">
          <Form
            onSubmit={(data) => {
              let payload = { name: data.sport_name };
              this.sbs.addSport(payload).then((res) => {
                this.back();
                console.log(res.data);
              });
            }}
          >
            {({ formProps }) => (
              <form {...formProps}>
                <div className="scrollable">
                  <div className="user-details">
                    <Field
                      name="sport_name"
                      label="SportName"
                      defaultValue={this.state.last_nameType}
                      value={this.state.last_nameType}
                    >
                      {({ fieldProps, error }) => (
                        <>
                          {' '}
                          <TextField
                            placeholder="Enter Sport name"
                            {...fieldProps}
                            onKeyUp={(e) => {
                              this.handleChange(e.target, 'sport');
                            }}
                          />
                          {error === 'empty' && (
                            <ErrorMessage>Please enter Last Name </ErrorMessage>
                          )}
                        </>
                      )}
                    </Field>
                  </div>
                </div>
                <div className="footer-actions">
                  <div />
                  <div className="actions">
                    {this.props.type == 'edit' ? (
                      <Button
                        type="button"
                        appearance="default"
                        onClick={this.props.cancel}
                      >
                        Cancel
                      </Button>
                    ) : (
                      <Button
                        type="button"
                        appearance="default"
                        onClick={this.props.toggleEdit}
                      >
                        Cancel
                      </Button>
                    )}
                    <Button type="submit" appearance="primary">
                      Save
                    </Button>
                  </div>
                </div>
              </form>
            )}
          </Form>
        </div>
      </div>
    );
  }
}

export default AddSport;
