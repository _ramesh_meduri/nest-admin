import React, { Component } from 'react';
import TextField from '@atlaskit/textfield';
import Form from '@atlaskit/form';
import Button from '@atlaskit/button';
import ArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left';
import ScorebirdService from '../../../services/scorebird.service';

class EditSport extends Component {
  constructor(props) {
    super(props);
    this.sbs = new ScorebirdService();
    this.state = {
      sportName: this.props.location.state.sportName
    };
  }

  onChange = (e) => {
    this.setState({ sportName: e.target.value });
  };

  back = () => {
    this.props.history.push('/sports');
  };

  render() {
    return (
      <div>
        <div className="content-header">
          <div className="content-header-inner header-row">
            <div className="left-panel back-with-heading">
              <div className="back-to-text" onClick={this.back}>
                <span className="back-icon">
                  <ArrowLeftIcon />
                </span>
                <b>Back To Sports </b>
              </div>
            </div>
            <div className="right-panel" />
          </div>
        </div>
        <div className="add-form">
          <Form
            onSubmit={(data) => {
              let id = this.props.match.params.sportId;
              let payload = { name: this.state.sportName };
              this.sbs.updateSport(id, payload).then((res) => {
                this.back();
                console.log(res.data);
              });
            }}
          >
            {({ formProps }) => (
              <form {...formProps}>
                <div className="scrollable">
                  <div className="user-details">
                    <TextField
                      placeholder="Enter Sport name"
                      value={this.state.sportName}
                      onChange={this.onChange}
                    />
                  </div>
                </div>
                <div className="footer-actions">
                  <div className="actions" style={{ display: 'flex', width: '100%', justifyContent: 'flex-end' }}>
                    <Button
                      type="button"
                      appearance="default"
                      onClick={this.props.cancel}
                    >
                      Cancel
                    </Button>
                    <Button type="submit" appearance="primary">
                      Update
                    </Button>
                  </div>
                </div>
              </form>
            )}
          </Form>
        </div>
      </div>
    );
  }
}

export default EditSport;
