import React, { Component } from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import { Redirect } from 'react-router-dom';
import Button from '@atlaskit/button';
import TextField from '@atlaskit/field-text';
import SearchIcon from '@atlaskit/icon/glyph/search';
import Spinner from '@atlaskit/spinner';
import ScorebirdService from '../../../services/scorebird.service';

const Header = (props) => (
  <div className="content-header">
    <div className="content-header-inner header-row">
      <div className="left-panel">
        <h1 className="content-label">Sports Types</h1>
      </div>
      <div className="right-panel">
        <div>
          <Button
            appearance="default"
            className="custom-btn"
            onClick={props.setRedirect}
          >
            Add Sport
          </Button>
        </div>
        <div className="content-search">
          <div className="search-icon">
            <SearchIcon />
            <TextField
              placeholder="Search"
              label="hidden label"
              isLabelHidden
            />
          </div>
        </div>
      </div>
    </div>
  </div>
);

class Sports extends Component {
  constructor(props) {
    super(props);
    this.sbs = new ScorebirdService();
    this.state = {
      redirect: false,
      editRedirect: false,
      columnDefs: [
        { headerName: 'SPORT NAME', field: 'name' },
        { headerName: 'CREATED AT', field: 'createdAt' },
        { headerName: 'UPDATED AT', field: 'updatedAt' }
      ],
      DefaultcolDef: {
        width: 100,
        editable: true,
        lockPosition: true,
        filter: 'agTextColumnFilter'
      },
      rowData: [],
      _id: '',
      name: '',
      loading: true
    };
  }

  componentDidMount() {
    this.sbs.getSports().then((res) => {
      this.setState({ rowData: res.data.data ,loading: false});
    });
  }

  setRedirect = () => {
    this.setState({ redirect: true });
  };

  onRowClicked = (event) => {
    let Id = event.data._id;
    let name = event.data.name;
    this.setState({ _id: Id, editRedirect: true, name: name });
  };

  getGridData() {
    var normalView = {
      display: this.state.isEdit ? 'none' : ' '
    };

    return (
      <div className="ag-theme-material ag-custom-styles device-grid">
      <div />
      {this.state.loading ? (
        <div className="loader">
          <Spinner size="large" />
        </div>
      ) : (
      <div className="user-info-container">
        <div className="user-normal-view" style={normalView}>
          <div
            className="ag-theme-material ag-custom-styles device-grid"
            style={{
              height: 'calc(100vh - 60px)',
              width: '100%'
            }}
          >
            <div className="contai" />
            <AgGridReact
              enableSorting={true}
              pagination={true}
              enableColResize={true}
              paginationPageSize="30"
              rowHeight="48"
              headerHeight="36"
              columnDefs={this.state.columnDefs}
              rowData={this.state.rowData}
              onGridReady={this.onGridReady.bind(this)}
              onRowClicked={this.onRowClicked}
            />
          </div>
        </div>
      </div>
      )}
      </div>
    );
  }

  renderRowRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to={'/addSport'} />;
    }
    if (this.state.editRedirect) {
      let sportName = this.state.name;
      let sportId = this.state._id;
      return (
        <Redirect
          to={{
            pathname: '/editSport/' + sportId,
            state: { sportID: sportId, sportName: sportName }
          }}
        />
      );
    }
  };

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
    window.addEventListener('resize', function() {
      setTimeout(function() {
        params.api.sizeColumnsToFit();
      });
    });
    params.api.sizeColumnsToFit();
  }

  render() {
    return (
      <div>        
        <Header adduserform={this.adduserform} setRedirect={this.setRedirect} />
        <div style={{ width: '100%', height: '100%' }}>
          {this.getGridData()}
          {this.renderRowRedirect()}
          <div style={{ width: '40px', height: '15px', float: 'right' }}> </div>
        </div>
      </div>
    );
  }
}
export default Sports;
