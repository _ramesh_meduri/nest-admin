import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { AgGridReact } from 'ag-grid-react';
import Button from '@atlaskit/button';
import { Field } from '@atlaskit/form';
import Spinner from '@atlaskit/spinner';
import '../../../styles/devices.scss';
import '../../../styles/react-tags.scss';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import ScoreBirdService from '../../../services/scorebird.service';
import EditFilledIcon from '@atlaskit/icon/glyph/edit-filled';
import DropdownMenu, {
  DropdownItemGroup,
  DropdownItem
} from '@atlaskit/dropdown-menu';
import HipchatMediaAttachmentCountIcon from '@atlaskit/icon/glyph/hipchat/media-attachment-count';
import moment from 'moment';

import { WithContext as ReactTags } from 'react-tag-input';

const Icon = <EditFilledIcon size="small" />;

const KeyCodes = {
  comma: 188,
  enter: 13
};

const delimiters = [KeyCodes.comma, KeyCodes.enter];

class DialogContent extends Component {
  ScoreBirdService;

  static defaultProps = {
    isDisabled: false,
    shouldFocus: false,
    isDefaultOpen: false,
    isRequired: false,
    items: [
      { content: 'Sydney', value: 'city_1' },
      { content: 'Canberra', value: 'city_2' },
      { content: 'Melbourne', value: 'city_3' },
      { content: 'Perth', value: 'city_4', isDisabled: true },
      { content: 'Some city with spaces', value: 'city_5' },
      { content: 'Some city with another spaces', value: 'city_6' }
    ],
    label: 'Device Name',
    placeholder: 'Device Name',
    name: 'test'
  };

  constructor(props) {
    super(props);
    this.ScoreBirdService = new ScoreBirdService();
  }

  // we need to keep track of this state ourselves and pass it back into the MultiSelectStateless
  state = {
    isOpen: this.props.isDefaultOpen,
    selectedItems: [],
    filterValue: '',
    allDifferent: this.props.editGroup.allDifferent,
    selectedDevices: this.props.editGroup.selectedDevices,
    selectedGroups: this.props.editGroup.selectedGroups,
    allGroups: this.props.editGroup.allGroups,
    submitting: false,
    cancelBtnLabel: 'Cancel',
    submitBtnLabel: 'Update Group'
  };

  //group autosuggest tagging
  handleGroupDelete = (i) => {
    const { selectedGroups } = this.state;
    this.setState({
      selectedGroups: selectedGroups.filter((group, index) => index !== i)
    });
  };

  handleGroupAddition = (group) => {
    this.setState((state) => ({
      selectedGroups: [...state.selectedGroups, group]
    }));
  };

  handleFilterSuggestions = (textInputValue, allSuggestions) => {
    var lowerCaseQuery = textInputValue.toLowerCase();
    var selected = this.state.selectedGroups;

    return allSuggestions.filter(function(suggestion) {
      return (
        suggestion.text.toLowerCase().includes(lowerCaseQuery) &&
        selected.findIndex((s) => {
          return suggestion.text === s.text;
        }) === -1
      );
    });
  };

  selectItem = (item) => {
    const selectedItems = [...this.state.selectedItems, item];
    this.setState({ selectedItems });
  };

  removeItem = (item) => {
    const selectedItems = this.state.selectedItems.filter(
      (i) => i.value !== item.value
    );
    this.setState({ selectedItems });
  };

  selectedChange = (item) => {
    if (this.state.selectedItems.some((i) => i.value === item.value)) {
      this.removeItem(item);
    } else {
      this.selectItem(item);
    }
    // we could update isInvalid here
  };

  handleFilterChange = (value) => {
    // value will tell us the value the filter wants to change to
    this.setState({ filterValue: value });
  };

  handleOpenChange = (attrs) => {
    // attrs.isOpen will tell us the state that the dropdown wants to move to
    this.setState({ isOpen: attrs.isOpen });
  };

  closeDialog = () => {
    this.props.onClose();
  };

  updateDeviceGroup = () => {
    this.setState({
      submitting: true,
      submitBtnLabel: 'Updating...'
    });
    let device_ids = this.state.selectedDevices.map((d) => {
      return d.id;
    });
    this.ScoreBirdService.assignGroupsToDevices({
      devices: device_ids,
      groups: this.state.selectedGroups,
      allDifferent: this.state.allDifferent
    }).then((response) => {
      this.setState({
        submitting: false,
        submitBtnLabel: 'Update Group'
      });
      let selectedGroups = this.state.selectedGroups;
      if (response.newGroups) {
        selectedGroups.forEach((g) => {
          if (g.id === g.text) {
            let ix = response.newGroups.findIndex((ng) => {
              return ng.text === g.text;
            });
            if (ix > -1) {
              g.id = response.newGroups[ix].id;
            }
          }
        });
      }
      setTimeout(() => {
        this.props.onClose();
        this.props.editGroup.afterSave({
          devices: device_ids,
          selectedGroups: selectedGroups,
          newGroups: response.newGroups,
          allDifferent: this.state.allDifferent
        });
      }, 200);
    });
  };

  render() {
    return (
      <div>
        <div className="form-field">
          <Field label="Devices">
            <ReactTags tags={this.state.selectedDevices} readOnly={true} />
          </Field>
        </div>
        <div className="form-field">
          <Field label="Group">
            <ReactTags
              tags={this.state.selectedGroups}
              suggestions={this.state.allGroups}
              handleDelete={this.handleGroupDelete}
              handleFilterSuggestions={this.handleFilterSuggestions}
              handleAddition={this.handleGroupAddition}
              delimiters={delimiters}
              testinfo={this.testinfo}
              placeholder="Add new group"
              allowDragDrop={false}
            />
          </Field>

          {this.state.allDifferent && (
            <span className="edit-groups-warning" style={{ fontSize: '12px' }}>
              * Groups for selected devices are different. Any groups which are
              added will be appended to previous groups.
            </span>
          )}
        </div>
        <div className="dialog-footer {this.state.submitting?'loading':''}">
          <Button
            appearance="subtle"
            className="custom-btn"
            onClick={this.closeDialog}
          >
            {this.state.cancelBtnLabel}
          </Button>
          <Button
            appearance="danger"
            className="custom-btn-red"
            isDisabled={this.state.submitting}
            onClick={this.updateDeviceGroup}
          >
            {this.state.submitBtnLabel}
          </Button>
        </div>
      </div>
    );
  }
}

class EditGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showContent: false
    };
  }

  showGroupsData() {
    if (this.state.isGroupsLoading) {
      return (
        <div className="loader">
          <Spinner size="large" />
        </div>
      );
    } else {
      return (
        <div className="edit-dialog-content">
          <h3>Edit Group</h3>
          <DialogContent
            onClose={this.closeContent}
            editGroup={this.props.data}
          />
        </div>
      );
    }
  }

  showContent() {
    if (this.state.showContent) {
      return (
        <div className="custom-inline-dialog-content">
          {this.showGroupsData()}
        </div>
      );
    } else {
      return;
    }
  }

  toggleContent = () => {
    this.setState({
      showContent: !this.state.showContent
    });
  };

  closeContent = () => {
    this.setState({
      showContent: false
    });
  };

  getBtn() {
    if (this.props.data.isOpen) {
      return (
        <div className="custom-inline-dialog-inner">
          <Button
            isSelected={this.props.isSelected}
            onClick={this.toggleContent}
            appearance="primary"
            className="custom-btn"
            iconBefore={Icon}
          >
            Edit Group
          </Button>
          {this.showContent()}
        </div>
      );
    } else {
      return;
    }
  }
  render() {
    return <div className="custom-inline-dialog">{this.getBtn()}</div>;
  }

  onlinedropdown() {
    this.props.dropdownonline();
  }
  offlinedropdown() {
    this.props.dropdownoffline();
  }
}
const Header = (props) => (
  <div className="content-header">
    <div className="content-header-inner">
      <div className="left-panel">
        <div className="content-label">Devices</div>
      </div>
      <div className="right-panel">
        {!props.loading && (
          <div className="DropdownMenu-devices">
            <DropdownMenu
              trigger="Filter"
              triggerType="button"
              shouldFlip={false}
              positions="bottom"
              isMenuFixed
              onOpenChange={(e) => e}
            >
              <DropdownItemGroup>
                {
                  <DropdownItem onClick={props.filterList.getAll}>
                    <div
                      className="status-icon-wrap"
                      style={{ paddingLeft: '7px' }}
                    >
                      All
                    </div>
                  </DropdownItem>
                }
                {
                  <DropdownItem onClick={props.filterList.getOnline}>
                    <div className="status-icon-wrap green-icon">
                      <HipchatMediaAttachmentCountIcon />
                      Online
                    </div>
                  </DropdownItem>
                }
                {
                  <DropdownItem onClick={props.filterList.getOffline}>
                    <div className="status-icon-wrap gray-icon">
                      <HipchatMediaAttachmentCountIcon />
                      Offline
                    </div>
                  </DropdownItem>
                }
              </DropdownItemGroup>
            </DropdownMenu>
          </div>
        )}
        <div className="edit-inline-dialog">
          <EditGroup data={props.editGroup} />
          {/* {/ <Button appearance="default">Filters</Button> /} */}
        </div>
      </div>
    </div>
  </div>
);

const lastOnlineTSRenderer = (params) => {
  return params.value === '--'
    ? '--'
    : moment(params.value).format('MM/DD/YYYY HH:mm:ss a');
};

const deviceIDColrenderer = (props) => (
  <NavLink to={'/deviceDetail/' + props.value}>
    <span>{props.value}</span>
  </NavLink>
);

class Devices extends Component {
  _ismounted = false;
  sbs;
  groupsById = {};
  constructor(props) {
    super(props);
    this.sbs = new ScoreBirdService();

    this.state = {
      selectedonlineDevices: [],
      selectedofflineDevices: [],
      allDevices: [],
      selectedDeviceId: -1,
      redirect: false,
      loading: true,
      editGroup: {
        loading: false,
        isOpen: false,
        selectedDevices: [],
        selectedGroups: [],
        allGroups: [],
        allDifferent: false,
        afterSave: this.afterSave
      },
      groups: [],
      columnDefs: [
        {
          headerName: 'Device Name',
          field: 'device_name',
          headerCheckboxSelection: true,
          checkboxSelection: true,
          tooltip: (params) => params.value,

          cellClass: function(params) {
            return params.data.online ? 'device-online' : 'device-offline';
          }
        },
        {
          headerName: 'Device ID#',
          field: 'device_id',
          cellRendererFramework: deviceIDColrenderer,
          tooltip: (params) => params.value
        },
        {
          headerName: 'Group',
          field: 'group_names',
          tooltip: (params) => params.value
        },
        {
          headerName: 'Sport',
          field: 'sport',
          tooltip: (params) => params.value
        },
        {
          headerName: 'Score Board',
          field: 'score_board',
          tooltip: (params) => params.value
        },
        {
          headerName: 'Last Online',
          field: 'last_online_ts',
          tooltip: lastOnlineTSRenderer,
          cellRenderer: lastOnlineTSRenderer
        },
        {
          headerName: 'Notes',
          field: 'notes',
          tooltip: (params) => params.value
        }
      ],

      defaultColDef: {
        width: 100,
        editable: false,
        lockPosition: true,

        filter: 'agTextColumnFilter'
      },
      rowSelection: 'multiple',
      rowData: [],
      onGridReady: function(params) {
        params.api.sizeColumnsToFit();
        window.addEventListener('resize', function() {
          setTimeout(function() {
            params.api.sizeColumnsToFit();
          });
        });
      }
    };

    this.filterList = {
      getAll: () => {
        this.setState({
          rowData: this.state.allDevices
        });
      },
      getOnline: () => {
        this.setState({
          rowData: this.state.selectedonlineDevices
        });
      },
      getOffline: () => {
        this.setState({
          rowData: this.state.selectedofflineDevices
        });
      }
    };
  }

  init() {
    let _this = this;
    this.sbs.getGroupsData().then((groupsResponse) => {
      let groups = this.sbs.groupsData;
      groups.forEach((g) => {
        _this.groupsById[g.id] = g.text;
      });
      let editGroup = this.state.editGroup;
      Object.assign(editGroup, {
        loading: false,
        allGroups: groups
      });
      _this.setState({
        groups: groups,
        editGroup: editGroup
      });
      this.sbs.getDevicesData().then((devicesResponse) => {
        let devices = this.sbs.devicesData;
        let selectedonlineDevices = [];
        let selectedofflineDevices = [];

        devices.forEach((device) => {
          if (device.group && device.group !== '--') {
            let groups = device.group.split(', ');
            let groups2 = groups.map((g) => {
              return _this.groupsById[g];
            });
            device.group_names = groups2.length ? groups2.join(', ') : '--';
            device.group_names =
              device.group_names === ',' ? '--' : device.group_names;
          }
          if (device.online === true) {
            selectedonlineDevices.push(device);
          } else if (device.online === false) {
            selectedofflineDevices.push(device);
          }
        });
        _this.setState({
          selectedofflineDevices: selectedofflineDevices,
          selectedonlineDevices: selectedonlineDevices,
          allDevices: devices
        });
        _this.setState({
          loading: false,
          rowData: devices
        });
      });
    });
  }

  componentDidMount() {
    this.init();
  }

  reset() {
    this.setState({
      editGroup: {
        ...this.state.editGroup,
        allGroups: [],
        isOpen: false
      },
      loading: true,
      groups: [],
      rowData: []
    });
  }

  afterSave = (data) => {
    let devicesRowData = this.sbs.devicesData;

    devicesRowData.filter((device, index) => {
      if (data.devices.indexOf(device.device_id) > -1) {
        let groups = data.selectedGroups
          .map((g) => {
            return g.id;
          })
          .join(', ');
        let group_names = data.selectedGroups
          .map((g) => {
            return g.text;
          })
          .join(', ');

        let device_row = {};
        if (data.allDifferent) {
          device_row = {
            ...device,
            group: device.group ? device.group + ', ' + groups : '',
            group_names: device.group_names
              ? device.group_names + ', ' + group_names
              : ''
          };
        } else {
          device_row = {
            ...device,
            group: groups,
            group_names: group_names
          };
        }
        this.sbs.devicesData[index] = device_row;
      }
    });

    if (data.newGroups) {
      this.sbs.groupsData = [...this.sbs.groupsData, ...data.newGroups];
    }

    setTimeout(() => {
      this.reset();
      this.init();
    }, 100);
  };

  onRowSelected = (event) => {
    let selectedCount = Object.values(
      event.api.selectionController.selectedNodes
    ).filter((n) => {
      return n;
    }).length;

    let selectedRows = this.gridApi.getSelectedRows();
    let selectedRowsGrps = selectedRows.map((row) => {
      return row.group
        .split(',')
        .sort()
        .join(',');
    });
    let checkIfGroupsSame = selectedRowsGrps.every(
      (val, i, arr) => val === arr[0]
    );
    let selectedDevices = selectedRows.map((r) => {
      return {
        id: r.device_id,
        text: r.device_id
      };
    });

    let selectedGroups = [];
    if (checkIfGroupsSame && selectedRowsGrps.length) {
      let device_group = selectedRowsGrps[0];
      selectedGroups = this.state.groups.filter((g) => {
        return device_group.indexOf(g.id) > -1;
      });
    }

    //adding new data to old 'editGroup' object
    let editGroup = this.state.editGroup;
    Object.assign(editGroup, {
      allDifferent: !checkIfGroupsSame,
      isOpen: selectedCount > 0 ? true : false,
      selectedDevices: selectedDevices,
      selectedGroups: selectedGroups
    });

    this.setState({
      editGroup: editGroup
    });
  };

  onSelectionChanged = (event) => {
    //console.log(event);
  };

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
    window.addEventListener('resize', function() {
      setTimeout(function() {
        params.api.sizeColumnsToFit();
      });
    });

    params.api.sizeColumnsToFit();
  }

  dropdownonlineDevices = () => {
    let _this = this;
    _this.setState({
      rowData: this.state.selectedonlineDevices
    });
  };
  dropdownofflineDevices = () => {
    let _this = this;
    _this.setState({
      rowData: this.state.selectedofflineDevices
    });
  };

  getGridData() {
    if (this.state.loading) {
      return (
        <div className="loader">
          <Spinner size="large" />
        </div>
      );
    } else {
      var noRowsTemplate = '<span>No devices to show.</span>';
      return (
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <div style={{ overflow: 'hidden', flexGrow: '1' }}>
            <div
              className="ag-theme-material ag-custom-styles device-grid"
              style={{
                height: 'calc(100vh - 60px)',
                width: '100%'
              }}
            >
              <AgGridReact
                columnDefs={this.state.columnDefs}
                defaultColDef={this.state.defaultColDef}
                rowSelection={this.state.rowSelection}
                enableColResize={true}
                enableSorting={true}
                enableFilter={true}
                floatingFilter={true}
                rowHeight="48"
                suppressDragLeaveHidesColumns={true}
                suppressRowClickSelection={true}
                rowData={this.state.rowData}
                getRowNodeId={this.state.getRowNodeonline}
                onGridReady={this.state.onGridReady}
                onGridReady={this.onGridReady.bind(this)}
                onRowSelected={this.onRowSelected}
                onSelectionChanged={this.onSelectionChanged}
                pagination={true}
                paginationPageSize="20"
                overlayNoRowsTemplate={noRowsTemplate}
              />
            </div>
          </div>
        </div>
      );
    }
  }

  render() {
    return (
      <div className="content-container">
        <div className="content-header-outer">
          <Header
            editGroup={this.state.editGroup}
            filterList={this.filterList}
            loading={this.state.loading}
          />
        </div>
        <div style={{ width: '100%', height: '100%' }}>
          {this.getGridData()}
        </div>
      </div>
    );
  }
}

export default Devices;
