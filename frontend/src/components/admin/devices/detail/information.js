import React, { PureComponent } from 'react';
import Button from '@atlaskit/button';
import FieldText from '@atlaskit/field-text';
import { WithContext as ReactTags } from 'react-tag-input';
import Select from '@atlaskit/select';
import { Field, FormSection } from '@atlaskit/form';

import ScoreBirdService from '../../../../services/scorebird.service';

const KeyCodes = {
  comma: 188,
  enter: 13
};

const sports = [
  { value: 'basketball', label: 'Basket Ball' },
  { value: 'football', label: 'Foot Ball' }
];

const score_boards = [
  { value: 'scoreboard.1', label: 'Daktronics 5000' },
  { value: 'scoreboard.2', label: 'Electromech' },
  { value: 'scoreboard.3', label: 'Fairplay MP70' },
  { value: 'scoreboard.4', label: 'Nevco' }
];

const getGroups = function(group_ids, all_groups) {
  let selected_groups = [];
  if (group_ids) {
    all_groups.map((g) => {
      if (group_ids.indexOf(g.id) > -1) {
        selected_groups.push({
          id: g.id,
          text: g.text
        });
      }
    });
  }

  return selected_groups;
};

const delimiters = [KeyCodes.comma, KeyCodes.enter];

// class DeviceData extends PureComponent<void, State> {
class DeviceData extends PureComponent {
  constructor(props) {
    super(props);
    this.sbs = new ScoreBirdService();
  }

  sbs;
  state = {
    validateOnChange: true,
    deviceName: 'abc',
    loading: true,
    isEdit: false,
    location: '--',
    deviceSaving: false,
    selectedItems: [],
    filterValue: '',
    selectedGroups: getGroups(this.props.info.groups, this.props.groups) || [],
    allGroups: this.props.groups,
    submitting: false,
    showErrors: false,
    errorMsgs: Array,
    cancelBtnLabel: 'Cancel',
    submitBtnLabel: 'Update Group',
    deviceInfoSaving: false,
    deviceForm: {
      name: this.props.info.device_name ? this.props.info.device_name : '',
      group: getGroups(this.props.info.groups, this.props.groups) || [],
      sport:
        sports.filter((s) => {
          return this.props.info.sport === s.value;
        })[0] || '',
      score_board:
        score_boards.filter((sb) => {
          return this.props.info.score_board === sb.value;
        })[0] || '',
      notes: this.props.info.notes ? this.props.info.notes : ''
    }
  };

  formRef;
  // If you provide a submit handler you can do any custom data handling & validation
  onSubmitHandler = () => {
    console.log('onSubmitHandler');

    const validateResult = this.formRef.validate();
    console.log(validateResult);
  };

  onValidateHandler = () => {
    console.log('onValidateHandler');
  };

  onResetHandler = () => {
    console.log('onResetHandler');
  };

  onChangeHandler = () => {
    const ref = this.formRef;
    console.log(ref);
    console.log(this.state);
    console.log('onChangeHandler');
  };
  onBlurHandler = () => {
    console.log('onBlurHandler');
  };
  onFocusHandler = () => {
    console.log('onFocusHandler');
  };

  toggleEdit = () => {
    this.setState({
      isEdit: !this.state.isEdit
      // deviceInfoState = () => { isEdit return '' }
    });
  };

  componentDidMount() {
    this.sbs.getDeviceLocation(this.props.info.sim_id).then((location) => {
      this.setState({
        location: location
      });
    });
  }

  //group autosuggest tagging
  handleGroupDelete = (i) => {
    const { selectedGroups } = this.state;
    this.setState({
      selectedGroups: selectedGroups.filter((group, index) => index !== i)
    });
  };

  handleGroupAddition = (group) => {
    this.setState((state) => ({
      selectedGroups: [...state.selectedGroups, group]
    }));
  };

  handleFilterSuggestions = (textInputValue, allSuggestions) => {
    var lowerCaseQuery = textInputValue.toLowerCase();
    var selected = this.state.selectedGroups;

    return allSuggestions.filter(function(suggestion) {
      return (
        suggestion.text.toLowerCase().includes(lowerCaseQuery) &&
        selected.findIndex((s) => {
          return suggestion.text === s.text;
        }) === -1
      );
    });
  };

  selectItem = (item) => {
    const selectedItems = [...this.state.selectedItems, item];
    this.setState({ selectedItems });
  };

  removeItem = (item) => {
    const selectedItems = this.state.selectedItems.filter(
      (i) => i.value !== item.value
    );
    this.setState({ selectedItems });
  };

  selectedChange = (item) => {
    if (this.state.selectedItems.some((i) => i.value === item.value)) {
      this.removeItem(item);
    } else {
      this.selectItem(item);
    }
    // we could update isInvalid here
  };

  handleFilterChange = (value) => {
    // value will tell us the value the filter wants to change to
    this.setState({ filterValue: value });
  };

  handleOpenChange = (attrs) => {
    // attrs.isOpen will tell us the state that the dropdown wants to move to
    this.setState({ isOpen: attrs.isOpen });
  };

  saveDeviceDetails = (e) => {
    e.preventDefault();
    // const validateResult = this.formRef.validate();

    let device = {
      id: this.props.info.serial,
      name: this.formRef.device_name.value,
      groups: this.state.selectedGroups,
      sport: this.formRef.sport.value,
      score_board: this.formRef.score_board.value,
      notes: this.formRef.notes.value
    };

    let error_keys = [];
    let is_error = false;
    for (var key in device) {
      if (key === 'groups' && !device[key].length) {
        is_error = true;
        error_keys.push(key);
      } else if (!device[key]) {
        is_error = true;
        error_keys.push(key);
      }
    }

    if (!is_error) {
      this.setState({
        deviceInfoSaving: true
      });
      this.sbs.saveDeviceDetails(device).then((response) => {
        if (response.newGroups) {
          this.sbs.groupsData = [...this.sbs.groupsData, ...response.newGroups];
        }
        this.props.afterSave({
          device: device,
          newGroups: response.newGroups ? response.newGroups : []
        });
      });
    } else {
      this.setState({
        showErrors: true,
        errorMsg: 'please enter ' + error_keys.join(', ')
      });
    }
  };

  renderForm() {
    var editView = {
      display: this.state.isEdit ? 'flex' : 'none'
    };

    var normalView = {
      display: this.state.isEdit ? 'none' : 'flex'
    };

    return (
      <div className="device-info-container">
        <div className="inner-header">
          <h2>Device Information</h2>
          {!this.state.isEdit ? (
            <Button
              appearance="primary"
              className="custom-btn"
              onClick={this.toggleEdit}
            >
              Edit
            </Button>
          ) : null}
          {/* <Button appearance="primary" className="custom-btn" onClick={this.toggleEdit}>{this.state.isEdit ? 'Cancel' : 'Edit'} Device</Button> */}
        </div>
        <div className="view-info" style={normalView}>
          <div className="notEdit-field-sec">
            <label>ID</label>
            <span>
              {this.props.info.serial ? this.props.info.serial : '--'}
            </span>
          </div>
          <div className="notEdit-field-sec">
            <label>Name</label>
            <span>
              {this.props.info.device_name ? this.props.info.device_name : '--'}
            </span>
          </div>
          <div className="notEdit-field-sec">
            <label>Group</label>
            <span>
              {this.props.info.group_names ? this.props.info.group_names : '--'}
            </span>
          </div>
          <div className="notEdit-field-sec">
            <label>Sport</label>
            <span>{this.props.info.sport ? this.props.info.sport : '--'}</span>
          </div>
          <div className="notEdit-field-sec">
            <label>Score Board</label>
            <span>
              {this.props.info.score_board ? this.props.info.score_board : '--'}
            </span>
          </div>
          <div className="notEdit-field-sec">
            <label>Location</label>
            <span>{this.state.location}</span>
          </div>
          <div className="notEdit-field-sec">
            <label>Notes</label>
            <span>{this.props.info.notes ? this.props.info.notes : '--'}</span>
          </div>
          <div className="notEdit-field-sec">
            <label>Status</label>
            <span>{this.props.info.status ? 'Online' : 'Offline'}</span>
          </div>
          <div className="notEdit-field-sec">
            <label>CPU ID</label>
            <span>
              {this.props.info.cpu_id ? this.props.info.cpu_id : '--'}
            </span>
          </div>
          <div className="notEdit-field-sec">
            <label>Firmware</label>
            <span>
              {this.props.info.firmware_version
                ? this.props.info.firmware_version
                : '--'}
            </span>
          </div>
          <div className="notEdit-field-sec">
            <label>CCID</label>
            <span>
              {this.props.info.device_info && this.props.info.device_info.ccid
                ? this.props.info.device_info.ccid
                : '--'}
            </span>
          </div>
          <div className="notEdit-field-sec">
            <label>Interface</label>
            <span>
              {this.props.info.device_info &&
              this.props.info.device_info.interface
                ? this.props.info.device_info.interface
                : '--'}
            </span>
          </div>
          <div className="notEdit-field-sec">
            <label>Cell Display</label>
            <span>
              {this.props.info.device_info &&
              this.props.info.device_info.cell_display
                ? this.props.info.device_info.cell_display
                : '--'}
            </span>
          </div>
          <div className="notEdit-field-sec">
            <label>Up Time</label>
            <span>
              {this.props.info.device_info && this.props.info.device_info.uptime
                ? this.props.info.device_info.uptime
                : '--'}
            </span>
          </div>
          <div className="notEdit-field-sec">
            <label>Model</label>
            <span>
              {this.props.info.model_variant
                ? this.props.info.model_variant
                : '--'}
            </span>
          </div>
          <div className="notEdit-field-sec">
            <label>SIM #</label>
            <span>
              {this.props.info.sim_id ? this.props.info.sim_id : '--'}
            </span>
          </div>
          <div className="notEdit-field-sec">
            <label>Last Online</label>
            <span>
              {this.props.info.last_online_ts
                ? this.props.info.last_online_ts
                : '--'}
            </span>
          </div>
          <div className="notEdit-field-sec">
            <label>Last Update</label>
            <span>
              {this.props.info.last_update_ts
                ? this.props.info.last_update_ts
                : '--'}
            </span>
          </div>
        </div>

        <div className="edit-info" style={editView}>
          <form
            className="device-form"
            onSubmit={this.onSubmitHandler}
            onReset={this.onResetHandler}
            ref={(form) => {
              this.formRef = form;
            }}
            //action={formTestUrl}
            // method="get"
            style={{ backgroundColor: 'white' }}
            //target="submitFrame"
          >
            <FormSection name="section-1">
              <div className="form-field-container">
                <div className="form-field">
                  <Field
                    label="Name"
                    helperText={null}
                    validateOnChange
                    isRequired
                  >
                    <FieldText
                      name="device_name"
                      placeholder="Enter device name"
                      value={this.state.deviceForm.name}
                    />
                  </Field>
                </div>

                <div className="form-field">
                  <Field label="Group">
                    <ReactTags
                      tags={this.state.selectedGroups}
                      suggestions={this.state.allGroups}
                      handleDelete={this.handleGroupDelete}
                      handleFilterSuggestions={this.handleFilterSuggestions}
                      handleAddition={this.handleGroupAddition}
                      delimiters={delimiters}
                      placeholder="Add new group"
                      validateOnChange
                      isRequired
                      allowDragDrop={false}
                    />
                  </Field>
                </div>

                <div className="form-field">
                  <Field label="Sport" helperText={null} required>
                    <Select
                      name="sport"
                      className="single-select"
                      options={sports}
                      validateOnChange
                      isRequired
                      defaultValue={this.state.deviceForm.sport}
                      placeholder="Choose a Sport"
                    />
                  </Field>
                </div>

                <div className="form-field">
                  <Field label="Score Board" helperText={null} required>
                    <Select
                      name="score_board"
                      className="single-select"
                      options={score_boards}
                      defaultValue={this.state.deviceForm.score_board}
                      placeholder="Choose a Score board"
                    />
                  </Field>
                </div>

                <div className="form-field">
                  <Field label="Notes" validateOnChange isRequired>
                    <FieldText
                      name="notes"
                      label="Is Invalid & showing message"
                      value={this.state.deviceForm.notes}
                    />
                  </Field>
                </div>
              </div>
              <p>
                <Button
                  type="submit"
                  appearance="primary"
                  onClick={this.saveDeviceDetails}
                  isDisabled={this.state.deviceInfoSaving}
                >
                  {this.state.deviceInfoSaving ? 'Saving...' : 'Save'}
                </Button>
                {this.state.isEdit ? (
                  <Button
                    appearance="subtle"
                    className="custom-btn"
                    onClick={this.toggleEdit}
                  >
                    Cancel
                  </Button>
                ) : null}
              </p>
            </FormSection>
          </form>
        </div>

        {this.state.showErrors ? (
          <div className="errorMsgs">{this.state.errorMsg}</div>
        ) : (
          ''
        )}
      </div>
    );
  }

  renderLoader() {}

  render() {
    return <div>{this.renderForm()}</div>;
  }
}

export default DeviceData;
