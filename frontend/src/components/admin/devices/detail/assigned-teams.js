import React, { Component } from 'react';
import { AgGridReact } from 'ag-grid-react';
import Button from '@atlaskit/button';
import TextField from '@atlaskit/field-text';
import Spinner from '@atlaskit/spinner';
import SearchIcon from '@atlaskit/icon/glyph/search';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import { CheckboxSelect } from '@atlaskit/select';
import ScoreBirdService from '../../../../services/scorebird.service';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';

class AssignedTeam extends Component {
  constructor(props) {
    super(props);
    this.sbs = new ScoreBirdService();
    this.state = {
      isTeamModalOpen: false,
      selectedDeviceId: -1,
      redirect: false,
      loading: true,
      allTeamsLoading: true,
      allTeams: [],
      teamsAssignLoading: false,
      assignBtnLabel: 'Assign',
      columnDefs: [
        { headerName: 'Team Name', field: 'team_name' },
        { headerName: 'School', field: 'school' },
        { headerName: 'Team Location', field: 'team_location' },
        { headerName: 'Added On', field: 'added_on' }
      ],

      defaultColDef: {
        // set every column width
        width: 100,
        // make every column editable
        editable: false,
        lockPosition: true,
        // headerCheckboxSelection: isFirstColumn,
        // checkboxSelection: isFirstColumn,
        // make every column use 'text' filter by default
        filter: 'agTextColumnFilter'
      },
      rowSelection: 'multiple',
      teamsRowData: []
    };
    var _this = this;
    this.addTeamModal = {
      selected: [],
      defaultTeams: [],
      open: () => {
        if (_this.state.allTeamsLoading) {
          _this.setState({
            isTeamModalOpen: true,
            allTeamsLoading: true
          });
          setTimeout(() => {
            _this.setState({
              allTeamsLoading: false
            });
          }, 100);
        } else {
          this.setState({
            isTeamModalOpen: true
          });
        }
      },
      close: () => {
        this.setState({
          isTeamModalOpen: false
        });
      },
      assignTeams: () => {
        let selectedTeams = this.addTeamModal.selected.map((t) => {
          return t.value;
        });
        let params = {
          device_id: this.props.deviceInfo.serial,
          teams: selectedTeams
        };
        this.setState({
          teamsAssignLoading: true,
          assignBtnLabel: 'Assigning...'
        });
        this.sbs.assignTeams(params).then((response) => {
          this.afterTeamsAssigned(selectedTeams);
        });
      },
      handleChange: (options) => {
        this.addTeamModal.selected = [...options];
      },
      checkIfloaded: () => {
        const defaultTeams = this.state.teamsRowData.map((t) => {
          return {
            value: t.team_id,
            label: t.team_name
          };
        });

        const allTeams = this.state.allTeams.map((t) => {
          return {
            label: t.name,
            value: t.team_id
          };
        });

        if (this.state.allTeamsLoading) {
          return (
            <div className="loader">
              <Spinner size="large" />
            </div>
          );
        } else {
          return (
            <CheckboxSelect
              menuPortalTarget={document.body}
              styles={{ menuPortal: (base) => ({ ...base, zIndex: 9999 }) }}
              className="checkbox-select"
              classNamePrefix="select"
              defaultValue={defaultTeams}
              options={allTeams}
              onChange={this.addTeamModal.handleChange}
              placeholder="Choose Teams to Assign"
              isLoading={false}
            />
          );
        }
      },
      renderDOM: () => {
        const actions = [
          {
            text: this.state.assignBtnLabel,
            onClick: this.addTeamModal.assignTeams,
            isDisabled: this.state.teamsAssignLoading
          },
          { text: 'Cancel', onClick: this.addTeamModal.close }
        ];

        return (
          <ModalTransition>
            {this.state.isTeamModalOpen && (
              <Modal
                actions={actions}
                onClose={this.addTeamModal.close}
                heading="Assign Teams"
                width={'medium'}
              >
                {this.addTeamModal.checkIfloaded()}
              </Modal>
            )}
          </ModalTransition>
        );
      }
    };
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
    window.addEventListener('resize', function() {
      setTimeout(function() {
        params.api.sizeColumnsToFit();
      });
    });

    params.api.sizeColumnsToFit();
  };

  getGridData() {
    if (this.state.loading) {
      return (
        <div className="loader">
          <Spinner size="large" />
        </div>
      );
    } else {
      return (
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <div style={{ overflow: 'hidden', flexGrow: '1' }}>
            <div
              className="ag-theme-balham ag-custom-styles"
              style={{
                height: 'calc(100vh - 100px)',
                width: '100%'
              }}
            >
              <AgGridReact
                columnDefs={this.state.columnDefs}
                defaultColDef={this.state.defaultColDef}
                rowSelection={this.state.rowSelection}
                rowHeight="48"
                suppressDragLeaveHidesColumns={true}
                rowData={this.state.teamsRowData}
                onGridReady={this.onGridReady}
              />
            </div>
          </div>
        </div>
      );
    }
  }

  afterTeamsAssigned(selectedTeams) {
    let assignedTeams = this.state.allTeams.filter((t) => {
      return selectedTeams.indexOf(t.team_id) > -1;
    });
    let teamsRowData = assignedTeams.map((team) => {
      return {
        team_id: team.team_id,
        team_name: team.team_name ? team.team_name : team.name,
        school: team.school ? team.school : '',
        team_location: team.state ? team.state : '',
        added_on: '--'
      };
    });
    this.setState({
      loading: false,
      isTeamModalOpen: false,
      teamsAssignLoading: false,
      teamsRowData: teamsRowData,
      assignBtnLabel: 'Assign'
    });
  }

  getAllTeams() {
    this.sbs.getAllTeams().then((response) => {
      this.setState({
        allTeamsLoading: false,
        allTeams: response
      });
    });
  }

  setData() {
    let teamsRowData = [];
    this.props.teams.map((team) => {
      teamsRowData.push({
        team_id: team.team_id,
        team_name: team.team_name ? team.team_name : team.name,
        school: team.school ? team.school : '',
        team_location: team.state ? team.state : '',
        added_on: '--'
      });
    });

    this.setState({
      loading: false,
      teamsRowData: teamsRowData
    });
  }

  componentDidMount() {
    this.setData();
    var _this = this;
    setTimeout(() => {
      _this.getAllTeams();
    }, 1000);
  }

  render() {
    return (
      <div className="team-inner-container">
        <div className="inner-header">
          <h2>Assigned Teams</h2>
          <div className="d-flex">
            <Button
              appearance="primary"
              className="custom-btn"
              onClick={this.addTeamModal.open}
            >
              ADD TEAM
            </Button>
            {this.addTeamModal.renderDOM()}
            <div className="content-search">
              <div className="search-icon">
                <SearchIcon />
              </div>
              <TextField
                placeholder="Search"
                label="hidden label"
                isLabelHidden
              />
            </div>
          </div>
        </div>
        <div className="inner-tab-grid">{this.getGridData()}</div>
      </div>
    );
  }
}

export default AssignedTeam;
