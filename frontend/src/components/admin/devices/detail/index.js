import React, { Component } from 'react';
import styled from 'styled-components';
import Tabs from '@atlaskit/tabs';
import Spinner from '@atlaskit/spinner';
import DeviceInformation from './information';
import StreamingData from './streaming-data';
import AssignedTeam from './assigned-teams';
import ArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left';

import ScoreBirdService from '../../../../services/scorebird.service';
import moment from 'moment';

const Content = styled.div`
  padding: 0px;
  width: 100%;
`;

const Header = (props) => (
  <div className="content-header">
    <div className="content-header-inner">
      <div className="left-panel">
        <div className="content-label" onClick={props.onClickBack}>
          <span className="back-icon" to="/devices">
            <ArrowLeftIcon />
          </span>
          Devices {props.deviceData.serial}
        </div>
      </div>
      <div className="right-panel">
        <div className="content-filters" />
      </div>
    </div>
  </div>
);

class DeviceDetail extends Component {
  constructor(props) {
    super(props);
    this.sbs = new ScoreBirdService();
    this.groupsById = {};
    this.state = {
      loading: true,
      deviceId: -1,
      groups: [],
      teams: [],
      info: {},
      tabs: [],
      deviceId: '',
      location: '--'
    };
  }

  onClickBack = () => {
    this.props.history.push('/devices');
  };

  init() {
    const {
      match: { params }
    } = this.props;
    this.setState({
      deviceId: params.deviceId,
      loading: true
    });
    this.getDeviceDetails(params.deviceId);
  }

  componentDidMount() {
    this.init();
  }

  afterSave = (data) => {
    let { device } = data;
    let device_info = this.state.info;
    device_info.name = device.name;
    device_info.groups = device.groups;
    device_info.sport = device.sport;
    device_info.notes = device.notes;
    device_info.score_board = device.score_board;
    if (data.newGroups) {
      this.sbs.groupsData = [...this.sbs.groupsData, ...data.newGroups];
    }
    this.init();
  };

  getDeviceDetails(deviceId) {
    let _this = this;
    this.sbs.getGroupsData().then((response1) => {
      let groups = this.sbs.groupsData;

      // let groups = window.ScoreBirdService.groups;
      groups.forEach((g) => {
        _this.groupsById[g.id] = g.text;
      });

      _this.setState({
        groups: groups
      });

      this.sbs.getDeviceDetails(deviceId).then((response) => {
        let deviceInfo = response.deviceInfo;
        if (deviceInfo.last_online_ts) {
          let selectedDate = moment(deviceInfo.last_online_ts).format(
            'MM/DD/YYYY, h:mm:ss a'
          );
          if (selectedDate === 'Invalid date') {
            deviceInfo.last_online_ts = '-  -   -';
          } else {
            deviceInfo.last_online_ts = selectedDate;
          }
        }
        // last updated
        if (deviceInfo.last_update_ts) {
          let selectedDate = moment(deviceInfo.last_update_ts).format(
            'MM/DD/YYYY, h:mm:ss a'
          );
          if (selectedDate === 'Invalid date') {
            deviceInfo.last_online_ts = '-  -   -';
          } else {
            deviceInfo.last_update_ts = selectedDate;
          }
        }

        deviceInfo.group_names = '--';
        if (deviceInfo.groups) {
          let groups2 = deviceInfo.groups.map((g) => {
            return _this.groupsById[g];
          });
          deviceInfo.group_names = groups2.length ? groups2.join(', ') : '--';
          deviceInfo.group_names =
            deviceInfo.group_names === ',' ? '--' : deviceInfo.group_names;
        }

        this.setState({
          loading: false,
          info: deviceInfo,
          teams: response.teams,
          tabs: [
            {
              label: 'Device Details',
              content: (
                <Content>
                  <DeviceInformation
                    info={deviceInfo}
                    afterSave={this.afterSave}
                    groups={groups}
                  />
                </Content>
              )
            },
            {
              label: 'Assigned Teams',
              content: (
                <div className="teams-grid">
                  <Content>
                    <AssignedTeam
                      teams={response.teams}
                      deviceInfo={response.deviceInfo}
                    />
                  </Content>
                </div>
              )
            },
            {
              label: 'Streaming Data',
              content: (
                <Content>
                  <StreamingData deviceInfo={response.deviceInfo} />
                </Content>
              )
            }
          ],
          deviceId: response.deviceInfo.serial
        });
      });
    });
  }

  renderHeader() {
    if (!this.state.loading) {
      return (
        <Header onClickBack={this.onClickBack} deviceData={this.state.info} />
      );
    } else {
      return '';
    }
  }

  renderContent() {
    if (this.state.loading) {
      return (
        <div className="loader">
          <Spinner size="large" />
        </div>
      );
    } else {
      return (
        <div style={{ width: '100%', height: '100%' }}>
          <Tabs
            tabs={this.state.tabs}
            onSelect={(tab, index) => console.log('Selected Tab', index + 1)}
          />
        </div>
      );
    }
  }

  render() {
    return (
      <div className="content-container">
        <div className="content-header-outer">{this.renderHeader()}</div>
        {this.renderContent()}
      </div>
    );
  }
}

export default DeviceDetail;
