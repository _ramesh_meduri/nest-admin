import React, { Component } from 'react';
import { AgGridReact } from 'ag-grid-react';
import ScoreBirdService from '../../../../services/scorebird.service';
import Spinner from '@atlaskit/spinner';

class StreamingData extends Component {
  constructor(props) {
    super(props);
    this.sbs = new ScoreBirdService();
    this.deviceInfo = this.props.deviceInfo;
    this.state = {
      selectedDeviceId: -1,
      loading: true,
      groups: [],
      columnDefs: [
        { headerName: 'Name', field: 'stream_name' },
        { headerName: 'Team Grade', field: 'team_grade' },
        { headerName: 'Sport', field: 'sport' },
        { headerName: 'School', field: 'school' },
        { headerName: 'State', field: 'state' },
        { headerName: 'Notes', field: 'notes' }
      ],
      defaultColDef: {
        width: 100,
        editable: false,
        lockPosition: true,
        filter: 'agTextColumnFilter'
      },
      rowData: []
    };
  }

  init() {
    this.sbs.getGroupsData().then((groupsResponse) => {
      let groups = this.sbs.groupsData;
      groups.forEach((g) => {
        this.groupsById[g.id] = g.text;
      });
      this.setState({
        groups: groups
      });

      this.sbs.getDeviceStreamData(this.deviceInfo.serial).then((response) => {
        let streamData = response;
        let stream_rows = [];
        if (!streamData.length) {
          stream_rows = [
            {
              stream_name: 'Dummy name',
              team_grade: 'varsity',
              sport: 'football',
              school: 'Dysart',
              state: 'AZ',
              notes: 'Lorem epsum notes'
            },
            {
              stream_name: 'Dysart Varsity',
              team_grade: 'freshman',
              sport: 'football',
              school: 'Dysart',
              state: 'AZ',
              notes: 'Lorem epsum notes'
            },
            {
              stream_name: 'Dysart Varsity',
              team_grade: 'freshman',
              sport: 'football',
              school: 'Dysart',
              state: 'AZ',
              notes: 'Lorem epsum notes'
            }
          ];
        } else {
          stream_rows = streamData.map((d) => {
            return {
              stream_name: d.name,
              team_grade: d.team_grade || '--',
              sport: d.sport,
              school: d.school,
              state: d.state,
              notes: 'Lorem epsum notes'
            };
          });
        }

        this.setState({
          loading: false,
          rowData: stream_rows
        });
      });
    });
  }

  componentDidMount() {
    this.init();
  }

  reset() {
    this.setState({
      loading: true,
      groups: [],
      rowData: []
    });
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
    window.addEventListener('resize', function() {
      setTimeout(function() {
        params.api.sizeColumnsToFit();
      });
    });

    params.api.sizeColumnsToFit();
  };

  render() {
    var noRowsTemplate = '<span>No devices to show.</span>';
    if (this.state.loading) {
      return (
        <div className="stream-content-box">
          <div className="loader">
            <Spinner size="large" />
          </div>
          ;
        </div>
      );
    } else {
      return (
        <div>
          <div
            className="ag-theme-material ag-custom-styles device-grid"
            style={{
              height: 'calc(100vh - 60px)',
              width: '100%'
            }}
          >
            <AgGridReact
              columnDefs={this.state.columnDefs}
              defaultColDef={this.state.defaultColDef}
              enableColResize={true}
              enableSorting={true}
              enableFilter={true}
              floatingFilter={true}
              rowHeight="48"
              suppressDragLeaveHidesColumns={true}
              suppressRowClickSelection={true}
              rowData={this.state.rowData}
              onGridReady={this.onGridReady}
              pagination={true}
              paginationPageSize="20"
              overlayNoRowsTemplate={noRowsTemplate}
            />
          </div>
        </div>
      );
    }
  }
}

export default StreamingData;
