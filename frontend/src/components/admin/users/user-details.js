import React, { Component } from 'react';
import Form, {
  Field,
  FieldGroup,
  FormHeader,
  FormSection,
  FormFooter,
  Validator,
  Fieldset,
  CheckboxField
} from '@atlaskit/form';
import Button from '@atlaskit/button';
import ArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left';
import ScorebirdService from '../../../services/scorebird.service';
import AddUserForm from './add-userform';
import Spinner from '@atlaskit/spinner/dist/cjs/Spinner';

const Header = (props) => (
  <div className="content-header">
    {!props.isEdit ? (
      <div className="content-header-inner header-row">
        <div className="left-panel back-with-heading">
          <div className="back-to-text" onClick={props.onClickBack}>
            <span>
              <ArrowLeftIcon />
            </span>
            <b> Go Back To Users </b>
          </div>
        </div>
        <div className="right-panel">
          {!props.isEdit ? (
            <Button
              appearance="primary"
              className="custom-btn"
              onClick={props.userEditForm}
            >
              Edit User Details{' '}
            </Button>
          ) : null}
        </div>
      </div>
    ) : null}
  </div>
);

class UserDetails extends Component {
  constructor(props) {
    super(props);
    this.sbs = new ScorebirdService();
    this.state = {
      isEdit: false,
      UserDetails: {},
      user_Id: '',
      loading: true
    };
  }

  componentDidMount() {
    this.init();
  }

  init = () => {
    let userIdIndex = this.props.location.pathname.lastIndexOf('/');
    let userId = this.props.location.pathname.substring(
      userIdIndex + 1,
      this.props.location.pathname.length
    );

    userId.toString();
    console.log('userid', userId);
    console.log('userdetails params ', userId);
    this.sbs.getUserData(userId).then((response) => {
      let UserDetails = response;
      let UserDetailsId = response[0];
      console.log('userdetails params ', UserDetails);

      this.setState({
        UserDetails: UserDetails,
        user_Id: UserDetailsId.unique_id,
        loading: false
      });

      // this.props.dispatch({
      //   type: 'CURRENT_PATH',
      //   current_component: 'users'
      // });
    });
  };

  userEditForm = () => {
    this.setState({
      isEdit: !this.state.isEdit
    });
  };

  onClickBack = () => {
    this.props.history.push('/users');
  };

  renderform() {
    var normalView = {
      display: this.state.isEdit ? 'none' : ' '
    };
    var editView = {
      display: this.state.isEdit ? ' ' : 'none'
    };

    console.log('------------renderform--------------');
    console.log(this.state.loading);
    console.log('-------------renderform-------------');

    return (
      <div>
        {this.state.loading ? (
          <div className="loader">
            <Spinner size="large" />
          </div>
        ) : (
          <div className="school-stadium-info">
            <div />
            <div style={normalView}>
              <div className="view-info">
                <div className="non-edit-field">
                  <label>First Name </label>
                  <div className="field-value">
                    {' '}
                    {this.state.UserDetails[0].first_name}
                  </div>
                </div>

                <div className="non-edit-field">
                  <label> Last Name</label>
                  <div className="field-value">
                    {' '}
                    {this.state.UserDetails[0].last_name}
                  </div>
                </div>

                <div className="non-edit-field">
                  <label>Email </label>
                  <div className="field-value">
                    {' '}
                    {this.state.UserDetails[0].email}
                  </div>
                </div>

                <div className="non-edit-field">
                  <label> Mobile</label>
                  <div className="field-value">
                    {' '}
                    {this.state.UserDetails[0].phone_number
                      ? this.state.UserDetails[0].phone_number
                      : '- -'}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}

        {this.state.loading && !this.state.isEdit ? (
          true
        ) : (
          <div className="edit-info" style={editView}>
            <AddUserForm
              type="edit"
              cancel={this.userEditForm}
              userData={this.state.UserDetails}
              init={this.init}
            />
          </div>
        )}
      </div>
    );
  }

  render() {
    return (
      <div className="full-width-container">
        <Header
          userEditForm={this.userEditForm}
          isEdit={this.state.isEdit}
          onClickBack={this.onClickBack}
        />
        <div style={{ width: '100%', height: '100%' }}>{this.renderform()}</div>
      </div>
    );
  }
}

export default UserDetails;
