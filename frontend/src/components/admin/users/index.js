import React, { Component } from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import { Redirect } from 'react-router-dom';
import Button from '@atlaskit/button';
import TextField from '@atlaskit/field-text';
import SearchIcon from '@atlaskit/icon/glyph/search';
import AddnewUser from './add-userform';
import Spinner from '@atlaskit/spinner';
import ScorebirdService from '../../../services/scorebird.service';

const Header = (props) => (
  <div className="content-header">
    <div className="content-header-inner header-row">
      <div className="left-panel">
        <h1 className="content-label">Users</h1>
      </div>
      <div className="right-panel">
        <div>
          <Button
            appearance="default"
            className="custom-btn"
            onClick={props.toggleEdit}
          >
            Add User
          </Button>
        </div>
        <div className="content-search">
          <div className="search-icon">
            <SearchIcon />
            <TextField
              placeholder="Search"
              label="hidden label"
              isLabelHidden
            />
          </div>
        </div>
      </div>
    </div>
  </div>
);

class Users extends Component {
  constructor(props) {
    super(props);
    this.sbs = new ScorebirdService();
    this.state = {
      isEdit: false,
      redirect: false,
      selectedUser: '',
      columnDefs: [
        { headerName: 'USER NAME', field: 'first_name' },
        { headerName: 'EMAIL', field: 'email' },
        { headerName: 'SCHOOL NAME', field: 'school' }
      ],
      DefaultcolDef: {
        width: 100,
        editable: true,
        lockPosition: true,
        filter: 'agTextColumnFilter'
      },
      rowData: [],
      schoolId: '',
      loading:true
    };
  }

  componentDidMount() {
    this.init();
  }

  init = () => {
    this.sbs.getUsers().then((res) => {
      this.setState({ rowData: res.data.data ,loading: false});
    });
  };

  onRowClicked = (event) => {
    console.log(event.data.unique_id);
    let userId = event.data.unique_id;
    this.setRowRedirect(userId);
  };

  toggleEdit = () => {
    this.setState({
      isEdit: !this.state.isEdit
    });
  };

  getGridData() {
    var normalView = {
      display: this.state.isEdit ? 'none' : ' '
    };

    var editView = {
      display: this.state.isEdit ? ' ' : 'none'
    };

    return (
      <div className="ag-theme-material ag-custom-styles device-grid">
        <div />
        {this.state.loading ? (
          <div className="loader">
            <Spinner size="large" />
          </div>
        ) : (
      <div className="facility-info-container">
        <div className="user-normal-view" style={normalView}>
          <div
            className="ag-theme-material ag-custom-styles device-grid"
            style={{
              height: 'calc(100vh - 60px)',
              width: '100%'
            }}
          >
            <div className="contai" />
            <AgGridReact
              enableSorting={true}
              pagination={true}
              enableColResize={true}
              paginationPageSize="30"
              rowHeight="48"
              headerHeight="36"
              columnDefs={this.state.columnDefs}
              rowData={this.state.rowData}
              onGridReady={this.onGridReady.bind(this)}
              onRowClicked={this.onRowClicked}
            />
          </div>
        </div>
        {this.state.isEdit ? (
          <div className="user-Edit-view" style={editView}>
            <form>
              <AddnewUser
                schoolId={this.state.schoolId}
                toggleEdit={this.toggleEdit}
                updateUserList={this.init}
              />
            </form>
          </div>
        ) : null}
      </div>
        )}
      </div>
    );
  }

  setRowRedirect = (userId) => {
    this.setState({
      selectedUser: userId,
      redirect: true
    });
  };

  renderRowRedirect = () => {
    if (this.state.redirect) {
      let userId = this.state.selectedUser;
      return <Redirect to={'/usersDetails/' + userId} />;
    }
  };

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
    window.addEventListener('resize', function() {
      setTimeout(function() {
        params.api.sizeColumnsToFit();
      });
    });
    params.api.sizeColumnsToFit();
  }

  render() {
    return (
      <div>
        <Header adduserform={this.adduserform} toggleEdit={this.toggleEdit} />
        <div style={{ width: '100%', height: '100%' }}>
          {this.getGridData()}
          {this.renderRowRedirect()}
          <div style={{ width: '40px', height: '15px', float: 'right' }}> </div>
        </div>
      </div>
    );
  }
}

// function mapStateToProps(state) {
//   return {
//     displayContainerNav: state.layout.displayContainerNav,
//     school_id: state.schools.school_id
//   };
// }

// class Modal extends React.Component {
//   state = {
//     schoolName: ''
//   };
//   schoolName = (e) => {
//     this.setState({
//       schoolName: e.value
//     });
//   };
//   render() {
//     if (!this.props.show) {
//       return null;
//     }
//     return (
//       <div className="backdrop">
//         <div className="modal">
//           <div className="modal-header">
//             <p className="address-type">Add New School</p>
//           </div>
//           <div className="modal-body">
//             <TextField
//               label="School Name "
//               name="school_name"
//               id="school_name"
//               required={true}
//             />
//           </div>
//           <div className="footer"></div>
//         </div>
//       </div>
//     );
//   }
// }

// Modal.propTypes = {
//   onClose: PropTypes.func.isRequired,
//   show: PropTypes.bool,
//   children: PropTypes.node
// };

export default Users;
