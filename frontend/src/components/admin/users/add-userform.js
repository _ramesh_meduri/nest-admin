import React, { Fragment, Component } from 'react';
import Select from '@atlaskit/select';
import TextField from '@atlaskit/textfield';
import Form, { Field, ErrorMessage } from '@atlaskit/form';
import Button from '@atlaskit/button';
import ArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left';
import ScorebirdService from '../../../services/scorebird.service';
import { Auth, API } from 'aws-amplify';
//

const school = [
  { label: 'Belton High School', value: 'Belton High School' },
  { label: 'Ellison High School', value: 'Ellison High School' }
];
const role = [
  { label: 'Belton ', value: 'Belton High School' },
  { label: 'Ellison ', value: 'Ellison High School' }
];
const referred = [
  { label: '1234 ', value: 'Belton High School' },
  { label: '4567 ', value: 'Ellison High School' }
];

class AddUserForm extends Component {
  constructor(props) {
    super(props);
    this.sbs = new ScorebirdService();
    console.log('122', this.props.userData);
    this.state = {
      flags: {
        submitted: false
      },
      useraccount: {
        first_name: '',
        last_name: '',
        email: '',
        phone_number: ''
      },
      schools_data: [],
      loading: true,
      // first_nameType: this.props.type === 'edit' && this.props.userData ? this.props.userData[0] : '',
      emailType:
        this.props.type === 'edit' && this.props.userData.length
          ? this.props.userData[0].email
          : '',
      first_nameType:
        this.props.type === 'edit' && this.props.userData.length
          ? this.props.userData[0].first_name
          : '',
      last_nameType:
        this.props.type === 'edit' && this.props.userData.length
          ? this.props.userData[0].last_name
          : '',
      phone_numberType:
        this.props.type === 'edit' && this.props.userData.length
          ? this.props.userData[0].phone_number
          : ''
      // referred_byType: this.props.type === 'edit' && this.props.userData.length ? { label: this.props.userData[0].referred_by, value: this.props.userData[0].referred_by } : '',
      // user_roleType: this.props.type === 'edit' && this.props.userData.length ? { label: this.props.userData[0].user_role, value: this.props.userData[0].user_role } : '',
    };
  }

  signUp() {
    this.setState({
      flags: { submitted: true }
    });
    if (
      this.state.state &&
      this.state.city &&
      this.state.school.sel_school_id &&
      this.state.school.physical_address &&
      this.state.school.physical_city &&
      this.state.school.physical_zip &&
      this.state.school.mailing_address &&
      this.state.school.mailing_city &&
      this.state.school.mailing_zip
    ) {
      if (this.state.account.password !== this.state.account.confirm_password) {
        this.setState({ error: 'Passwords do not match' });
        return;
      }

      // validationData needs to be sent as array of k/v pairs [ { Name: "some name", Value: "some value" },... ]
      var school_kv_pairs = [];
      // console.log(Object.entries(this.state.school));
      Object.entries(this.state.school).forEach((item) => {
        if (item[0] === 'city') {
        } else if (item[0] === 'sel_school') {
          if (!item[1].value) {
            var schoolName = item[1];
          }
          school_kv_pairs.push({
            Name: item[0],
            Value: item[1].value
              ? item[1].value.toString()
              : schoolName[0].value.toString()
          });
        } else {
          school_kv_pairs.push({ Name: item[0], Value: item[1].toString() });
        }
      });

      let accountDetails = {
        username: this.state.account.email,
        password: this.state.account.password,
        attributes: {
          email: this.state.account.email,
          'custom:referred_by': this.state.account.referred_by,
          'custom:primary_contact': 'true',
          family_name: this.state.account.last_name,
          given_name: this.state.account.first_name
        },
        validationData: school_kv_pairs
      };

      let phone_number = this.state.account.phone
        ? `+1${this.state.account.phone.replace(/[-()]/g, '')}`
        : null;

      Auth.signUp(accountDetails) // all validation happens in AWS Cognito
        .then((data) => {
          let postParams = {
            body: {
              user: {
                email: this.state.account.email,
                cognito_id: data.userSub,
                first_name: this.state.account.first_name,
                last_name: this.state.account.last_name
              },
              school: this.state.school
            }
          };
          var userParams = [
            {
              email: this.state.account.email,
              first_name: this.state.account.first_name,
              last_name: this.state.account.last_name
              // phone_number: phone_number,
              // referred_by: this.state.account.referred_by,
              // org_name: this.state.account.org_name,
              // school_id: this.state.school.sel_school_id,
              // newSchool: this.state.newSchool,
              // city: this.state.city,
              // state: this.state.state,
              // schoolName: this.state.school.sel_school.value,
            }
          ];
          API.post('customerPortal', '/schools', postParams).then(
            (response) => {
              // console.log("response", response)
              this.setState({
                loading: true,
                signUpTxt: 'Signing Up...'
              });
              // if (!response.errors) {
              //   this.props.history.push({ pathname: "/login" });
              //   this.sbs.userSignUp(userParams)
              //   return;
              // }

              // handle any validation errors
            }
          );
        })
        .catch((err) => {
          if (typeof err == 'string') {
            this.setState({ error: this.processCognitoMsg(err) });
          } else {
            this.setState({ error: this.processCognitoMsg(err.message) });
          }
        });
    }
  }

  handleChange = (target, type) => {
    if (type === 'user') {
      let useraccount = this.state.useraccount;
      useraccount[target.name] = target.value;
      console.log('print here account details', useraccount);
      // console.log("print here values",account)
      this.setState({
        useraccount: useraccount
      });
    }
  };

  selectSchool = (e) => {
    let schoolDetails = {
      ...this.state.school,
      ...{
        sel_school_id: e.value,
        sel_school: { label: e.label, value: e.label },
        school_not_found: false,
        other_school_name: '',
        is_firstname: ''
      }
    };
  };

  componentDidMount() {
    console.log('print here props', this.props);
  }

  changeState = (e) => {
    // console.log("print enterpi", e)
  };

  backToUser = () => {
    this.props.history.push('/users');
  };

  validateFields = (value) => {
    if (value.length === 0) {
      return 'empty';
    }
  };

  render() {
    // console.log(" print her user details ==      ", this.props)
    // console.log(" print her user details ==>", this.state.first_nameType.value)

    return (
      <div>
        <div className="content-header">
          <div className="content-header-inner header-row">
            <div className="left-panel back-with-heading">
              {this.props.type == 'edit' ? (
                <div className="back-to-text" onClick={this.props.cancel}>
                  <span className="back-icon">
                    <ArrowLeftIcon />
                  </span>
                  <b>Go Back To User </b>
                </div>
              ) : (
                <div className="back-to-text" onClick={this.props.toggleEdit}>
                  <span className="back-icon">
                    <ArrowLeftIcon />
                  </span>
                  <b>Go Back To Users </b>
                </div>
              )}
            </div>
            <div className="right-panel" />
          </div>
        </div>
        <div className="add-form">
          <Form
            onSubmit={(data) => {
              if (this.props.type === 'edit') {
                let userData = [
                  {
                    email: data.email,
                    first_name: data.first_name,
                    // first_name: {key:'value'},
                    last_name: data.last_name,
                    phone_number: data.phone_number,
                    unique_id: this.props.userData[0].unique_id
                    // referred_by: data.referred.value,
                    // user_role: data.role.value,
                  }
                ];

                // console.log('form data user  form facility ', userData);

                this.sbs.updateUserInfo(userData).then(() => {
                  this.props.init();
                  this.props.cancel();
                });
              } else {
                this.signUp();
                let userinfo = [
                  {
                    email: data.email,
                    first_name: data.first_name,
                    // first_name: ['haneef','value'],
                    last_name: data.last_name,
                    phone_number: data.phone_number,
                    school_id: this.props.schoolId
                  }
                ];
                this.sbs.adduserFromUserpage(userinfo).then(() => {
                  this.props.updateUserList();
                  this.props.toggleEdit();
                });
              }
            }}
          >
            {({ formProps }) => (
              <form {...formProps}>
                {/* <Field name="school" label="Organization/School Name">
                  {({ fieldProps: { id, ...rest } }) => (
                    <Fragment>
                      <Select
                        inputId={id}
                        placeholder="Enter Organization/School Name"
                        {...rest}
                        onChange={this.selectSchool}
                      // options={this.state.schools_data}
                      />
                    </Fragment>
                  )}
                </Field> */}
                <div className="scrollable1">
                  <div className="user-details">
                    {/* {console.log("print ", this.props)} */}
                    <Field
                      name="first_name"
                      label="FirstName"
                      defaultValue={this.state.first_nameType}
                      value={this.state.first_nameType}
                      // validate={this.validateFields}
                    >
                      {({ fieldProps, error }) => (
                        <>
                          {' '}
                          <TextField
                            placeholder="Enter First Name"
                            {...fieldProps}
                            onKeyUp={(e) => {
                              this.handleChange(e.target, 'user');
                            }}
                          />
                          {error === 'empty' && (
                            <ErrorMessage>
                              Please enter First Name{' '}
                            </ErrorMessage>
                          )}
                        </>
                      )}
                    </Field>
                    <Field
                      name="last_name"
                      label="LastName"
                      defaultValue={this.state.last_nameType}
                      value={this.state.last_nameType}
                      // validate={this.validateFields}
                    >
                      {({ fieldProps, error }) => (
                        <>
                          {' '}
                          <TextField
                            placeholder="Enter Last Name"
                            {...fieldProps}
                            onKeyUp={(e) => {
                              this.handleChange(e.target, 'user');
                            }}
                          />
                          {error === 'empty' && (
                            <ErrorMessage>Please enter Last Name </ErrorMessage>
                          )}
                        </>
                      )}
                      {/* {({ fieldProps,error }) => <TextField placeholder="Enter Last Name" {...fieldProps} onKeyUp={(e) => this.handleChange(e.target, 'user')}  />} */}
                    </Field>
                    <Field
                      name="email"
                      label="Email"
                      defaultValue={this.state.emailType}
                      value={this.state.emailType}
                      // validate={this.validateFields}
                    >
                      {({ fieldProps, error }) => (
                        <>
                          {' '}
                          <TextField
                            placeholder="Enter Email"
                            {...fieldProps}
                            onKeyUp={(e) => {
                              this.handleChange(e.target, 'user');
                            }}
                          />
                          {error === 'empty' && (
                            <ErrorMessage>Please enter Email </ErrorMessage>
                          )}
                        </>
                      )}
                      {/* {({ fieldProps }) => <TextField placeholder="Enter Email" {...fieldProps} onKeyUp={(e)=>this.handleChange(e.target,'user')}  />} */}
                    </Field>
                    <Field
                      name="phone_number"
                      label="Mobile"
                      defaultValue={this.state.phone_numberType}
                      value={this.state.phone_numberType}
                      // validate={this.validateFields}
                    >
                      {({ fieldProps, error }) => (
                        <>
                          {' '}
                          <TextField
                            placeholder="Enter Mobile Number"
                            {...fieldProps}
                            onKeyUp={(e) => {
                              this.handleChange(e.target, 'user');
                            }}
                          />
                          {error === 'empty' && (
                            <ErrorMessage>
                              Please enter phone number{' '}
                            </ErrorMessage>
                          )}
                        </>
                      )}
                      {/* {({ fieldProps }) => <TextField placeholder="Enter Mobile Number" {...fieldProps} onKeyUp={(e)=>this.handleChange(e.target,'user')} />} */}
                    </Field>
                    {/* <Field name="role" label="UserRole"
                      defaultValue={this.state.user_roleType.value}
                      value={this.state.user_roleType}>
                      {({ fieldProps: { id, ...rest } }) => (
                        <Fragment>
                          <Select
                            inputId={id}
                            {...rest}
                            placeholder="Admin"
                            options={role}
                          />
                        </Fragment>
                      )}
                    </Field>
                    <Field name="referred" label="Refrred By" value='ll'
                      defaultValue={this.state.referred_byType.value}
                      value={this.state.referred_byType}
                    >
                      {({ fieldProps: { id, ...rest } }) => (
                        <Fragment>
                          <Select
                            inputId={id}
                            {...rest}
                            placeholder="Enter Organization/School Name"
                            options={referred}
                          />
                        </Fragment>
                      )}
                    </Field> */}
                  </div>
                </div>
                <div className="footer-actions">
                  <div />
                  <div className="actions">
                    {this.props.type == 'edit' ? (
                      <Button
                        type="button"
                        appearance="default"
                        onClick={this.props.cancel}
                      >
                        Cancel
                      </Button>
                    ) : (
                      <Button
                        type="button"
                        appearance="default"
                        onClick={this.props.toggleEdit}
                      >
                        Cancel
                      </Button>
                    )}
                    <Button type="submit" appearance="primary">
                      Save
                    </Button>
                    {/* <Button type="submit"appearance="primary">Save</Button> */}
                  </div>
                </div>
              </form>
            )}
          </Form>
        </div>
      </div>
    );
  }
}

export default AddUserForm;
