import React, { Fragment, Component } from 'react';
import facilityLogo from '../../../assets/images/menu-icons/score_board.svg';

class UsersLeftNav extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="left-nav">
        <div className="leftnav-header">
          <img src={facilityLogo} alt="facility logo" />
          <h3>Users(83)</h3>
        </div>
        <div className="nav-actions">
          <div className="status">
            <label>All Teams</label>
          </div>
          <div className="status">
            <label>Belton High School </label>
          </div>
          <div className="status">
            <label>North san Mtn. High School</label>
          </div>
          <div className="status">
            <label>Crossville High School</label>
          </div>
          <div className="status">
            <label>North san Mtn. High School</label>{' '}
          </div>
        </div>
      </div>
    );
  }
}

export default UsersLeftNav;
