import React, { Fragment } from 'react';
import { Route, Switch, Redirect, NavLink } from 'react-router-dom';
import logo from '../../assets/images/SB_Logo.svg';
import '../../App.scss';
import GlobalNavigation from '@atlaskit/global-navigation';
import { AtlassianIcon } from '@atlaskit/logo';
import DashboardIcon from '@atlaskit/icon/glyph/dashboard';
import ScreenIcon from '@atlaskit/icon/glyph/screen';
import EmojiActivityIcon from '@atlaskit/icon/glyph/emoji/activity';
import SignOutIcon from '@atlaskit/icon/glyph/sign-out';
import facilitiesIcon from '../../assets/images/menu-icons/facilities.svg';
import usersIcon from '../../assets/images/menu-icons/users.svg';

import {
  HeaderSection,
  Item,
  LayoutManager,
  MenuSection,
  NavigationProvider
} from '@atlaskit/navigation-next';

import Dashboard from './dashboard';
import Devices from './devices';
import DeviceDetail from './devices/detail';
import Games from './games';
import GameDetail from './games/detail';
import FacilityList from './facilities/facilityList';
import FacilityItem from './facilities/facilityItem';
import AddFacility from './facilities/addFacility';
import Users from './users/index';
import UserDetails from '../admin/users/user-details';
import sportIcon from '../../assets/images/menu-icons/teams.svg';
import AddSport from './Sports/addSport';
import EditSport from './Sports/editSport';
import Sports from './Sports/index';

const MyGlobalNavigation = () => (
  <div className="scorebird-global-nav">
    <GlobalNavigation
      productIcon={() => <AtlassianIcon size="medium" />}
      onProductClick={() => { }}
    />
  </div>
);

const MyProductNavigation = (props) => (
  <Fragment>
    <HeaderSection>
      {({ className }) => (
        <div className="logo-container">
          <NavLink to="/">
            <img src={logo} className="scorebird-logo" alt="logo" />
          </NavLink>
        </div>
      )}
    </HeaderSection>
    <MenuSection>
      {({ className }) => (
        <div className="navigation-links">
          <NavLink activeClassName="is-active" to="/" exact={true}>
            <Item text="Dashboard" before={DashboardIcon} isActive={false} />
          </NavLink>
          <NavLink activeClassName="is-active" to="/devices">
            <Item text="Devices" before={ScreenIcon} isActive={false} />
          </NavLink>
          <NavLink activeClassName="is-active" to="/games">
            <Item text="Games" before={EmojiActivityIcon} isActive={false} />
          </NavLink>
          <NavLink to="/facilities" onClick={props.onNavClick}>
            <Item
              text="Facilities"
              isActive={false}
              before={() => (
                <img className="custom-icon" src={facilitiesIcon} />
              )}
            />
          </NavLink>
          <NavLink to="/users" onClick={props.onNavClick}>
            <Item
              text="Users"
              isActive={false}
              before={() => <img className="custom-icon" src={usersIcon} />}
            />
          </NavLink>
          <NavLink to="/sports" onClick={props.onNavClick}>
            <Item
              text="Sport Types"
              isActive={false}
              before={() => <img className="custom-icon" src={sportIcon} />}
            />
          </NavLink>
          <div className="logout-link">
            <Item
              text="Log Out"
              before={SignOutIcon}
              isActive={false}
              onClick={() => props.afterLogout()}
            />
          </div>
        </div>
      )}
    </MenuSection>
  </Fragment>
);

const MainLayout = (props) => (
  <NavigationProvider initialUIController={{ isResizeDisabled: true }}>
    <div className="my-layout-outer">
      <LayoutManager
        globalNavigation={MyGlobalNavigation}
        productNavigation={() => (
          <MyProductNavigation afterLogout={props.afterLogout} />
        )}
        containerNavigation={null}
        collapseToggleTooltipContent={false}
      >
        <Switch>
          <Route exact path="/" component={Dashboard} />
          <Route path="/dashboard" component={Dashboard} />
          <Route path="/devices" component={Devices} />
          <Route path="/deviceDetail/:deviceId" component={DeviceDetail} />
          <Route path="/games" component={Games} />
          <Route path="/gameDetail/:gameId" component={GameDetail} />
          <Route path="/facilities" component={FacilityList} />
          <Route path="/addFacility" component={AddFacility} />
          <Route path="/facilityItem/:facilityId" component={FacilityItem} />
          <Route path="/users" component={Users} />
          <Route path="/usersDetails/:userId" component={UserDetails} />
          <Route path="/sports" component={Sports} />
          <Route path="/addSport" component={AddSport} />
          <Route path="/editSport/:sportId" component={EditSport} />
          <Redirect to="/" />
        </Switch>
      </LayoutManager>
    </div>
  </NavigationProvider>
);

const AdminPanel = ({ afterLogout }) => (
  <div className="App admin-panel">
    <MainLayout afterLogout={afterLogout} />
  </div>
);

export default AdminPanel;
