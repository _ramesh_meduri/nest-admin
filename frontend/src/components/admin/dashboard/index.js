import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import deviceBlueLogo from '../../../assets/images/device_blue.svg';
import deviceGreenLogo from '../../../assets/images/device_green.svg';
import gamesAquaLogo from '../../../assets/images/game_aqua.svg';
import gamesGreenLogo from '../../../assets/images/game_aqua.svg';
import charts from 'fusioncharts/fusioncharts.charts';
import ReactFC from 'react-fusioncharts';
import FusionCharts from 'fusioncharts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import ScoreBirdService from '../../../services/scorebird.service';
import Spinner from '@atlaskit/spinner';
import CountUp from 'react-countup';
import { AgGridReact } from 'ag-grid-react';
import Button from '@atlaskit/button';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';

ReactFC.fcRoot(FusionCharts, FusionTheme);
charts(FusionCharts);

class DevicesDonutChart extends Component {
  dataSource = {
    chart: {
      caption: '',
      paletteColors: '#29C29E,#FFBC39,#E2E4EA',
      aligncaptionwithcanvas: '0',
      captionpadding: '0',
      decimals: '',
      plottooltext: '',
      centerlabel: '',
      enableRotation: '0',
      startingAngle: '0',
      enableSlicing: '0',
      enableMultiSlicing: '0',
      enableSmartLabels: '1',
      showHoverEffect: '1',
      plotHoverEffect: '1',
      showToolBarButtonTooltext: '0',
      showLegend: '1',
      legendItemFont:
        '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen","Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",sans-serif;',
      legendItemFontSize: '14px',
      legendPosition: '',
      legendNumColumns: '0',
      'legendPosition Range': '1',
      legendAllowDrag: '0',
      drawCustomLegendIcon: '1',
      showPercentValues: '0',
      showLabels: '1',
      showValues: '1',
      showToolTip: '1',
      showPrintMenuItem: '0',
      showShadow: '0',
      skipOverlapLabels: '0',
      theme: 'fusion',
      legendItemFontBold: '1'
    },
    data: [
      {
        label: 'Online',
        value: this.props.deviceAnalytics.data.online_count
      },
      {
        label: 'Streaming',
        value: this.props.deviceAnalytics.data.streaming_count
      },
      {
        label: 'Offline',
        value: this.props.deviceAnalytics.data.offline_count
      }
    ]
  };

  render() {
    return (
      <div>
        <div className="device-header">
          <p>Devices Status</p>
        </div>
        <ReactFC
          type="doughnut2d"
          width="90%"
          height="40%"
          dataFormat="JSON"
          dataSource={this.dataSource}
        />
      </div>
    );
  }
}

class IntegrationsDonutChart extends Component {
  constructor(props) {
    super(props);
    this.dataSource1 = {
      chart: {
        caption: '',
        //"paletteColors":"#29C29E,#E2E4EA,#EE4B3C",
        captionpadding: '0',
        decimals: '',
        plottooltext: '',
        centerlabel: '',
        enableRotation: '0',
        startingAngle: '0',
        enableSlicing: '0',
        enableMultiSlicing: '0',
        enableSmartLabels: '1',
        showHoverEffect: '1',
        plotHoverEffect: '1',
        showToolBarButtonTooltext: '0',
        showLegend: '1',
        legendItemFont:
          '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen","Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",sans-serif;',
        legendItemFontSize: '14px',
        legendPosition: '',
        legendNumColumns: '0',
        'legendPosition Range': '1',
        legendAllowDrag: '0',
        drawCustomLegendIcon: '1',
        showPercentValues: '0',
        showLabels: '1',
        showValues: '1',
        showToolTip: '1',
        showPrintMenuItem: '0',
        showShadow: '0',
        skipOverlapLabels: '0',
        theme: 'fusion',
        legendItemFontBold: '1'
      },
      data: this.props.parterAnalytics.data
    };
  }

  render() {
    return (
      <div>
        <div className="device-header">
          <p> Integration patners </p>
        </div>
        <ReactFC
          type="doughnut2d"
          width="90%"
          height="40%"
          dataFormat="JSON"
          dataSource={this.dataSource1}
        />
      </div>
    );
  }
}

const Header = (props) => (
  <div className="content-header">
    <div className="content-header-inner">
      <div className="left-panel">
        <div className="content-label">Dashboard</div>
      </div>
      <div className="right-panel" />
    </div>
  </div>
);

const gameNameColrenderer = (props) => (
  <NavLink to={'/gameDetail/' + props.data.game_id}>{props.value}</NavLink>
);

class Dashboard extends Component {
  current_date =
    new Date().getFullYear() +
    '/' +
    (new Date().getMonth() + 1) +
    '/' +
    new Date().getDate();
  deviceAnalytics = null;
  gamesAnalytics = null;

  constructor(props) {
    super(props);
    this.state = {
      gamesByStatus: {
        loading: true,
        gamesByStatus: {}
      },
      Schedulegames: [],
      deviceAnalytics: {
        loading: true,
        data: {
          online_count: 0,
          offline_count: 0,
          livegames_count: 0,
          Streaming_count: 0
        }
      },
      parterAnalytics: {
        loading: true,
        data: {}
      },
      gamesTodayLoading: true,
      isFinalizeModalOpened: false,
      modalFinalizeHeading: 'FINALIZE GAME',
      modalFinalizeText: 'Do you really want to finalize this game?',
      showMoreGamesLink: false,
      gamesGrid: {
        columnDefs: [
          {
            headerName: 'Game',
            field: 'game_name',
            cellClass: function(params) {
              return 'game_name_col ' + params.data.game_icon;
            },
            tooltip: (params) => params.value,
            cellRendererFramework: gameNameColrenderer
          },
          {
            headerName: 'Sport',
            field: 'sport',
            tooltip: (params) => params.value
          },
          {
            headerName: 'Start Time',
            field: 'start_time',
            tooltip: (params) => params.value
          },
          {
            headerName: 'Home Team',
            field: 'home_team',
            tooltip: (params) => params.value
          },
          {
            headerName: 'Away Team',
            field: 'away_team',
            tooltip: (params) => params.value
          },
          // { headerName: "Group Name", field: "group_name" },
          {
            headerName: 'Device Serial',
            field: 'device_name',
            tooltip: (params) => params.value
          },
          { headerName: 'Game Snapshot', field: 'game_shopshot' },
          {
            headerName: 'Actions',
            field: '',
            cellRendererFramework: this.gameActionColrenderer,
            suppressFilter: true
          }
        ],
        defaultColDef: {
          width: 100,
          editable: false,
          lockPosition: true
        },
        rowData: []
      }
    };
    this.sbs = new ScoreBirdService();
  }

  componentDidMount() {
    this.sbs.getDashboardAnalytics().then((response) => {
      this.deviceAnalytics = response.devices;
      let deviceAnalytics = {
        loading: false,
        data: this.deviceAnalytics
      };
      this.setState({
        deviceAnalytics: deviceAnalytics,
        Dashboard: this.props.iconcount
      });
    });

    let selected_date = this.selected_date;
    // let selected_date = '2018/10/05';

    this.sbs.getGamesData(selected_date).then((response) => {
      let gamesData = this.sbs.gamesData[selected_date];

      let gamesByStatus = {};
      let gamesByPartner = {};
      gamesData.map((g) => {
        let status = g.game_icon ? g.game_icon : 'no-icon';
        if (!gamesByStatus[status]) {
          gamesByStatus[status] = [];
        }
        gamesByStatus[status].push(g);
        if (!gamesByPartner[g.partner_id]) {
          gamesByPartner[g.partner_id] = [];
        }
        gamesByPartner[g.partner_id].push(g);
      });

      let partners = Object.keys(gamesByPartner);
      let partners_data = [];

      partners.map((partner) => {
        partners_data.push({
          label: partner,
          value: gamesByPartner[partner].length
        });
      });
      let showMoreGamesLink = gamesData.length > 5;
      gamesData = gamesData.slice(0, 5);

      this.setState({
        gamesGrid: {
          ...this.state.gamesGrid,
          rowData: gamesData
        },
        parterAnalytics: {
          loading: false,
          data: partners_data
        },
        gamesByStatus: {
          loading: false
        },
        gamesByStatus: gamesByStatus,
        gamesTodayLoading: false,
        showMoreGamesLink: showMoreGamesLink
      });
    });
  }

  getDevicesChart() {
    if (this.state.deviceAnalytics.loading) {
      return (
        <div className="loader">
          <Spinner size="large" />
        </div>
      );
    } else {
      return <DevicesDonutChart deviceAnalytics={this.state.deviceAnalytics} />;
    }
  }

  getIntegrationChart() {
    if (this.state.parterAnalytics.loading) {
      return (
        <div className="loader">
          <Spinner size="large" />
        </div>
      );
    } else {
      return (
        <IntegrationsDonutChart parterAnalytics={this.state.parterAnalytics} />
      );
    }
  }
  renderFinalizeDOM = () => {
    const closeModal = () => {
      this.setState({
        isFinalizeModalOpened: false
      });
    };

    const actions = [
      { text: 'YES', onClick: this.finalizeGame, isDisabled: false },
      { text: 'NO', onClick: closeModal }
    ];

    return (
      <ModalTransition>
        {this.state.isFinalizeModalOpened && (
          <Modal
            actions={actions}
            onClose={true}
            heading={this.state.modalFinalizeHeading}
            width={'medium'}
          >
            {this.state.modalFinalizeText}
          </Modal>
        )}
      </ModalTransition>
    );
  };

  finalizeGame = () => {
    this.setState({
      isFinalizeModalOpened: false
    });
  };

  confirmFinalizeGame = (selectedGame) => {
    this.selectedGame = selectedGame;
    let modalFinalizeHeading = 'FINALIZE GAME';
    let modalFinalizeText = 'Do you really want to finalize this game?';
    if (selectedGame.finalized) {
      modalFinalizeHeading = 'UNFINALIZE GAME';
      modalFinalizeText = 'Do you really want to unfinalize this game?';
    }
    this.setState({
      isFinalizeModalOpened: true,
      modalFinalizeHeading: modalFinalizeHeading,
      modalFinalizeText: modalFinalizeText
    });
  };

  gameActionColrenderer = (params) => {
    if (params.data.game_data) {
      if (params.data.finalized) {
        return (
          <Button
            className="finalize-btn"
            onClick={this.confirmFinalizeGame.bind(null, params)}
          >
            UNFINALIZE
          </Button>
        );
      } else {
        return (
          <Button
            className="finalize-btn"
            onClick={this.confirmFinalizeGame.bind(null, params)}
          >
            FINALIZE
          </Button>
        );
      }
    } else {
      return '';
    }
  };

  onGamesGridReady = (params) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
    window.addEventListener('resize', function() {
      setTimeout(function() {
        params.api.sizeColumnsToFit();
      });
    });

    params.api.sizeColumnsToFit();
  };
  getGamesToday() {
    if (this.state.gamesTodayLoading) {
      return (
        <div className="loader">
          <Spinner size="large" />
        </div>
      );
    } else {
      var noRowsTemplate = '<span>No games to show.</span>';
      return (
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <div style={{ overflow: 'hidden', flexGrow: '1' }}>
            <div
              className="ag-theme-material ag-custom-styles device-grid dashboard-games-grid"
              style={{
                height: '500px',
                width: 'calc(100% - 32px)',
                margin: '0px auto',
                marginTop: '10px'
              }}
            >
              <AgGridReact
                columnDefs={this.state.gamesGrid.columnDefs}
                defaultColDef={this.state.gamesGrid.defaultColDef}
                enableColResize={true}
                rowHeight="48"
                suppressDragLeaveHidesColumns={true}
                suppressRowClickSelection={true}
                rowData={this.state.gamesGrid.rowData}
                onGridReady={this.onGamesGridReady}
                overlayNoRowsTemplate={noRowsTemplate}
              />
            </div>
            {this.state.showMoreGamesLink && (
              <div
                style={{
                  textAlign: 'left',
                  padding: '16px 15px',
                  fontSize: '14px',
                  marginTop: '-15px'
                }}
              >
                <NavLink to={'/games'}>See all games</NavLink>
              </div>
            )}
          </div>
          {this.renderFinalizeDOM()}
        </div>
      );
    }
  }

  render() {
    return (
      <div className="content-container dashboard-page">
        <div className="content-header-outer">
          <Header />
        </div>
        <div className="content-main-container">
          <div className="cards-section">
            <div className="card-container">
              <div className="icon-sec">
                <img src={deviceGreenLogo} className="icon" alt="logo" />
              </div>
              <div className="content online">
                <span className="title">Online Devices</span>
                <span className="count">
                  {' '}
                  {this.state.deviceAnalytics.data.online_count === 0 ? (
                    '--'
                  ) : (
                    <CountUp
                      start={0}
                      end={this.state.deviceAnalytics.data.online_count}
                      duration={1}
                    />
                  )}
                </span>
              </div>
            </div>

            <div className="card-container">
              <div className="icon-sec">
                <img src={deviceBlueLogo} className="icon" alt="logo" />
              </div>
              <div className="content">
                <span className="title">Streaming Devices</span>
                <span className="count">
                  {this.state.deviceAnalytics.data.offline_count === 0 ? (
                    '--'
                  ) : (
                    <CountUp
                      start={0}
                      end={this.state.deviceAnalytics.data.streaming_count}
                      duration={1}
                    />
                  )}
                </span>
              </div>
            </div>
            <div className="card-container">
              <div className="icon-sec">
                <img src={gamesGreenLogo} className="icon" alt="logo" />
              </div>
              <div className="content">
                <span className="title">Scheduled Games</span>
                <span className="count">
                  {this.state.gamesByStatus['gray-icon'] &&
                  this.state.gamesByStatus['gray-icon'].length ? (
                    <CountUp
                      start={0}
                      end={this.state.gamesByStatus['gray-icon'].length}
                      duration={1}
                    />
                  ) : (
                    '--'
                  )}
                </span>
              </div>
            </div>

            <div className="card-container">
              <div className="icon-sec">
                <img src={gamesAquaLogo} className="icon" alt="logo" />
              </div>
              <div className="content">
                <span className="title">Live Games</span>
                <span className="count">
                  {this.state.gamesByStatus['green-icon'] &&
                  this.state.gamesByStatus['green-icon'].length ? (
                    <CountUp
                      start={0}
                      end={this.state.gamesByStatus['green-icon'].length}
                      duration={1}
                    />
                  ) : (
                    '--'
                  )}
                </span>
              </div>
            </div>
          </div>

          <div className="dashboard-widget charts-container">
            <div className="card-container2">{this.getDevicesChart()}</div>
            <div className="card-container2 m-r-0">
              {this.getIntegrationChart()}
            </div>
          </div>

          <div className="dashboard-widget games-container">
            <div className="games-header-container device-header">
              <p>Games Today</p>
            </div>
            <div className="games-list-container">{this.getGamesToday()}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;
