import React, { Component } from 'react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import { AgGridReact } from 'ag-grid-react';
import Button from '@atlaskit/button';
import { Redirect } from 'react-router-dom';
import Spinner from '@atlaskit/spinner';
import ScorebirdService from '../../../services/scorebird.service';

const Header = (props) => (
  <div className="content-header">
    <div className="content-header-inner header-row">
      <div className="left-panel">
        <div className="back-to-text">
          <p>All Facilities</p>
        </div>
      </div>
      <div className="right-panel">
        <Button
          appearance="default"
          className="custom-btn"
          onClick={props.addFacility}
        >
          Add Facility{' '}
        </Button>
      </div>
    </div>
  </div>
);

class FacilityList extends Component {
  constructor(props) {
    super(props);
    this.sbs = new ScorebirdService();
    this.state = {
      selectedFacility: '',
      redirect: false,
      columnDefs: [
        {
          headerName: 'FACILITY NAME',
          cellRenderer: function(params) {
            return params.data.school_data.length ? params.data.name : ' - - ';
          }
        },
        {
          headerName: 'SCHOOL NAME',
          cellRenderer: function(params) {
            return params.data.school_data.length
              ? params.data.school_data[0].name
              : ' - - ';
          }
        },
        {
          headerName: 'TYPE',
          cellRenderer: function(params) {
            return params.data.facility_type_data.length
              ? params.data.facility_type_data[0].name
              : ' - - ';
          }
        },
        {
          headerName: 'SCOREBOARD TYPE',
          cellRenderer: function(params) {
            return params.data.scoreboard_type_data.length
              ? params.data.scoreboard_type_data[0].name
              : ' - - ';
          }
        },
        { headerName: 'NO. OF SPORTS', field: 'noofsports' },
        { headerName: 'STATUS', field: 'status' }
      ],
      DefaultcolDef: {
        width: 100,
        editable: true,
        lockPosition: true,
        filter: 'agTextColumnFilter'
      },
      rowData: null,
      loading: true
    };
  }

  componentDidMount() {
    this.sbs.getFacilities().then((res) => {
      this.setState({ loading: false, rowData: res.data.data });
    });
  }

  getGridData() {
    return (
      <div className="ag-theme-material ag-custom-styles device-grid">
        <div />
        {this.state.loading ? (
          <div className="loader">
            <Spinner size="large" />
          </div>
        ) : (
          <AgGridReact
            enableSorting={true}
            rowHeight="48"
            headerHeight="36"
            columnDefs={this.state.columnDefs}
            rowData={this.state.rowData}
            onGridReady={this.onGridReady.bind(this)}
            onRowClicked={this.onRowClicked}
          />
        )}
      </div>
    );
  }

  setRowRedirect = (facilityId) => {
    this.setState({
      selectedFacility: facilityId,
      redirect: true
    });
  };

  onRowClicked = (event) => {
    let facilityId = event.data.unique_id;
    this.setRowRedirect(facilityId);
  };

  renderRowRedirect = () => {
    if (this.state.redirect) {
      let facilityId = this.state.selectedFacility;
      return (
        <Redirect
          to={{
            pathname: '/facilityItem/' + facilityId,
            state: { data: this.state.rowData }
          }}
        />
      );
    }
  };

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
    window.addEventListener('resize', function() {
      setTimeout(function() {
        params.api.sizeColumnsToFit();
      });
    });
    params.api.sizeColumnsToFit();
  }

  onClickBack = () => {
    this.props.history.push('/app/schools');
  };

  addFacility = () => {
    this.props.history.push({ pathname: '/addFacility' });
  };

  render() {
    return (
      <div className="full-width-container">
        <div className="right-container">
          <div>
            <Header addFacility={this.addFacility} />
            {this.getGridData()}
            {this.renderRowRedirect()}
          </div>
        </div>
      </div>
    );
  }
}

export default FacilityList;
