let backendHost;

const hostname = window && window.location && window.location.hostname;

if (hostname === '202.63.102.86') {
  backendHost = 'http://202.63.102.86:4000';
} else if (hostname === '192.168.1.1') {
  backendHost = 'http://192.168.1.1:4000';
} else {
  backendHost = 'http://' + hostname + ':8000';
}

export const API_URL = `${backendHost}`;
