import React, { Component } from 'react';
import { Label } from '@atlaskit/field-base';

class DpController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.initialValue || '',
      isOpen: props.initialIsOpen || false
    };
  }

  onValueChange = (value) => {
    this.recentlySelected = true;
    this.setState(
      {
        value,
        isOpen: false
      },
      () => {
        setTimeout(() => {
          this.recentlySelected = false;
        }, 200);
      }
    );
  };

  onFocus = () => {
    this.setState({
      isOpen: false
    });
  };

  render() {
    return (
      // eslint-disable-next-line jsx-a11y/no-static-element-interactions
      <div onClick={this.handleClick}>
        {this.props.children({
          value: this.state.value,
          onValueChange: this.onValueChange,
          isOpen: this.state.isOpen,
          onBlur: this.onBlur
        })}
      </div>
    );
  }
}

export default DpController;

// export default () => {
//     return (
//         <div>
//             <h3>Date picker</h3>
//             <Label
//                 htmlFor="react-select-datepicker-2--input"
//                 label="controlled (value)"
//             />
//             <Controlled initialValue="2018-01-02">
//                 {({ value, onValueChange, onBlur }) => (
//                     <DatePicker
//                         id="datepicker-2"
//                         value={value}
//                         onChange={onValueChange}
//                         onBlur={onBlur}
//                     />
//                 )}
//             </Controlled>
//         </div>
//     );
// };
