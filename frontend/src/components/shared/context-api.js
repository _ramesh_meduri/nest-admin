class Application extends React.Component {
  constructor() {
    super();

    this.state = {
      nbTasks: 0
    };
  }

  addTask = () => {
    this.setState((prevState) => ({
      nbTasks: prevState.nbTasks + 1
    }));
  };

  removeTask = () => {
    this.setState((prevState) => ({
      nbTasks: prevState.nbTasks - 1
    }));
  };

  isLoading = () => {
    return this.state.nbTasks > 0;
  };

  getChildContext = () => {
    return {
      addTask: this.addTask,
      removeTask: this.removeTask,
      isLoading: this.isLoading
    };
  };

  render() {
    return (
      <div>
        <ComponentX />
        <ComponentY />
        <LoadingMask />
      </div>
    );
  }
}

Application.childContextTypes = {
  addTask: PropTypes.func,
  removeTask: PropTypes.func,
  isLoading: PropTypes.func
};

const LoadingMask = (props, context) =>
  context.isLoading() ? <div>LOADING ...</div> : null;

LoadingMask.contextTypes = {
  isLoading: PropTypes.func
};

class ComponentX extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      message: 'Processing ...'
    };
  }

  componentDidMount() {
    this.context.addTask();

    setTimeout(() => {
      this.setState({
        message: 'ComponentX ready !'
      });

      this.context.removeTask();
    }, 3500);
  }

  render() {
    return (
      <div>
        <button disabled>{this.state.message}</button>
      </div>
    );
  }
}

ComponentX.contextTypes = {
  addTask: PropTypes.func,
  removeTask: PropTypes.func
};

class ComponentY extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      message: 'Processing ...'
    };
  }

  componentDidMount() {
    this.context.addTask();

    setTimeout(() => {
      this.setState({
        message: 'ComponentY ready !'
      });

      this.context.removeTask();
    }, 6000);
  }

  render() {
    return (
      <div>
        <button disabled>{this.state.message}</button>
      </div>
    );
  }
}

ComponentY.contextTypes = {
  addTask: PropTypes.func,
  removeTask: PropTypes.func
};

ReactDOM.render(<Application />, document.getElementById('app'));
