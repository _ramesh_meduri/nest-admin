import React, { Component } from 'react';

class NotFound extends Component {
  render() {
    return (
      <div>
        <h2>404</h2>
        404 not allowed
      </div>
    );
  }
}

export default NotFound;
