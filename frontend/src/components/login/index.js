import React, { Component } from 'react';
import '../../styles/login.scss';
import logo from '../../assets/images/logo.png';

import Button from '@atlaskit/button';
import FieldText from '@atlaskit/field-text';
import { Field } from '@atlaskit/form';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      emailError: false,
      passwordError: false,
      invalidError: false,
      email: '',
      password: '',
      isBtnDisabled: false
    };
  }

  formRef;

  admin_details = {
    email: 'admin@scorebird.com',
    password: 'scorebird@4321!'
  };

  updateEmailValue = (evt) => {
    this.setState({ email: evt.target.value });
  };

  updatePasswordValue = (evt) => {
    this.setState({ password: evt.target.value });
  };

  emailErrorHandler = () => {
    if (this.state.invalidError) {
      if (!this.state.email || !this.state.password) {
        return (
          <span className="login-error">Please enter email and password.</span>
        );
      } else {
        return (
          <span className="login-error">Email and password are incorrect.</span>
        );
      }
    }
  };

  clickHandler = () => {
    if (
      this.state.email === this.admin_details.email &&
      this.state.password === this.admin_details.password
    ) {
      this.setState({
        isBtnDisabled: true
      });
      setTimeout(() => {
        this.props.afterLogin();
        this.setState({
          isBtnDisabled: false
        });
      }, 200);
    } else {
      this.setState({
        invalidError: true
      });
    }
  };

  checkLoginForm = (e) => {
    if (e.keyCode === 13) {
      this.clickHandler();
    } else {
      this.setState({
        invalidError: false
      });
    }
  };

  render() {
    return (
      <div className="login-container">
        <div className="login-box-sec">
          <div className="login-logo-container">
            <img src={logo} className="scorebird-logo" alt="logo" />
          </div>
          <div className="form-container">
            <form
              className="device-form"
              style={{ backgroundColor: 'white' }}
              ref={(form) => {
                this.formRef = form;
              }}
            >
              <div className="">
                <div className="form-field">
                  <Field label={null} helperText={null} required>
                    <FieldText
                      name="myText"
                      placeholder="Email"
                      id="username"
                      onChange={this.updateEmailValue}
                      value={this.state.email}
                      onKeyUp={this.checkLoginForm}
                    />
                  </Field>                  
                </div>
                <div className="form-field">
                  <Field label={null} helperText={null} required>
                    <FieldText
                      name="myText"
                      placeholder="Password"
                      type="password"
                      id="password"
                      onChange={this.updatePasswordValue}
                      value={this.state.password}
                      onKeyUp={this.checkLoginForm}
                    />
                  </Field>                  
                </div>
              </div>
              <p>
                <Button
                  appearance="primary"
                  className="login-btn"
                  onClick={this.clickHandler}
                  isDisabled={this.state.isBtnDisabled}
                >
                  Login
                </Button>
                {this.emailErrorHandler()}
              </p>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
