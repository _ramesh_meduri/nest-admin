### NeST Admin(Super Admin Portal) -- MERN Application
---
##### Description


##### Backend Tech Stack
> Node  
> Express  
> MongoDB  
> Mongoose


##### Frontend Tech Stack
> React  
> React Router  
> Redux  
> Redux Thunk  
> Atlaskit  


##### Commands for Local Development

Start the `MongoDB` server `mongodb://localhost:27017/score`
```sh
cd backend
npm install
npm start
```

Server Starts Listening on http://localhost:2222
```sh
cd frontend
npm install
npm start
```
Client Starts Listening on http://localhost:1111



