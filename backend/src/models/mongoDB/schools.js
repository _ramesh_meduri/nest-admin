var MongoDBConfig = require('../../config/mongodb.json');
var url = MongoDBConfig.url;
var database = 'staging';
var MongoClient = require('mongodb').MongoClient;
const moment = require('moment');

exports.getSchool = function(schoolId) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('schools')
        .find({ unique_id: schoolId })
        .toArray()
        .then((response) => {
          var school = [];
          if (err) {
            reject(err);
          } else {
            school = school.concat(response);
          }
          resolve({
            data: school,
            success: 1
          });
        });
    });
  });
};
exports.getAll = function() {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('schools')
        .find()
        .toArray()
        .then((docs) => {                    
          resolve({
            data: docs,
            total: docs.length
          });
        });
    });
  });
 };

exports.getSchoolsList = function(userId) {
  if (userId != 'getSchools') {
    return new Promise(function(resolve, reject) {
      MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
        if (err) throw err;
        var dbo = db.db(database);
        dbo
          .collection('users')
          .aggregate([
            { $match: { email: userId } },
            {
              $lookup: {
                from: 'organisations',
                localField: 'unique_id',
                foreignField: 'created_by',
                as: 'orgdata'
              }
            },
            { $unwind: '$orgdata' },
            {
              $lookup: {
                from: 'schools',
                localField: 'orgdata.unique_id',
                foreignField: 'org_id',
                as: 'schools'
              }
            }
          ])
          .toArray()
          .then((data) => {
            let response = [];
            if (err) {
              reject(err);
            } else {
              response = response.concat(data);
              resolve(response[0].schools);
            }
          });
      });
    });
  } else {
    return new Promise(function(resolve, reject) {
      MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
        if (err) throw err;
        var dbo = db.db(database);
        dbo
          .collection('schools')
          .find()
          .toArray()
          .then((response) => {
            var schoollist = [];
            if (err) {
              reject(err);
            } else {
              schoollist = schoollist.concat(response);
            }
            resolve({
              data: schoollist,
              total: schoollist.length
            });
          });
      });
    });
  }
};
exports.getSchoolListforSignup = function() {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('schools')
        .find()
        .toArray()
        .then((response) => {
          var schoollist = [];
          if (err) {
            reject(err);
          } else {
            schoollist = schoollist.concat(response);
          }
          resolve({
            data: schoollist,
            total: schoollist.length
          });
        });
    });
  });
};

exports.getSchoolData = function(schoolId) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('facilities')
        .aggregate([
          { $match: { school_id: schoolId } },
          // uniueid
          // 'school_id'
          {
            $lookup: {
              from: 'schools',
              localField: 'school_id',
              foreignField: 'unique_id',
              as: 'school_data'
            }
          },

          {
            $lookup: {
              from: 'facility_types',
              localField: 'facility_type_id',
              foreignField: 'unique_id',
              as: 'facility_type_data'
            }
          },
          {
            $lookup: {
              from: 'scoreboards',
              localField: 'scoreboard_id',
              foreignField: 'unique_id',
              as: 'scoreboard_type_data'
            }
          },
          {
            $lookup: {
              from: 'venues',
              localField: 'venue_id',
              foreignField: 'unique_id',
              as: 'venue_type_data'
            }
          }
        ])
        .toArray()
        .then((data) => {
          let response = [];
          if (err) {
            reject(err);
          } else {
            response = response.concat(data);
            resolve(response);
          }
        });
    });
  });
};

exports.getSchoolfacilityData = function(schoolId) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('facilities')
        .aggregate([
          { $match: { unique_id: schoolId } },
          // uniueid
          // 'school_id'
          {
            $lookup: {
              from: 'schools',
              localField: 'school_id',
              foreignField: 'unique_id',
              as: 'school_data'
            }
          },

          {
            $lookup: {
              from: 'facility_types',
              localField: 'facility_type_id',
              foreignField: 'unique_id',
              as: 'facility_type_data'
            }
          },
          {
            $lookup: {
              from: 'scoreboards',
              localField: 'scoreboard_id',
              foreignField: 'unique_id',
              as: 'scoreboard_type_data'
            }
          },
          {
            $lookup: {
              from: 'venues',
              localField: 'venue_id',
              foreignField: 'unique_id',
              as: 'venue_type_data'
            }
          }
        ])
        .toArray()
        .then((data) => {
          let response = [];
          if (err) {
            reject(err);
          } else {
            response = response.concat(data);
            resolve(response);
          }
        });
    });
  });
};

var getUniqueCode = function(length, type) {
  var text = '';
  var possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  var CurrentDate = moment().unix();
  text += '-' + type + '-';
  text += CurrentDate;
  return text;
};

exports.addSchool = function(parameters) {
  var created_on = moment().format('YYYY-MM-DD HH:MM:SS');
  return new Promise(function(resolve, reject) {
    if (!parameters.length) {
      resolve({
        success: false,
        message: 'Parameters passed are incorrect'
      });
    } else if (
      parameters.length &&
      (typeof parameters[0].name !== 'string' ||
        typeof parameters[0].same_as_Physical_address !== 'boolean' ||
        typeof parameters[0].mailing_address !== 'string' ||
        typeof parameters[0].mailing_state !== 'string' ||
        typeof parameters[0].mailing_city !== 'string' ||
        typeof parameters[0].mailing_postal_Code !== 'string' ||
        typeof parameters[0].address !== 'string' ||
        typeof parameters[0].state !== 'string' ||
        typeof parameters[0].city !== 'string' ||
        typeof parameters[0].org_id !== 'string')
    ) {
      resolve({
        success: false,
        message: 'Required all fields'
      });
    } else {
      parameters[0].created_on = created_on;
      parameters[0].unique_id = getUniqueCode(8, 'sch');
      MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
        if (err) throw err;
        var dbo = db.db(database);
        dbo.collection('schools').insertMany(parameters, function(err, res) {
          if (err) throw err;
          resolve({
            success: true,
            data: res
          });
        });
      });
    }
  });
};

exports.addSchoolfromSignUp = function(parameters) {
  return new Promise(function(resolve, reject) {
    if (!parameters.length) {
      resolve({
        success: false,
        message: 'Parameters passed are incorrect'
      });
    } else {
      parameters[0].unique_id = getUniqueCode(8, 'sch');
      MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
        if (err) throw err;
        var dbo = db.db(database);
        dbo.collection('schools').insertMany(parameters, function(err, res) {
          if (err) throw err;
          resolve({
            success: true,
            data: res
          });
        });
      });
    }
  });
};

exports.updateFacilityInfo = function(Facility) {
  return new Promise(function(resolve, reject) {
    // console.log("facilityid update ==>  ", Facility)
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      var query = { unique_id: Facility[0].facilityId };
      var modified_on = moment().format('YYYY-MM-DD HH:MM:SS');
      if (!Facility.length) {
        resolve({
          success: false,
          message: 'Parameters passed are incorrect'
        });
      } else if (
        Facility.length &&
        (typeof Facility[0].name !== 'string' ||
          typeof Facility[0].facility_type_id !== 'string' ||
          typeof Facility[0].scoreboard_id !== 'string' ||
          typeof Facility[0].ethernet_access !== 'string' ||
          typeof Facility[0].wifi_access !== 'string' ||
          typeof Facility[0].is_facility !== 'string' ||
          typeof Facility[0].sport !== 'object')
      ) {
        resolve({
          success: false,
          message: 'Required all fields'
        });
      } else {
        var data = {
          $set: {
            name: Facility[0].name || '',
            state: Facility[0].state || '',
            city: Facility[0].city || '',
            wifi_access: Facility[0].wifi_access || '',
            is_facility: Facility[0].is_facility || '',
            ethernet_access: Facility[0].ethernet_access || '',
            facility_type_id: Facility[0].facility_type_id || '',
            scoreboard_id: Facility[0].scoreboard_id || '',
            sport: Facility[0].sport || '',
            facility_comments: Facility[0].facilitycomments || '',
            facility_address: Facility[0].facilityaddress || '',
            modified_on: modified_on || ''
          }
        };
        dbo
          .collection('facilities')
          .updateMany(query, data, function(err, res) {
            if (err) throw err;
            db.close();
            resolve({
              data: data
            });
          });
      }
    });
  });
};

exports.addFacility = function(schoolfacility) {
  return new Promise(function(resolve, reject) {
    // console.log("print school facility data ==> ", schoolfacility);
    if (!schoolfacility.length) {
      resolve({
        success: false,
        message: 'Parameters passed are incorrect'
      });
    } else if (
      schoolfacility.length &&
      (typeof schoolfacility[0].name !== 'string' ||
        typeof schoolfacility[0].school_id !== 'string' ||
        typeof schoolfacility[0].facility_type_id !== 'string' ||
        typeof schoolfacility[0].scoreboard_id !== 'string' ||
        typeof schoolfacility[0].ethernet_access !== 'string' ||
        typeof schoolfacility[0].wifi_access !== 'string' ||
        typeof schoolfacility[0].is_facility !== 'string' ||
        typeof schoolfacility[0].sport !== 'object')
    ) {
      resolve({
        success: false,
        message: 'Required all fields'
      });
    } else {
      var created_on = moment().format('YYYY-MM-DD HH:MM:SS');
      var facilitiesparams = [
        {
          name: schoolfacility[0].name || '',
          school_id: schoolfacility[0].school_id || '',
          facility_type_id: schoolfacility[0].facility_type_id || '',
          scoreboard_id: schoolfacility[0].scoreboard_id || '',
          ethernet_access: schoolfacility[0].ethernet_access || '',
          wifi_access: schoolfacility[0].wifi_access || '',
          is_facility: schoolfacility[0].is_facility || '',
          sport: schoolfacility[0].sport || '',
          state: schoolfacility[0].state || '',
          city: schoolfacility[0].city || '',
          facility_comments: schoolfacility[0].facilitycomments || '',
          facility_address: schoolfacility[0].facilityaddress || '',
          created_on: created_on || '',
          status: 'REQUIRES CONFORMATION '
        }
      ];
      // console.log(facilitiesparams)
      MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
        if (err) throw err;
        var dbo = db.db(database);
        facilitiesparams[0].unique_id = getUniqueCode(8, 'fac');
        dbo
          .collection('facilities')
          .insertMany(facilitiesparams, function(err, res) {
            if (err) throw err;
            resolve({
              data: res
            });
          });
      });
    }
  });
};

exports.updateSchoolAddress = function(school) {
  // console.log("update school address ==>", school)

  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, function(err, db) {
      var modified_on = moment().format('YYYY-MM-DD HH:MM:SS');
      if (!school.length) {
        resolve({
          success: false,
          message: 'Parameters passed are incorrect'
        });
      } else if (
        school.length &&
        (typeof school[0].name !== 'string' ||
          typeof school[0].address !== 'string' ||
          typeof school[0].city !== 'string' ||
          typeof school[0].mailing_address !== 'string' ||
          typeof school[0].mailing_city !== 'string' ||
          typeof school[0].mailing_postal_Code !== 'string' ||
          typeof school[0].mailing_state !== 'string' ||
          typeof school[0].postal_Code !== 'string' ||
          typeof school[0].same_as_Physical_address !== 'boolean' ||
          typeof school[0].mailing_address !== 'string' ||
          typeof school[0].mailing_state !== 'string' ||
          typeof school[0].mailing_city !== 'string' ||
          typeof school[0].mailing_postal_Code !== 'string' ||
          typeof school[0].org_id !== 'string' ||
          typeof school[0].unique_id !== 'string')
      ) {
        resolve({
          success: false,
          message: 'Required all fields'
        });
      } else {
        if (err) throw err;
        var dbo = db.db(database);
        var query = { unique_id: school[0].unique_id };
        //data stores the updated value
        var data = {
          $set: {
            address: school[0].address || '',
            city: school[0].city || '',
            logo: school[0].logo || '',
            mailing_address: school[0].mailing_address || '',
            mailing_city: school[0].mailing_city || '',
            mailing_postal_Code: school[0].mailing_postal_Code || '',
            mailing_state: school[0].mailing_state || '',
            name: school[0].name || '',
            postal_Code: school[0].postal_Code || '',
            same_as_Physical_address:
              school[0].same_as_Physical_address || false,
            state: school[0].state || '',
            modified_on: modified_on || ''
          }
        };
        dbo.collection('schools').updateMany(query, data, function(err, res) {
          if (err) throw err;
          db.close();
          resolve({
            data: data
          });
        });
      }
    });
  });
};

exports.updateSchoolOrgId = function(school) {
  // console.log(school);
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      var query = { unique_id: school[0].school_id };
      //data stores the updated value
      var data = {
        $set: {
          org_id: school[0].org_id || ''
        }
      };
      dbo.collection('schools').updateMany(query, data, function(err, res) {
        if (err) throw err;
        db.close();
        resolve({
          data: data
        });
      });
    });
  });
};

exports.getAllSports = function() {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('sports')
        .find()
        .toArray()
        .then((response) => {
          sports = [];
          if (err) {
            reject(err);
          } else {
            sports = sports.concat(response);
          }

          resolve({
            data: sports,
            total: sports.length
          });
        });
    });
  });
};
exports.getTeamGrades = function() {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('team_grades')
        .find()
        .toArray()
        .then((response) => {
          team_grades = [];
          if (err) {
            reject(err);
          } else {
            team_grades = team_grades.concat(response);
          }

          resolve({
            data: team_grades,
            total: team_grades.length
          });
        });
    });
  });
};
