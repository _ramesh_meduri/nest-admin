var MongoDBConfig = require('../../config/mongodb.json');
var url = MongoDBConfig.url;
var MongoClient = require('mongodb').MongoClient;
const moment = require('moment');

exports.getDashBoardCount = function() {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db('staging');
      dbo
        .collection('devices')
        .find()
        .toArray()
        .then((response) => {
          var devices = response;
          var online_devices = devices.filter((i) => {
            return i.online && i.online === true;
          });

          var offline_devices = devices.filter((i) => {
            return !i.online;
          });

          var streaming_devices = devices.filter((i) => {
            return (
              i.last_update_ts &&
              moment()
                .utc()
                .diff(moment(i.last_update_ts), 'minutes') < 15
            );
          });

          resolve({
            online_count: online_devices.length ? online_devices.length : 0,
            offline_count: offline_devices.length ? offline_devices.length : 0,
            streaming_count: streaming_devices.length
              ? streaming_devices.length
              : 0
          });
        });
    });
  });
};
