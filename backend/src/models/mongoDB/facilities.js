var MongoDBConfig = require('../../config/mongodb.json');
var url = MongoDBConfig.url;
var database = 'staging';
var MongoClient = require('mongodb').MongoClient;

exports.getFacilitiesList = function() {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('facilities')
        .aggregate([
          {
            $lookup: {
              from: 'schools',
              localField: 'school_id',
              foreignField: 'unique_id',
              as: 'school_data'
            }
          },

          {
            $lookup: {
              from: 'facility_types',
              localField: 'facility_type_id',
              foreignField: 'unique_id',
              as: 'facility_type_data'
            }
          },
          {
            $lookup: {
              from: 'scoreboards',
              localField: 'scoreboard_id',
              foreignField: 'unique_id',
              as: 'scoreboard_type_data'
            }
          }
        ])
        .toArray()
        .then((data) => {
          let facilitieslist = [];
          if (err) {
            reject(err);
          } else {
            facilitieslist = facilitieslist.concat(data);
            resolve({
              data: facilitieslist,
              total: facilitieslist.length
            });
          }
        });
    });
  });
};

var MongoClient = require('mongodb').MongoClient;

exports.getSchoolfacilityData = function(schoolId) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('facilities')
        .aggregate([
          { $match: { school_id: schoolId } },
          // uniueid
          // 'school_id'
          {
            $lookup: {
              from: 'schools',
              localField: 'school_id',
              foreignField: 'unique_id',
              as: 'school_data'
            }
          },
          {
            $lookup: {
              from: 'facility_types',
              localField: 'facility_type_id',
              foreignField: 'unique_id',
              as: 'facility_type_data'
            }
          },
          {
            $lookup: {
              from: 'scoreboards',
              localField: 'scoreboard_id',
              foreignField: 'unique_id',
              as: 'scoreboard_type_data'
            }
          },
          {
            $lookup: {
              from: 'venues',
              localField: 'venue_id',
              foreignField: 'unique_id',
              as: 'venue_type_data'
            }
          }
        ])
        .toArray()
        .then((data) => {
          let response = [];
          if (err) {
            reject(err);
          } else {
            response = response.concat(data);
            resolve(response);
          }
        });
    });
  });
};

exports.addFacilityFromFacilitypage = function(schoolfacility) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      // var query =[{},{}]
      schoolfacility[0].unique_id = getUniqueCode(8, 'fac');
      dbo
        .collection('facilities')
        .insertMany(schoolfacility, function(err, res) {
          if (err) throw err;
          resolve({
            data: res
          });
        });
    });
  });
};

exports.updateFacilityInfoFromFacilitypage = function(school) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      var query = { unique_id: school.unique_id };
      //data stores the updated value
      var data = {
        $set: {
          name: school.name || '',
          facility_type_data: [{ name: school.facilitytype.value || '' }]
        }
      };
      dbo.collection('facilities').updateMany(query, data, function(err, res) {
        if (err) throw err;
        db.close();
        resolve({
          data: data
        });
      });
    });
  });
};

exports.getAllFacilityTypes = function() {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('facility_types')
        .find()
        .toArray()
        .then((response) => {
          facilitytypes = [];
          if (err) {
            reject(err);
          } else {
            facilitytypes = facilitytypes.concat(response);
          }

          resolve({
            data: facilitytypes,
            total: facilitytypes.length
          });
        });
    });
  });
};

exports.getAllScoreBoardTypes = function() {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('scoreboards')
        .find()
        .toArray()
        .then((response) => {
          scoreBoardtypes = [];
          if (err) {
            reject(err);
          } else {
            scoreBoardtypes = scoreBoardtypes.concat(response);
          }

          resolve({
            data: scoreBoardtypes,
            total: scoreBoardtypes.length
          });
        });
    });
  });
};
