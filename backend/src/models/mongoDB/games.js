const moment = require('moment');

var MongoDBConfig = require('../../config/mongodb.json');
var url = MongoDBConfig.url;
var MongoClient = require('mongodb').MongoClient;

var getGameStatusIcon = function(start_time, game) {
  let game_icon = 'no-icon';
  let iflive =
    moment(start_time).format('YYYY-MM-DD') == moment().format('YYYY-MM-DD');
  if (game.finalized) {
    game_icon = 'blue-icon';
  } else if (moment(start_time).diff(moment()) > 0) {
    game_icon = 'gray-icon';
  } else if (iflive && game.device_serial) {
    if (
      game.state &&
      game.state.ts &&
      moment().diff(moment(game.state.ts), 'minutes') < 2
    ) {
      game_icon = 'green-icon';
    } else {
      game_icon = 'red-icon';
    }
  }
  return game_icon;
};

exports.getGamesList = function(params) {
  // current_date = new Date(2018, 10, 2);
  var current_date =
    new Date().getFullYear() +
    '/' +
    (new Date().getMonth() + 1) +
    '/' +
    new Date().getDate();
  var selected_date = params.selected_date || current_date;
  selected_date = moment(selected_date).format('YYYY-MM-DD');
  return new Promise(function(resolve, reject) {
    var games = [];
    var schedules = [];
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db('staging');
      dbo
        .collection('games')
        .find({ scheduled_date: selected_date })
        .toArray()
        .then((response) => {
          games = games.concat(response);
          if (!games.length) {
            resolve(games);
            return;
          }
          var gamesBySchedule = {};

          var selected_schedules = [];
          games.map((game, index) => {
            if (!gamesBySchedule[game.schedule_id]) {
              gamesBySchedule[game.schedule_id] = game;
              selected_schedules.push(game.schedule_id);
            }
          });

          dbo
            .collection('schedules')
            .find({ schedule_id: { $in: selected_schedules } })
            .toArray()
            .then((response) => {
              schedules = schedules.concat(response);

              schedules.map((s) => {
                s.game_start_time = moment(s.game_start_ts).format(
                  'dddd [at] hh:mm A'
                );
                s.game_icon = getGameStatusIcon(
                  s.game_start_ts,
                  gamesBySchedule[s.schedule_id]
                );
                gamesBySchedule[s.schedule_id]['team_data'] = s;
              });

              var gamesData = Object.keys(gamesBySchedule).map(function(key) {
                return gamesBySchedule[key];
              });
              resolve(gamesData);
              db.close();
            });
        });
    });
  });
};

exports.getGameData = function(gameId) {
  return new Promise(function(resolve, reject) {
    var gameInfo = {};
    var response = {
      gameInfo: null
    };
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db('staging');

      //Game info
      dbo
        .collection('games')
        .find({ game_id: gameId })
        .toArray()
        .then((data) => {
          response.gameInfo = data[0];
          if (!response.gameInfo) {
            resolve(gameInfo);
            return;
          }

          var schedulePromise = false;
          var devicePromise = false;

          function checkIfPromisesFinished() {
            if (schedulePromise && devicePromise) {
              resolve(response);
            }
          }

          //  Schedule info
          dbo
            .collection('schedules')
            .find({ schedule_id: response.gameInfo.schedule_id })
            .toArray()
            .then((data1) => {
              let s = data1.length ? data1[0] : {};
              s.game_icon = getGameStatusIcon(
                s.game_start_ts,
                response.gameInfo
              );
              response.gameInfo.team_data = s;
              schedulePromise = true;
              checkIfPromisesFinished();
            });

          //  Device info
          dbo
            .collection('devices')
            .find({ projection: 'device_serial' })
            .toArray()
            .then((data2) => {
              response.gameInfo.device_data = data2.length ? data2[0] : {};
              devicePromise = true;
              checkIfPromisesFinished();
            });
        });
    });
  });
};

exports.finalizeGame = function(gameId) {};

exports.unfinalizeGame = function(gameId) {};
