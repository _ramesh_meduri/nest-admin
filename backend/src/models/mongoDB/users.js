var MongoDBConfig = require('../../config/mongodb.json');
var url = MongoDBConfig.url;
var database = 'staging';
var MongoClient = require('mongodb').MongoClient;
const moment = require('moment');

exports.getUsersList = function() {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('users')
        .find()
        .toArray()
        .then((response) => {
          userslist = [];
          if (err) {
            reject(err);
          } else {
            userslist = userslist.concat(response);
          }
          resolve({
            data: userslist,
            total: userslist.length
          });
        });
    });
  });
};
exports.getUsersListBySchoolId = function(schoolId) {
  if (schoolId) {
    return new Promise(function(resolve, reject) {
      MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
        if (err) throw err;
        var dbo = db.db(database);
        dbo
          .collection('users')
          .find({ school_id: schoolId })
          .toArray()
          .then((response) => {
            userslist = [];
            if (err) {
              reject(err);
            } else {
              userslist = userslist.concat(response);
            }

            resolve({
              data: userslist,
              total: userslist.length
            });
          });
      });
    });
  }
};
exports.getUserData = function(userId) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      var query = { unique_id: userId };
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('users')
        .find(query)
        .toArray()
        .then((data) => {
          let response = [];
          if (err) {
            reject(err);
          } else {
            response = response.concat(data);
            resolve(response);
          }
        });
    });
  });
};

var getUniqueCode = function(length, type) {
  var text = '';
  var possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  var CurrentDate = moment().unix();
  text += '-' + type + '-';
  text += CurrentDate;
  return text;
};

exports.adduserFromUserpage = function(parameters) {
  // console.log("parameters", parameters)
  return new Promise(function(resolve, reject) {
    var created_on = moment().format('YYYY-MM-DD HH:MM:SS');

    if (!parameters.length) {
      resolve({
        success: false,
        message: 'Parameters passed are incorrect'
      });
    } else if (
      parameters.length &&
      (typeof parameters[0].first_name !== 'string' ||
        typeof parameters[0].last_name !== 'string' ||
        typeof parameters[0].email !== 'string' ||
        typeof parameters[0].phone_number !== 'string' ||
        typeof parameters[0].school_id !== 'string')
    ) {
      resolve({
        success: false,
        message: 'Required all fields'
      });
    } else {
      parameters[0].created_on = created_on;
      parameters[0].unique_id = getUniqueCode(8, 'user');
      MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
        if (err) throw err;
        var dbo = db.db(database);
        dbo.collection('users').insertMany(parameters, function(err, res) {
          if (err) throw err;
          resolve({
            success: true,
            data: res
          });
        });
      });
    }
  });
};

exports.updateUserInfo = function(userdata) {
  // console.log("userdata", userdata)
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, function(err, db) {
      var modified_on = moment().format('YYYY-MM-DD HH:MM:SS');
      if (!userdata.length) {
        resolve({
          success: false,
          message: 'Parameters passed are incorrect'
        });
      } else if (
        userdata.length &&
        (typeof userdata[0].first_name !== 'string' ||
          typeof userdata[0].last_name !== 'string' ||
          typeof userdata[0].email !== 'string' ||
          typeof userdata[0].phone_number !== 'string' ||
          typeof userdata[0].unique_id !== 'string')
      ) {
        // console.log("please stop here ")
        resolve({
          success: false,
          message: 'Required all fields to update '
        });
      } else {
        // console.log("userdata", userdata)
        if (err) throw err;
        var dbo = db.db(database);
        var query = { unique_id: userdata[0].unique_id };
        var data = {
          $set: {
            first_name: userdata[0].first_name || '',
            last_name: userdata[0].last_name || '',
            email: userdata[0].email || '',
            phone_number: userdata[0].phone_number || '',
            //  referred_by:userdata[0].referred_by||'',
            //  user_role:userdata[0].user_role||'',
            modified_on: modified_on || ''
          }
        };
        dbo.collection('users').updateMany(query, data, function(err, res) {
          if (err) throw err;
          db.close();
          resolve({
            data: data
          });
        });
      }
    });
  });
};

// exports.getUsersListBySchoolId = function(schoolId) {
//   console.log(schoolId)

//   return new Promise(function(resolve, reject) {
//     MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
//       if (err) throw err;
//       var dbo = db.db(database);
//       dbo
//         .collection('users')
//         .find({schoolId:schoolId})
//         .toArray()
//         .then((data) => {
//           let response = [];
//           if (err) {
//             reject(err);
//           } else {
//             response = response.concat(data);
//             resolve(response);
//           }
//         });
//     });
//   });
// };

// find()

// db.users.find({"unique_id" : "JAhdvJos-user-1554879738"})
//    .projection({})
//    .sort({_id:-1})
//    .limit(100)
