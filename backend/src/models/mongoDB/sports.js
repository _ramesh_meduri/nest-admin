var MongoDBConfig = require('../../config/mongodb.json');
var url = MongoDBConfig.url;
var database = 'staging';
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;

exports.getAll = function() {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('sports')
        .find()
        .toArray()
        .then((response) => {
          userslist = [];
          if (err) {
            reject(err);
          } else {
            userslist = userslist.concat(response);
          }
          resolve({
            data: userslist,
            total: userslist.length
          });
        });
    });
  });
};

exports.createOne = function(obj) {
  obj.createdAt = obj.createdAt || new Date();
  obj.updatedAt = obj.updatedAt || new Date();
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo.collection('sports').save(obj, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  });
};

exports.updateOne = function(id, obj) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo.collection('sports').findOneAndUpdate(
        { _id: ObjectID(id) },
        {
          $set: {
            name: obj.name,
            updatedAt: new Date()
          }
        },
        (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        }
      );
    });
  });
};

exports.deleteOne = function(id) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db(database);
      dbo
        .collection('sports')
        .findOneAndDelete({ _id: ObjectID(id) }, (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
    });
  });
};
