const AWS = require('aws-sdk');
const AWSConfig = require('../../config/aws.json');
AWS.config.update(AWSConfig);

var MongoDBConfig = require('../../config/mongodb.json');
var url = MongoDBConfig.url;
var MongoClient = require('mongodb').MongoClient;

exports.getDevicesList = function() {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db('staging');
      dbo
        .collection('devices')
        .find()
        .toArray()
        .then((data) => {
          var items = [];
          if (err) {
            reject(err);
          } else {
            items = items.concat(data);
            resolve(items);
          }
        });
    });
  });
};

exports.getDeviceData = function(deviceId) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db('staging');
      dbo
        .collection('devices')
        .find({ serial: deviceId })
        .toArray()
        .then((data) => {
          if (err) {
            reject(err);
          } else {
            var response = {
              deviceInfo: null,
              teams: []
            };
            if (data) {
              response.deviceInfo = data[0];
              if (
                response.deviceInfo &&
                response.deviceInfo.home_teams &&
                response.deviceInfo.home_teams.length
              ) {
                //  teams query
                dbo
                  .collection('teams')
                  .find({ team_id: { $in: response.deviceInfo.home_teams } })
                  .toArray()
                  .then((data) => {
                    if (err) {
                      reject(err);
                    } else {
                      response.teams = response.teams.concat(data);
                      resolve(response);
                    }
                  });
              } else {
                resolve(response);
              }
            } else {
              resolve(response);
            }
          }
        });
    });
  });
};

// Aws query

exports.getDeviceStreamData = function(deviceId) {
  var lambda = new AWS.Lambda();

  return new Promise(function(resolve, reject) {
    var params = {
      FunctionName: 'lamda-redis-zip' /* required */,
      InvocationType: 'RequestResponse',
      Payload: JSON.stringify({
        get: true,
        key: 'c2c.school-f0a366bb-0b08-45f9-8082-cbcbeb0a9a49.teams'
      })
    };
    lambda.invoke(params, function(err, data) {
      if (err) {
        resolve({
          error: err
        });
      } else {
        resolve({
          response: data
        });
      }
    });
  });
};

exports.getAllTeams = function() {
  return new Promise(function(resolve, reject) {
    //teams query
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      var teams = [];
      var dbo = db.db('staging');
      dbo
        .collection('teams')
        .find()
        .limit(100)
        .toArray()
        .then((data) => {
          if (err) {
            reject(err);
          } else {
            teams = teams.concat(data.Items);
            resolve({
              data: {
                Items: data
              }
            });
          }
        });
    });
  });
};

exports.getAllGroups = function() {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      var dbo = db.db('staging');
      dbo
        .collection('groups')
        .find()
        .toArray()
        .then((data) => {
          if (err) {
            reject(err);
          } else {
            resolve({
              data: {
                Items: data
              }
            });
          }
        });
    });
  });
};

exports.getDeviceLocation = function(sim_id) {
  return new Promise(function(resolve, reject) {
    var apiKey = '3he2rRDrPUfKHdieRQoOv4CmOLIF4u';
    var orgId = '13417';
    var locationIQ_token = 'b6b6363732758d';

    var request = require('request');

    //hologram api to get device identifier by sim number
    request(
      {
        url:
          'https://dashboard.hologram.io/api/1/devices?orgid=' +
          orgId +
          '&apikey=' +
          apiKey +
          '&sim=' +
          sim_id
      },
      function(error, response, body) {
        if (!error && response.statusCode == 200) {
          let res = JSON.parse(body);
          let device_id = res.data[0].id;

          //hologram api to get device latitude and longitude by device identifier
          request(
            {
              url:
                'https://dashboard.hologram.io/api/1/devices/' +
                device_id +
                '?orgid=' +
                orgId +
                '&apikey=' +
                apiKey
            },
            function(error1, response1, body1) {
              if (!error1 && response1.statusCode == 200) {
                let res1 = JSON.parse(body1);
                let latitude =
                  res1.data &&
                  res1.data.lastsession &&
                  res1.data.lastsession.latitude;
                let longitude =
                  res1.data &&
                  res1.data.lastsession &&
                  res1.data.lastsession.longitude;

                //locationIQ geocode api to get location by latitude and longitude
                request(
                  {
                    url:
                      'https://us1.locationiq.com/v1/reverse.php?key=' +
                      locationIQ_token +
                      '&lat=' +
                      latitude +
                      '&lon=' +
                      longitude +
                      '&format=json'
                  },
                  function(error2, response2, body2) {
                    if (!error2 && response2.statusCode == 200) {
                      let res2 = JSON.parse(body2);
                      let address = res2.display_name;
                      resolve({
                        device: {
                          id: device_id,
                          sim: sim_id
                        },
                        geo: {
                          latitude: latitude,
                          longitude: longitude
                        },
                        maps: {
                          address: address
                        }
                      });
                    }
                  }
                );
              }
            }
          );
        }
      }
    ); //end of request 1
  }); //end promise
};

var getUniqueCode = function(length) {
  var text = '';
  var possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

var addNewGroups = function(groups) {
  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;

      var dbo = db.db('staging');
      let newGroups = [];
      let insert_groups = groups.map((group) => {
        let group_id = 'group.' + getUniqueCode(8);
        let group_name = group.text;
        newGroups.push({
          id: group_id,
          text: group_name
        });
        return {
          group_id: group_id,
          group_name: group_name
        };
      });

      dbo.collection('groups').insertMany(insert_groups, function(err, res) {
        if (err) throw err;
        resolve({
          data: res,
          newGroups: newGroups
        });
        db.close();
      });
    });
  });
};

var assignGroupsToDevices = function(parameters) {
  let { devices, groups } = parameters;
  groups = groups.map((g) => {
    return g.id;
  });

  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      var dbo = db.db('staging');
      devices.map((device, index) => {
        var query = { serial: device };
        if (parameters.allDifferent) {
          var newValues = { $addToSet: { groups: { $each: groups } } };
        } else {
          var newValues = { $set: { groups: groups } };
        }
        dbo.collection('devices').update(query, newValues, function(err, res) {
          if (err) throw err;
          resolve({
            data: res
          });
          db.close();
        });
      });
    }); //end of monogodb connect
  }); //end of promise
};

exports.assignGroups = function(parameters) {
  let { devices, groups } = parameters;
  let oldGroups = groups.filter((group) => {
    return group.id != group.text;
  });
  let newGroups = groups.filter((group) => {
    return group.id == group.text;
  });

  return new Promise(function(resolve, reject) {
    if (newGroups.length) {
      addNewGroups(newGroups).then((response) => {
        let newlyGroups = response.newGroups;
        if (newlyGroups) {
          parameters.groups = [...oldGroups, ...newlyGroups];
        }
        assignGroupsToDevices(parameters).then((response2) => {
          resolve({
            response: response2,
            newGroups: newlyGroups
          });
        });
      });
    } else {
      assignGroupsToDevices(parameters).then((response2) => {
        resolve({
          response: response2
        });
      });
    }
  });
};

exports.assignTeams = function(parameters) {
  let { device_id, teams } = parameters;

  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db('staging');
      var query = { serial: device_id };
      var data = {
        $set: {
          home_teams: teams
        }
      };
      dbo.collection('devices').update(query, data, function(err, res) {
        if (err) throw err;
        resolve({
          data: res
        });
      });
    });
  });
};

function updateDeviceInfo(device) {
  let groups_data = device.groups.map((g) => {
    return g.id;
  });

  return new Promise(function(resolve, reject) {
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db('staging');
      var query = { serial: device.id };
      //data stores the updated value
      var data = {
        $set: {
          device_name: device.name || '',
          sport: device.sport || '',
          groups: groups_data || '',
          notes: device.notes || '',
          score_board: device.score_board || ''
        }
      };
      dbo.collection('devices').updateMany(query, data, function(err, res) {
        if (err) throw err;
        db.close();
        resolve({
          data: data
        });
      });
    });
  });
}

exports.updateDeviceData = function(device) {
  return new Promise(function(resolve, reject) {
    let { groups } = device;
    let oldGroups = groups.filter((group) => {
      return group.id != group.text;
    });
    let newGroups = groups.filter((group) => {
      return group.id == group.text;
    });

    if (newGroups.length) {
      addNewGroups(newGroups).then((response) => {
        let newlyGroups = response.newGroups;
        let updated_groups = [...oldGroups, ...newlyGroups];
        device.groups = updated_groups;
        updateDeviceInfo(device).then((response2) => {
          resolve({
            response: response2,
            newGroups: newlyGroups
          });
        });
      });
    } else {
      updateDeviceInfo(device).then((response2) => {
        resolve({
          response: response2
        });
      });
    }
  });
};
