const AWS = require('aws-sdk');
const AWSConfig = require('../../config/aws.json');
AWS.config.update(AWSConfig);
const moment = require('moment');

exports.getDashBoardCount = function() {
  var docClient = new AWS.DynamoDB.DocumentClient();
  var params = {
    TableName: 'nest.devices',
    Count: 'true'
  };

  var items = [];
  return new Promise(function(resolve, reject) {
    docClient.scan(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        items = items.concat(data.Items);

        var online_devices = items.filter((i) => {
          return i.online && i.online === true;
        });

        var offline_devices = items.filter((i) => {
          return !i.online;
        });

        var streaming_devices = items.filter((i) => {
          return (
            i.last_update_ts &&
            moment()
              .utc()
              .diff(moment(i.last_update_ts), 'minutes') < 15
          );
        });

        resolve({
          online_count: online_devices.length ? online_devices.length : 0,
          offline_count: offline_devices.length ? offline_devices.length : 0,
          streaming_count: streaming_devices.length
            ? streaming_devices.length
            : 0
        });
      }
    });
  });
};
