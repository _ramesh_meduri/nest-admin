const AWS = require('aws-sdk');
const AWSConfig = require('../../config/aws.json');
const moment = require('moment');
AWS.config.update(AWSConfig);

var getGameStatusIcon = function(start_time, game) {
  let game_icon = '';
  let iflive =
    moment(start_time).format('YYYY-MM-DD') == moment().format('YYYY-MM-DD');
  if (game.finalized) {
    game_icon = 'blue-icon';
  }
  // else if(moment(start_time).diff(moment()) > 0){
  //   game_icon = 'gray-icon';
  // }
  else if (iflive && game.device_serial) {
    if (
      game.state &&
      game.state.ts &&
      moment().diff(moment(game.state.ts), 'minutes') < 10
    ) {
      game_icon = 'green-icon';
    } else {
      game_icon = 'red-icon';
    }
  }
  return game_icon;
};

function gamesScan(params) {
  var schedules = [];
  var params = {
    TableName: 'nest.schedules',
    // ProjectionExpression: "game_id, schedule_id, ",
    // FilterExpression: "contains (game_start_ts, :selected_date)",
    FilterExpression: 'begins_with (game_start_ts, :selected_date)',
    ExpressionAttributeValues: {
      ':selected_date': selected_date
    }
  };
  if (params.LastEvaluatedKey) {
    params.ExclusiveStartKey = data.LastEvaluatedKey;
  }
  return new Promise(function(resolve, reject) {
    docClient.scan(params, function(err, data) {
      schedules = schedules.concat(data.Items);
      if (data.LastEvaluatedKey) {
        gamesScan({
          LastEvaluatedKey: data.LastEvaluatedKey
        });
      }
    });
  });

  return schedules;
}

exports.getGamesList = function(params) {
  var current_date =
    new Date().getFullYear() +
    '/' +
    (new Date().getMonth() + 1) +
    '/' +
    new Date().getDate();
  var selected_date = params.selected_date || current_date;
  selected_date = moment(selected_date).format('YYYY-MM-DD');
  var docClient = new AWS.DynamoDB.DocumentClient();
  var params = {
    TableName: 'nest.schedules',
    FilterExpression: 'begins_with (game_start_ts, :selected_date)',
    ExpressionAttributeValues: {
      ':selected_date': selected_date
    }
  };

  var schedules = [];
  return new Promise(function(resolve, reject) {
    function onScan(err, data) {
      if (err) {
        return err;
      } else {
        schedules = schedules.concat(data.Items);

        if (typeof data.LastEvaluatedKey != 'undefined') {
          params.ExclusiveStartKey = data.LastEvaluatedKey;
          docClient.scan(params, onScan);
        } else {
          var schedulesByGame = {};
          var schedulesById = {};

          var filterExpressionArr = [];
          var expressionAttrObj = {};
          schedules.forEach((s, index) => {
            if (s.schedule_id) {
              if (!schedulesById[s.schedule_id]) {
                schedulesById[s.schedule_id] = s;
                if (
                  s.game_start_ts &&
                  moment(s.game_start_ts).diff(moment()) > 0
                ) {
                  schedulesById[s.schedule_id].game_icon = 'gray-icon';
                } else {
                  schedulesById[s.schedule_id].game_icon = 'no-icon';
                }
              }
            }
            if (s.game_id) {
              filterExpressionArr.push(
                'contains (game_id, :game_id_' + index + ')'
              );
              expressionAttrObj[':game_id_' + index] = s.game_id;
            }
          });

          if (!filterExpressionArr.length) {
            resolve([]);
            return;
          }

          //teams query
          var params2 = {
            TableName: 'nest.games',
            FilterExpression: filterExpressionArr.join(' OR '),
            ExpressionAttributeValues: expressionAttrObj
          };

          var games = [];
          docClient.scan(params2, function(err, data) {
            if (err) {
              reject(err);
            } else {
              games = games.concat(data.Items);
              games.forEach((game) => {
                if (schedulesById[game.schedule_id]) {
                  var schedule_data = schedulesById[game.schedule_id];
                  var game_icon = getGameStatusIcon(
                    schedule_data.game_start_ts,
                    game
                  );
                  schedulesById[game.schedule_id].game_data = game;
                  if (game_icon) {
                    schedulesById[game.schedule_id].game_icon = game_icon;
                  }
                }
              });

              var gamesData = Object.keys(schedulesById).map(function(key) {
                return schedulesById[key];
              });

              resolve(gamesData);
            }
          });
        }
      }
    }

    docClient.scan(params, onScan);
  });
};

exports.getGamesListOld2 = function(params) {
  var current_date =
    new Date().getFullYear() +
    '/' +
    (new Date().getMonth() + 1) +
    '/' +
    new Date().getDate();
  var selected_date = params.selected_date || current_date;
  selected_date = moment(selected_date).format('YYYY-MM-DD');
  var docClient = new AWS.DynamoDB.DocumentClient();
  var params = {
    TableName: 'nest.schedules',
    // ProjectionExpression: "game_id, schedule_id, ",
    // FilterExpression: "contains (game_start_ts, :selected_date)",
    FilterExpression: 'begins_with (game_start_ts, :selected_date)',
    ExpressionAttributeValues: {
      ':selected_date': selected_date
    },
    Limit: 10000
  };

  console.log(params);

  var schedules = [];
  return new Promise(function(resolve, reject) {
    function onScan(err, data) {
      if (err) {
        return err;
      } else {
        schedules = schedules.concat(data.Items);

        if (typeof data.LastEvaluatedKey != 'undefined') {
          params.ExclusiveStartKey = data.LastEvaluatedKey;
          docClient.scan(params, onScan);
        } else {
        }
      }
    }

    docClient.scan(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        console.log(data);
        schedules = schedules.concat(data.Items);

        if (!schedules.length) {
          resolve(schedules);
          return;
        }

        var schedulesByGame = {};
        var schedulesById = {};

        var filterExpressionArr = [];
        var expressionAttrObj = {};
        schedules.forEach((s, index) => {
          if (s.schedule_id) {
            if (!schedulesById[s.schedule_id]) {
              schedulesById[s.schedule_id] = s;
              if (
                s.game_start_ts &&
                moment(s.game_start_ts).diff(moment()) > 0
              ) {
                schedulesById[s.schedule_id].game_icon = 'gray-icon';
              } else {
                schedulesById[s.schedule_id].game_icon = 'no-icon';
              }
            }
          }
          if (s.game_id) {
            filterExpressionArr.push(
              'contains (game_id, :game_id_' + index + ')'
            );
            expressionAttrObj[':game_id_' + index] = s.game_id;
          }
        });

        if (!filterExpressionArr.length) {
          resolve([]);
          return;
        }

        //teams query
        var params = {
          TableName: 'nest.games',
          FilterExpression: filterExpressionArr.join(' OR '),
          ExpressionAttributeValues: expressionAttrObj
        };

        var games = [];
        docClient.scan(params, function(err, data) {
          if (err) {
            reject(err);
          } else {
            games = games.concat(data.Items);
            games.forEach((game) => {
              // var team_data = schedulesByGame[game.game_id];
              // team_data.game_start_time = moment(team_data.game_start_ts).format('dddd [at] hh:mm A');
              // team_data.game_icon = getGameStatusIcon(team_data.game_start_ts, game);
              // game.team_data = team_data;
              // gamesBySchedule[s.schedule_id]['team_data'] = s;

              if (schedulesById[game.schedule_id]) {
                var schedule_data = schedulesById[game.schedule_id];
                var game_icon = getGameStatusIcon(
                  schedule_data.game_start_ts,
                  game
                );
                schedulesById[game.schedule_id].game_data = game;
                if (game_icon) {
                  schedulesById[game.schedule_id].game_icon = game_icon;
                }
              }
            });

            // var gamesData = Object.values(gamesBySchedule);
            // var gamesData = Object.keys(gamesBySchedule).map(function(key) {
            //   return gamesBySchedule[key];
            // });

            // resolve(games);

            var gamesData = Object.keys(schedulesById).map(function(key) {
              return schedulesById[key];
            });

            resolve(gamesData);
          }
        });
      }
    });
  });
};

exports.getGamesListOld = function(params) {
  var current_date =
    new Date().getFullYear() +
    '/' +
    (new Date().getMonth() + 1) +
    '/' +
    new Date().getDate();
  var selected_date = params.selected_date || current_date;
  selected_date = moment(selected_date).format('YYYY-MM-DD');
  var docClient = new AWS.DynamoDB.DocumentClient();
  var params = {
    TableName: 'nest.games',
    // ProjectionExpression: "game_id, schedule_id, ",
    FilterExpression: 'contains (scheduled_date, :selected_date)',
    ExpressionAttributeValues: {
      ':selected_date': selected_date
    }
    // Limit: 100
  };

  var games = [];
  return new Promise(function(resolve, reject) {
    docClient.scan(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        games = games.concat(data.Items);

        if (!games.length) {
          resolve(games);
          return;
        }

        var gamesBySchedule = {};

        var filterExpressionArr = [];
        var expressionAttrObj = {};
        games.map((game, index) => {
          if (!gamesBySchedule[game.schedule_id]) {
            gamesBySchedule[game.schedule_id] = game;
            filterExpressionArr.push(
              'contains (schedule_id, :schedule_id_' + index + ')'
            );
            expressionAttrObj[':schedule_id_' + index] = game.schedule_id;
          }
        });

        //teams query
        var params = {
          TableName: 'nest.schedules',
          FilterExpression: filterExpressionArr.join(' OR '),
          ExpressionAttributeValues: expressionAttrObj
        };

        var schedules = [];
        docClient.scan(params, function(err, data) {
          if (err) {
            reject(err);
          } else {
            schedules = schedules.concat(data.Items);
            schedules.map((s) => {
              s.game_start_time = moment(s.game_start_ts).format(
                'dddd [at] hh:mm A'
              );
              s.game_icon = getGameStatusIcon(
                s.game_start_ts,
                gamesBySchedule[s.schedule_id]
              );
              gamesBySchedule[s.schedule_id]['team_data'] = s;
            });

            // var gamesData = Object.values(gamesBySchedule);
            var gamesData = Object.keys(gamesBySchedule).map(function(key) {
              return gamesBySchedule[key];
            });
            resolve(gamesData);
          }
        });
      }
    });
  });
};

exports.getGameData = function(scheduleId) {
  var docClient = new AWS.DynamoDB.DocumentClient();

  return new Promise(function(resolve, reject) {
    var params = {
      TableName: 'nest.schedules',
      FilterExpression: 'contains (schedule_id, :schedule_id)',
      ExpressionAttributeValues: {
        ':schedule_id': scheduleId
      }
    };

    //Game info
    docClient.scan(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        var response = {
          gameInfo: {
            team_data: {},
            device_data: {}
          }
        };

        if (data.Items) {
          let team_data = data.Items[0];
          response.gameInfo.team_data = team_data;
          if (team_data && team_data.game_id) {
            var gameId = team_data.game_id;

            if (s.game_start_ts && moment(s.game_start_ts).diff(moment()) > 0) {
              schedulesById[s.schedule_id].game_icon = 'gray-icon';
            } else {
              schedulesById[s.schedule_id].game_icon = 'no-icon';
            }

            //Schedule info
            if (gameId) {
              var params = {
                TableName: 'nest.games',
                FilterExpression: 'contains (game_id, :gameId)',
                ExpressionAttributeValues: {
                  ':gameId': gameId
                }
              };
              docClient.scan(params, function(err, data) {
                if (err) {
                  reject(err);
                } else {
                  if (!data.Items.length) {
                    response.gameInfo.team_data = {};
                  } else {
                    let game_data = data.Items ? data.Items[0] : {};
                    response.gameInfo = { ...response.gameInfo, ...game_data };
                    let game_start_ts =
                      response.gameInfo.team_data.game_start_ts || '';
                    let game_icon = getGameStatusIcon(
                      game_start_ts,
                      response.gameInfo
                    );
                    response.gameInfo.team_data = s;

                    //Device info
                    if (game_data.device_serial) {
                      var params2 = {
                        TableName: 'nest.devices',
                        FilterExpression: 'contains (serial, :device_serial)',
                        ExpressionAttributeValues: {
                          ':device_serial': game_data.device_serial
                        }
                      };

                      docClient.scan(params2, function(err, data) {
                        if (err) {
                          reject(err);
                        } else {
                          resolve(response);
                        }
                      });
                    } else {
                      response.gameInfo.device_data = {};
                      resolve(response);
                    }
                  }
                  schedulePromise = true;
                  checkIfPromisesFinished();
                }
              });
            } else {
              response.gameInfo.team_data = {};
              devicePromise = true;
              checkIfPromisesFinished();
            }
          } else {
            resolve(response);
          }
        } else {
          resolve(response);
        }
      }
    });
  });
};

exports.getGameDataOld = function(gameId) {
  var docClient = new AWS.DynamoDB.DocumentClient();

  return new Promise(function(resolve, reject) {
    var params = {
      TableName: 'nest.games',
      FilterExpression: 'contains (game_id, :gameId)',
      ExpressionAttributeValues: {
        ':gameId': gameId
      }
    };

    //Game info
    docClient.scan(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        var response = {
          gameInfo: null
        };

        if (data.Items) {
          response.gameInfo = data.Items[0];
          if (response.gameInfo && response.gameInfo.schedule_id) {
            var schedulePromise = false;
            var devicePromise = false;

            function checkIfPromisesFinished() {
              if (schedulePromise && devicePromise) {
                resolve(response);
              }
            }

            //Schedule info
            if (response.gameInfo.schedule_id) {
              var params = {
                TableName: 'nest.schedules',
                FilterExpression: 'contains (schedule_id, :schedule_id)',
                ExpressionAttributeValues: {
                  ':schedule_id': response.gameInfo.schedule_id
                }
              };

              docClient.scan(params, function(err, data) {
                if (err) {
                  reject(err);
                } else {
                  if (!data.Items.length) {
                    response.gameInfo.team_data = {};
                  } else {
                    let s = data.Items ? data.Items[0] : {};
                    s.game_icon = getGameStatusIcon(
                      s.game_start_ts,
                      response.gameInfo
                    );
                    response.gameInfo.team_data = s;
                  }
                  schedulePromise = true;
                  checkIfPromisesFinished();
                }
              });
            } else {
              response.gameInfo.team_data = {};
              devicePromise = true;
              checkIfPromisesFinished();
            }

            //Device info
            if (response.gameInfo.device_serial) {
              var params2 = {
                TableName: 'nest.devices',
                FilterExpression: 'contains (serial, :device_serial)',
                ExpressionAttributeValues: {
                  ':device_serial': response.gameInfo.device_serial
                }
              };

              docClient.scan(params2, function(err, data) {
                if (err) {
                  reject(err);
                } else {
                  response.gameInfo.device_data = data.Items
                    ? data.Items[0]
                    : {};
                  devicePromise = true;
                  checkIfPromisesFinished();
                }
              });
            } else {
              response.gameInfo.device_data = {};
              devicePromise = true;
              checkIfPromisesFinished();
            }
          } else {
            resolve(response);
          }
        } else {
          resolve(response);
        }
      }
    });
  });
};

exports.finalizeGame = function(gameId) {};

exports.unfinalizeGame = function(gameId) {};
