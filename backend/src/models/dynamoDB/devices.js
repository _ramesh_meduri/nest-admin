const AWS = require('aws-sdk');
const AWSConfig = require('../../config/aws.json');
AWS.config.update(AWSConfig);
const moment = require('moment');

exports.getDevicesList = function() {
  var docClient = new AWS.DynamoDB.DocumentClient();
  var params = {
    TableName: 'nest.devices'
    // Limit: 100
  };
  var items = [];
  return new Promise(function(resolve, reject) {
    docClient.scan(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        items = items.concat(data.Items);
        items = items.sort(function(a, b) {
          var from_ts = moment(
            a.last_online_ts ? a.last_online_ts : '1971-01-01'
          ).unix();
          var to_ts = moment(
            b.last_online_ts ? b.last_online_ts : '1971-01-01'
          ).unix();
          return to_ts - from_ts;
        });
        resolve(items);
      }
    });
  });
};

exports.getDeviceData = function(deviceId) {
  var docClient = new AWS.DynamoDB.DocumentClient();

  return new Promise(function(resolve, reject) {
    var params = {
      TableName: 'nest.devices',
      FilterExpression: 'contains (serial, :deviceId)',
      ExpressionAttributeValues: {
        ':deviceId': deviceId
      }
    };

    docClient.scan(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        var response = {
          deviceInfo: null,
          teams: []
        };

        if (data.Items) {
          response.deviceInfo = data.Items[0];
          if (
            response.deviceInfo &&
            response.deviceInfo.home_teams &&
            response.deviceInfo.home_teams.length
          ) {
            var filterExpressionArr = [];
            var expressionAttrObj = {};
            response.deviceInfo.home_teams.map((team, index) => {
              filterExpressionArr.push(
                'contains (team_id, :team_id_' + index + ')'
              );
              expressionAttrObj[':team_id_' + index] = team;
            });

            //teams query
            var params = {
              TableName: 'nest.teams',
              FilterExpression: filterExpressionArr.join(' OR '),
              ExpressionAttributeValues: expressionAttrObj
            };

            docClient.scan(params, function(err, data) {
              if (err) {
                reject(err);
              } else {
                response.teams = response.teams.concat(data.Items);
                resolve(response);
              }
            });
          } else {
            resolve(response);
          }
        } else {
          resolve(response);
        }
      }
    });
  });
};

exports.getDeviceStreamData = function(deviceId) {
  var lambda = new AWS.Lambda();

  return new Promise(function(resolve, reject) {
    var params = {
      FunctionName: 'lamda-redis-zip' /* required */,
      InvocationType: 'RequestResponse',
      Payload: JSON.stringify({
        get: true,
        key: 'c2c.school-f0a366bb-0b08-45f9-8082-cbcbeb0a9a49.teams'
      })
    };
    lambda.invoke(params, function(err, data) {
      if (err) {
        resolve({
          error: err
        });
      } else {
        resolve({
          response: data
        });
      }
    });
  });
};

exports.getAllTeams = function() {
  var docClient = new AWS.DynamoDB.DocumentClient();

  return new Promise(function(resolve, reject) {
    //teams query
    var params = {
      TableName: 'nest.teams',
      Limit: 100
    };

    var teams = [];
    docClient.scan(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        teams = teams.concat(data.Items);
        resolve({
          data: data
        });
      }
    });
  });
};

exports.getAllGroups = function() {
  var docClient = new AWS.DynamoDB.DocumentClient();

  return new Promise(function(resolve, reject) {
    //teams query
    var params = {
      TableName: 'nest.groups'
    };

    var groups = [];
    docClient.scan(params, function(err, data) {
      if (err) {
        reject(err);
      } else {
        groups = groups.concat(data.Items);
        resolve({
          data: data
        });
      }
    });
  });
};

exports.getDeviceLocation = function(sim_id) {
  return new Promise(function(resolve, reject) {
    var apiKey = '3he2rRDrPUfKHdieRQoOv4CmOLIF4u';
    var orgId = '13417';
    var locationIQ_token = 'b6b6363732758d';

    var request = require('request');

    //hologram api to get device identifier by sim number
    request(
      {
        url:
          'https://dashboard.hologram.io/api/1/devices?orgid=' +
          orgId +
          '&apikey=' +
          apiKey +
          '&sim=' +
          sim_id
      },
      function(error, response, body) {
        if (!error && response.statusCode == 200) {
          let res = JSON.parse(body);
          let device_id = res.data[0].id;

          //hologram api to get device latitude and longitude by device identifier
          request(
            {
              url:
                'https://dashboard.hologram.io/api/1/devices/' +
                device_id +
                '?orgid=' +
                orgId +
                '&apikey=' +
                apiKey
            },
            function(error1, response1, body1) {
              if (!error1 && response1.statusCode == 200) {
                let res1 = JSON.parse(body1);
                let latitude =
                  res1.data &&
                  res1.data.lastsession &&
                  res1.data.lastsession.latitude;
                let longitude =
                  res1.data &&
                  res1.data.lastsession &&
                  res1.data.lastsession.longitude;

                //locationIQ geocode api to get location by latitude and longitude
                request(
                  {
                    url:
                      'https://us1.locationiq.com/v1/reverse.php?key=' +
                      locationIQ_token +
                      '&lat=' +
                      latitude +
                      '&lon=' +
                      longitude +
                      '&format=json'
                  },
                  function(error2, response2, body2) {
                    if (!error2 && response2.statusCode == 200) {
                      let res2 = JSON.parse(body2);
                      let address = res2.display_name;
                      resolve({
                        device: {
                          id: device_id,
                          sim: sim_id
                        },
                        geo: {
                          latitude: latitude,
                          longitude: longitude
                        },
                        maps: {
                          address: address
                        }
                      });
                    }
                  }
                );
              }
            }
          );
        }
      }
    ); //end of request 1
  }); //end promise
};

var getUniqueCode = function(length) {
  var text = '';
  var possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

var addNewGroups = function(groups) {
  var dynamodb = new AWS.DynamoDB();

  return new Promise(function(resolve, reject) {
    let newGroups = [];

    let queires = groups.map((group) => {
      let group_id = 'group.' + getUniqueCode(8);
      let group_name = group.text;
      newGroups.push({
        id: group_id,
        text: group_name
      });
      return {
        PutRequest: {
          Item: {
            group_id: { S: group_id },
            group_name: { S: group_name }
          }
        }
      };
    });

    var batchRequest = {
      RequestItems: {
        'nest.groups': queires
      }
    };

    dynamodb.batchWriteItem(batchRequest, function(err, data) {
      if (err) {
        //reject(err);
      } else {
        resolve({
          data: data,
          newGroups: newGroups
        });
      }
    });
  });
};

var assignGroupsToDevices = function(parameters) {
  var docClient = new AWS.DynamoDB.DocumentClient();
  let { devices, groups } = parameters;
  let datas = [];

  groups = groups.map((g) => {
    return g.id;
  });

  return new Promise(function(resolve, reject) {
    devices.map((device, index) => {
      if (parameters.allDifferent) {
        var params = {
          TableName: 'nest.devices',
          Key: {
            serial: device
          },
          ReturnValues: 'ALL_NEW',
          UpdateExpression:
            'set #groups = list_append(if_not_exists(#groups, :empty_list), :groups)',
          ExpressionAttributeNames: {
            '#groups': 'groups'
          },
          ExpressionAttributeValues: {
            ':groups': groups,
            ':empty_list': []
          }
        };
      } else {
        var params = {
          TableName: 'nest.devices',
          Key: {
            serial: device
          },
          ReturnValues: 'ALL_NEW',
          UpdateExpression: 'SET groups = :groups',
          ExpressionAttributeValues: {
            ':groups': groups
          }
        };
      }

      docClient.update(params, function(err, data) {
        if (err) {
          //reject(err);
        } else {
          datas.push(data);
          if (index == devices.length - 1) {
            resolve({
              data: data
            });
          }
        }
      });
    });
  });
};

exports.assignGroups = function(parameters) {
  let { devices, groups } = parameters;
  let oldGroups = groups.filter((group) => {
    return group.id != group.text;
  });
  let newGroups = groups.filter((group) => {
    return group.id == group.text;
  });

  return new Promise(function(resolve, reject) {
    if (newGroups.length) {
      addNewGroups(newGroups).then((response) => {
        let newlyGroups = response.newGroups;
        if (newlyGroups) {
          parameters.groups = [...oldGroups, ...newlyGroups];
        }
        assignGroupsToDevices(parameters).then((response2) => {
          resolve({
            response: response2,
            newGroups: newlyGroups
          });
        });
      });
    } else {
      assignGroupsToDevices(parameters).then((response2) => {
        resolve({
          response: response2
        });
      });
    }
  });
};

exports.assignTeams = function(parameters) {
  var docClient = new AWS.DynamoDB.DocumentClient();
  let { device_id, teams } = parameters;

  return new Promise(function(resolve, reject) {
    var params = {
      TableName: 'nest.devices',
      Key: {
        serial: device_id
      },
      UpdateExpression: 'SET home_teams = :home_teams',
      ExpressionAttributeValues: {
        ':home_teams': teams
      },
      ReturnValues: 'ALL_NEW'
    };

    docClient.update(params, function(err, data) {
      if (err) {
        //reject(err);
      } else {
        resolve({
          data: data
        });
      }
    });
  });
};

function updateDeviceInfo(device) {
  var docClient = new AWS.DynamoDB.DocumentClient();

  let groups_data = device.groups.map((g) => {
    return g.id;
  });

  return new Promise(function(resolve, reject) {
    var params = {
      TableName: 'nest.devices',
      Key: {
        serial: device.id
      },
      UpdateExpression:
        'SET device_name = :device_name, sport = :sport, groups = :groups, notes = :notes, score_board = :score_board',
      ExpressionAttributeValues: {
        ':device_name': device.name || '',
        ':sport': device.sport || '',
        ':groups': groups_data || '',
        ':notes': device.notes || '',
        ':score_board': device.score_board || ''
      },
      ReturnValues: 'ALL_NEW'
    };

    docClient.update(params, function(err, data) {
      if (err) {
        //reject(err);
      } else {
        resolve({
          data: data
        });
      }
    });
  });
}

exports.updateDeviceData = function(device) {
  return new Promise(function(resolve, reject) {
    let { groups } = device;
    let oldGroups = groups.filter((group) => {
      return group.id != group.text;
    });
    let newGroups = groups.filter((group) => {
      return group.id == group.text;
    });

    if (newGroups.length) {
      addNewGroups(newGroups).then((response) => {
        let newlyGroups = response.newGroups;
        let updated_groups = [...oldGroups, ...newlyGroups];
        device.groups = updated_groups;
        updateDeviceInfo(device).then((response2) => {
          resolve({
            response: response2,
            newGroups: newlyGroups
          });
        });
      });
    } else {
      updateDeviceInfo(device).then((response2) => {
        resolve({
          response: response2
        });
      });
    }
  });
};
