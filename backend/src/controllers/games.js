var dbconfig = require('../config/db.json');

var gameModel = require('../models/' + dbconfig.folder + '/games');

exports.index = function(req, res) {
  return gameModel
    .getGamesList()
    .then((response) => {
      console.log(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

//get devices list based on the params
exports.getGamesList = function(req, res) {
  gameModel
    .getGamesList(req.body)
    .then((response) => {
      res.json({
        games: response
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getGameData = function(req, res) {
  let gameId = req.params.gameId;
  gameModel
    .getGameData(gameId)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.finalizeGame = function(req, res) {
  let gameId = req.params.gameId;
  gameModel
    .finalizeGame(gameId)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.unfinalizeGame = function(req, res) {
  let gameId = req.params.gameId;
  gameModel
    .unfinalizeGame(gameId)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};
