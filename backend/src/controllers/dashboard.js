var dbconfig = require('../config/db.json');

var dashboardModel = require('../models/' + dbconfig.folder + '/dashboard');

exports.getDashBoardCount = function(req, res) {
  dashboardModel
    .getDashBoardCount()
    .then((response) => {
      res.json({
        devices: response
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
