var dbconfig = require('../config/db.json');
var facilitesModel = require('../models/' + dbconfig.folder + '/facilities');

exports.index = function(req, res) {
  return facilitesModel
    .getFacilitiesList()
    .then((response) => {
      console.log(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getFacilitiesList = function(req, res) {
  facilitesModel
    .getFacilitiesList()
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getSchoolfacilityData = function(req, res) {
  facilitesModel;
  getSchoolfacilityData()
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.addFacilityFromFacilitypage = function(req, res) {
  facilitesModel
    .addFacilityFromFacilitypage(req.body)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.updateFacilityInfoFromFacilitypage = function(req, res) {
  facilitesModel
    .updateFacilityInfoFromFacilitypage(req.body)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.getAllFacilityTypes = function(req, res) {
  facilitesModel
    .getAllFacilityTypes()
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getAllScoreBoardTypes = function(req, res) {
  facilitesModel
    .getAllScoreBoardTypes()
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};
