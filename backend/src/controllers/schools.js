var dbconfig = require('../config/db.json');
var schoolModel = require('../models/' + dbconfig.folder + '/schools');

exports.index = function(req, res) {
  return schoolModel
    .getSchoolsList()
    .then((response) => {
      console.log(response);
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.getAll = function(req, res) {
  return schoolModel
    .getAll()
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
 };


exports.getSchoolsList = function(req, res) {
  // let data = {name:'ramesh'};
  // return res.json(data);
  schoolModel
    .getSchoolsList(req.params.email)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.getSchool = function(req, res) {
  schoolModel
    .getSchool(req.params.schoolId)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getSchoolListforSignup = function() {
  schoolModel
    .getSchoolListforSignup()
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getSchoolData = function(req, res) {
  let schoolId = req.params.schoolId;
  schoolModel
    .getSchoolData(schoolId)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getSchoolfacilityData = function(req, res) {
  let schoolId = req.params.schoolId;
  schoolModel
    .getSchoolfacilityData(schoolId)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.addschool = function(req, res) {
  schoolModel
    .addSchool(req.body)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.addSchoolfromSignUp = function(req, res) {
  schoolModel
    .addSchoolfromSignUp(req.body)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.updateFacilityInfo = function(req, res) {
  schoolModel
    .updateFacilityInfo(req.body)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.updateSchoolAddress = function(req, res) {
  schoolModel
    .updateSchoolAddress(req.body)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.updateSchoolOrgId = function(req, res) {
  schoolModel
    .updateSchoolOrgId(req.body)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.addFacility = function(req, res) {
  schoolModel
    .addFacility(req.body)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.getAllSports = function(req, res) {
  schoolModel
    .getAllSports()
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.getTeamGrades = function(req, res) {
  schoolModel
    .getTeamGrades()
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};
