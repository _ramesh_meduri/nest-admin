var dbconfig = require('../config/db.json');
var usersModel = require('../models/' + dbconfig.folder + '/users');

exports.index = function(req, res) {
  return schoolModel
    .getUsersList()
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getUsersList = function(req, res) {
  usersModel
    .getUsersList()
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getUserData = function(req, res) {
  let userId = req.params.userId;
  usersModel
    .getUserData(userId)
    .then((response) => {
      // console.log(response);
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.adduserFromUserpage = function(req, res) {
  usersModel
    .adduserFromUserpage(req.body)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.updateUserInfo = function(req, res) {
  usersModel
    .updateUserInfo(req.body)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.getUsersListBySchoolId = function(req, res) {
  let schoolId = req.params.schoolId;
  usersModel
    .getUsersListBySchoolId(schoolId)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};
