var dbconfig = require('../config/db.json');
var sportsModel = require('../models/' + dbconfig.folder + '/sports');

exports.getAll = function(req, res) {
  sportsModel
    .getAll()
    .then((docs) => {
      res.json(docs);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.createOne = function(req, res) {
  sportsModel
    .createOne(req.body)
    .then((doc) => {
      res.json(doc);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.updateOne = function(req, res) {
  sportsModel
    .updateOne(req.params.id, req.body)
    .then((doc) => {
      res.json(doc);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.deleteOne = function(req, res) {
  sportsModel
    .deleteOne(req.params.id)
    .then((doc) => {
      res.json(doc);
    })
    .catch((err) => {
      console.log(err);
    });
};
