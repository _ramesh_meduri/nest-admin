var dbconfig = require('../config/db.json');

var deviceModel = require('../models/' + dbconfig.folder + '/devices');

exports.index = function(req, res) {
  return deviceModel
    .getDevicesList()
    .then((response) => {
      console.log(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

//get devices list based on the params
exports.getDevicesList = function(req, res) {
  deviceModel
    .getDevicesList()
    .then((response) => {
      res.json({
        devices: response
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

//device data based on id
exports.getDeviceData = function(req, res) {
  let deviceId = req.params.deviceId;
  deviceModel
    .getDeviceData(deviceId)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getDeviceStreamData = function(req, res) {
  let deviceId = req.params.deviceId;
  deviceModel
    .getDeviceStreamData(deviceId)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getAllTeams = function(req, res) {
  deviceModel
    .getAllTeams()
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getDeviceLocation = function(req, res) {
  let sim_id = req.params.sim_id;
  deviceModel
    .getDeviceLocation(sim_id)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getAllGroups = function(req, res) {
  deviceModel
    .getAllGroups()
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.assignGroups = function(req, res) {
  deviceModel
    .assignGroups(req.body)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.assignTeams = function(req, res) {
  deviceModel
    .assignTeams(req.body)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.updateDeviceData = function(req, res) {
  deviceModel
    .updateDeviceData(req.body)
    .then((response) => {
      res.json(response);
    })
    .catch((err) => {
      console.log(err);
    });
};
