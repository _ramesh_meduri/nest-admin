var express = require('express');
var router = express.Router();
var facilitiesController = require('../controllers/facilities');

router.get('/', facilitiesController.getFacilitiesList);
router.get(
  '/getSchoolfacilityData',
  facilitiesController.getSchoolfacilityData
);
router.post(
  '/addFacilityFromFacilitypage',
  facilitiesController.addFacilityFromFacilitypage
);
router.post(
  '/updateFacilityInfoFromFacilitypage',
  facilitiesController.updateFacilityInfoFromFacilitypage
);
router.get('/facilityTypes', facilitiesController.getAllFacilityTypes);

router.get('/scoreBoardTypes', facilitiesController.getAllScoreBoardTypes);

module.exports = router;
