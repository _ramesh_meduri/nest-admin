var express = require('express');
var router = express.Router();
var devicesController = require('../controllers/devices');

router.get('/', devicesController.getDevicesList);
router.get('/list', devicesController.getDevicesList);
router.get('/detail/:deviceId', devicesController.getDeviceData);
router.get('/streamData/:deviceId', devicesController.getDeviceStreamData);
router.post('/updateInfo', devicesController.updateDeviceData);
router.get('/getAllTeams', devicesController.getAllTeams);
router.get('/getDeviceLocation/:sim_id', devicesController.getDeviceLocation);
router.get('/getAllGroups', devicesController.getAllGroups);
router.post('/assignGroups', devicesController.assignGroups);
router.post('/assignTeams', devicesController.assignTeams);

module.exports = router;
