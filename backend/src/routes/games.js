var express = require('express');
var router = express.Router();
var gamesController = require('../controllers/games');

router.post('/', gamesController.getGamesList);
router.get('/detail/:gameId', gamesController.getGameData);
router.get('/finalize/:gameId', gamesController.finalizeGame);
router.get('/unfinalize/:gameId', gamesController.unfinalizeGame);

module.exports = router;
