var express = require('express');
var router = express.Router();
var sportsController = require('../controllers/sports');

router.get('/', sportsController.getAll);
router.post('/', sportsController.createOne);
router.put('/:id', sportsController.updateOne);
router.delete('/:id', sportsController.deleteOne);

module.exports = router;
