var express = require('express');
var router = express.Router();
var usersController = require('../controllers/users');

router.get('/', usersController.getUsersList);
router.get('/:schoolId', usersController.getUsersListBySchoolId);
router.get('/getuserdata/:userId', usersController.getUserData);
router.post('/adduserFromUserpage', usersController.adduserFromUserpage);
router.post('/updateUserInfo', usersController.updateUserInfo);

module.exports = router;
