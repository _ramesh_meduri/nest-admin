var express = require('express');
var router = express.Router();
var dashboardController = require('../controllers/dashboard');

router.get('/chatCount', dashboardController.getDashBoardCount);

module.exports = router;
