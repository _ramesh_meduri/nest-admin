var express = require('express');
var router = express.Router();
var schoolController = require('../controllers/schools');

router.get('/', schoolController.getSchoolsList);
router.get('/getAll', schoolController.getAll);
router.get('/details/:schoolId', schoolController.getSchool);
router.get('/getAllSports', schoolController.getAllSports);
router.get('/getTeamGrades', schoolController.getTeamGrades);
router.get('/:email', schoolController.getSchoolsList);
router.get('/detail/:schoolId', schoolController.getSchoolData);
router.get('/facilitydata/:schoolId', schoolController.getSchoolfacilityData);
router.post('/updateFacilityInfo', schoolController.updateFacilityInfo);
router.post('/updateSchoolAddress', schoolController.updateSchoolAddress);
router.post('/updateSchoolOrgId', schoolController.updateSchoolOrgId);
router.get('/getSchoolListforSignup', schoolController.getSchoolListforSignup);
router.post('/addSchool', schoolController.addschool);
router.post('/addschoolfromsignup', schoolController.addSchoolfromSignUp);
router.post('/addFacility', schoolController.addFacility);

module.exports = router;
